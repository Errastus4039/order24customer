package com.hitee.order24customerapp.LoginUtils;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.hitee.order24customerapp.Bottom.CustomerActivity;
import com.hitee.order24customerapp.DashboardUtils.BusinessActivity;
import com.hitee.order24customerapp.ForgotPasswordUtil.ForgotPasswordActivity;
import com.hitee.order24customerapp.Models.LoginResponseModel;
import com.hitee.order24customerapp.Models.ProfileModel;
import com.hitee.order24customerapp.Models.UserModel;
import com.hitee.order24customerapp.ProductsUtils.ProductDetailsActivity;
import com.hitee.order24customerapp.R;
import com.hitee.order24customerapp.RegisterUtils.RegisterActivity;
import com.hitee.order24customerapp.Utils.AlertDialogManager;
import com.hitee.order24customerapp.Utils.Constant;
import com.hitee.order24customerapp.Utils.PrefUtils;
import com.hitee.order24customerapp.app.AppController;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;

public class LoginActivity extends AppCompatActivity {

    UserModel user;
    AlertDialogManager alertDialogManager;
    ProfileModel userProfile;

    RelativeLayout relativeLayout;
    Button btnLog, btnReg;
    private AppCompatCheckBox checkbox;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final ActionBar abar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.custom_actionbar, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        TextView txttitle = (TextView) viewActionBar.findViewById(R.id.txtTitle);
        txttitle.setText("S I G N  I N");
        txttitle.setTextSize(14);
        if (abar != null) {
            abar.setCustomView(viewActionBar, params);

            abar.setDisplayShowCustomEnabled(true);
            abar.setDisplayShowTitleEnabled(false);
            abar.setHomeButtonEnabled(false);
            // abar.setDisplayHomeAsUpEnabled(true);
        }




        alertDialogManager = new AlertDialogManager();
        btnLog = (Button) findViewById(R.id.btnLog);
        TextView txtregister = (TextView) findViewById(R.id.register);
        TextView txtforget_password = (TextView) findViewById(R.id.forget_password);

        txtforget_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent ii = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(ii);
            }
        });

        txtregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        });

        final ProgressDialog waitingDialog = new ProgressDialog(this);
        waitingDialog.setTitle("Login");
        waitingDialog.setMessage("Please wait while we access your account");

        user = PrefUtils.getCurrentUserModel(LoginActivity.this);
        if (PrefUtils.getCurrentUser(LoginActivity.this) != null) {
            startActivity(new Intent(LoginActivity.this, BusinessActivity.class));

        } else {

        }

        Intent intent = getIntent();
        final String getProductId = intent.getStringExtra("getProductId");
        final String getBusinessName = intent.getStringExtra("getBusinessName");
        final String getDescription = intent.getStringExtra("getDescription");
        final String getImageUrl = intent.getStringExtra("getImageUrl");
        final String getName = intent.getStringExtra("getName");
        final String getPrice = intent.getStringExtra("getPrice");
        final String getProductionTime = intent.getStringExtra("getProductionTime");
        final String getThumbnail = intent.getStringExtra("getThumbnail");


        final EditText edtEmail = (EditText) findViewById(R.id.email);
        final EditText edtPassword = (EditText) findViewById(R.id.pswd);
        checkbox = (AppCompatCheckBox) findViewById(R.id.checkbox);



        checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean value) {
                if (value)
                {
                    // Show Password
                    edtPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
                else
                {
                    // Hide Password
                    edtPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });


        btnLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                waitingDialog.show();

                if (edtEmail.getText().toString().equals("") && edtPassword.getText().toString().equals("")) {
                    alertDialogManager.showAlertDialog(LoginActivity.this, "Failed", "Please fill the form", false);

                } else {

                    StringRequest strReq = new StringRequest(Request.Method.POST, Constant.LOGIN,
                            new com.android.volley.Response.Listener<String>() {

                                @Override
                                public void onResponse(String response) {
                                    Log.d("TAGGG", String.valueOf(response));

                                    waitingDialog.dismiss();
                                    if (response.equals("400")) {
                                        alertDialogManager.showAlertDialog(LoginActivity.this, "Failed", "Wrong email or password supplied", false);
                                        Intent intent = new Intent(LoginActivity.this, LoginActivity.class);// New activity
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        finish(); // Call once you redirect to another activity

                                    }
                                    else
                                    {
                                        try {
                                            JSONObject object = new JSONObject(response);
                                            UserModel userModel = new UserModel();
                                            userModel.setEmail(object.getString("Email"));
                                            userModel.setFirstName(object.getString("FirstName"));
                                            userModel.setLastName(object.getString("LastName"));
                                            userModel.setPhoneNumber(object.getString("Contact_Phone_1"));
                                            userModel.setId(object.getString("Id"));

                                            PrefUtils.setCurrentUserModel(userModel, LoginActivity.this);


                                            if (intent.getStringExtra("getProductId") == null || intent.getStringExtra("getPrice") == null) {
                                                Intent ii = new Intent(LoginActivity.this, CustomerActivity.class);
                                                startActivity(ii);
                                                finish();
                                            } else {
                                                Intent ii = new Intent(LoginActivity.this, ProductDetailsActivity.class);

                                                ii.putExtra("PRODUCT_DETAILS", "PD");
                                                ii.putExtra("getProductId", getProductId);
                                                ii.putExtra("getBusinessName", getBusinessName);
                                                ii.putExtra("getDescription", getDescription);
                                                ii.putExtra("getImageUrl", getImageUrl);
                                                ii.putExtra("getName", getName);
                                                ii.putExtra("getPrice", getPrice);
                                                ii.putExtra("getProductionTime", getProductionTime);
                                                ii.putExtra("getThumbnail", getThumbnail);

                                                startActivity(ii);
                                                finish();
                                            }


                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }

                                }
                            }, new com.android.volley.Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            NetworkResponse response = error.networkResponse;
                            if (error instanceof ServerError && response != null) {

                            }
                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("Email", edtEmail.getText().toString().trim());
                            params.put("Password", edtPassword.getText().toString().trim());

                            Log.d("POST_TAG", params.toString());
                            return params;
                        }
                    };
                    DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                    strReq.setRetryPolicy(retryPolicy);
                    AppController.getInstance().addToRequestQueue(strReq);

                }
            }
        });


    }


}
