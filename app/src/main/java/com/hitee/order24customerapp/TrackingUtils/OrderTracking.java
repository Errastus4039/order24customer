package com.hitee.order24customerapp.TrackingUtils;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.hitee.order24customerapp.Adapter.OrderHistoryAdapter;
import com.hitee.order24customerapp.Adapter.TrackingOrderHistoryAdapter;
import com.hitee.order24customerapp.LoginUtils.LoginActivity;
import com.hitee.order24customerapp.Models.OrderHistoryModel;
import com.hitee.order24customerapp.Models.UserModel;
import com.hitee.order24customerapp.OrderUtils.OrderHistoryActivity;
import com.hitee.order24customerapp.OrderUtils.OrderListActivity;
import com.hitee.order24customerapp.ProductsUtils.ProductListActivity;
import com.hitee.order24customerapp.R;
import com.hitee.order24customerapp.Utils.Constant;
import com.hitee.order24customerapp.Utils.PrefUtils;
import com.hitee.order24customerapp.Utils.RecyclerTouchListener;
import com.hitee.order24customerapp.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class OrderTracking extends AppCompatActivity {

    private static final String TAG = OrderTracking.class.getSimpleName();
    private RecyclerView recyclerView;
    private ArrayList<OrderHistoryModel> cartList;
    private TrackingOrderHistoryAdapter rcAdapter1;
    private ShimmerFrameLayout mShimmerViewContainer;
    String CategoryId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_tracking);


        final ActionBar abar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.custom_actionbar, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        TextView txttitle = (TextView) viewActionBar.findViewById(R.id.txtTitle);
        txttitle.setText("ORDERS AVAILABLE FOR TRACKING");
        txttitle.setTextSize(14);
        if (abar != null) {
            abar.setCustomView(viewActionBar, params);

            abar.setDisplayShowCustomEnabled(true);
            abar.setDisplayShowTitleEnabled(false);
            abar.setHomeButtonEnabled(false);
            abar.setDisplayHomeAsUpEnabled(true);
        }


        cartList = new ArrayList<>();
        UserModel model = PrefUtils.getCurrentUserModel(OrderTracking.this);
        if (model == null) {
            startActivity(new Intent(OrderTracking.this, LoginActivity.class));
            finish();
        } else {
            fetchProducts(Constant.GET_ORDER_HISTORY + "/" + model.getId());

            LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);


            RecyclerView rView1 = (RecyclerView) findViewById(R.id.recyclerView1);
            rView1.setLayoutManager(layoutManager);
            rView1.setNestedScrollingEnabled(false);
            rView1.setHasFixedSize(false);

            rcAdapter1 = new TrackingOrderHistoryAdapter(this, cartList);
            rView1.setAdapter(rcAdapter1);


            mShimmerViewContainer = findViewById(R.id.shimmer_view_container);

            rView1.addOnItemTouchListener(new RecyclerTouchListener(this, rView1, new RecyclerTouchListener.ClickListener() {
                @Override
                public void onClick(View view, int position) {
                    final OrderHistoryModel doc = cartList.get(position);
                    Intent i = new Intent(OrderTracking.this, TrackingListActivity.class);
                    i.putExtra("getBookingID", doc.getBookingID());
                    i.putExtra("getOrderValue", doc.getOrderValue());
                    i.putExtra("getBusinessAddress", doc.getBusinessAddress());
                    i.putExtra("getBusinessID", doc.getBusinessID());
                    i.putExtra("getBusinessPhone", doc.getBusinessPhone());
                    i.putExtra("getCustomerId", doc.getCustomerId());
                    i.putExtra("getCustomerPhone", doc.getCustomerPhone());
                    i.putExtra("getOrderId", doc.getOrderId());
                    i.putExtra("getCustomerName", doc.getCustomerName());
                    i.putExtra("getCreatedDate", doc.getCreatedDate());


                    startActivity(i);
                }

                @Override
                public void onLongClick(View view, int position) {

                }
            }));
        }
    }

    private void fetchProducts(String URL) {

        final ProgressDialog progress = new ProgressDialog(OrderTracking.this);
        progress.setMessage("Loading...");
        progress.show();

        JsonArrayRequest request = new JsonArrayRequest(URL,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        progress.dismiss();
                        Log.d("LL", response.toString());
                        try {
                            if (response.toString().equals("[]")) {
                                Toast.makeText(OrderTracking.this, "There is no history found.", Toast.LENGTH_LONG).show();
                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);
                                return;
                            }

                            for (int i = 0; i < response.length(); i++) {

                                try {
                                    final JSONObject obj = response.getJSONObject(i);

                                    OrderHistoryModel mydic = new OrderHistoryModel();
                                    mydic.setBookingID(obj.getString("BookingID"));
                                    mydic.setBusinessID(obj.getString("BusinessID"));
                                    mydic.setDropAddress(obj.getString("DropAddress"));
                                    mydic.setCustomerName(obj.getString("CustomerName"));
                                    mydic.setStoreName(obj.getString("StoreName"));
                                    mydic.setPickupAddress(obj.getString("PickupAddress"));
                                    mydic.setOrderValue(obj.getString("OrderValue"));
                                    mydic.setOrderId(obj.getString("OrderId"));
                                    mydic.setCreatedDate(obj.getString("CreatedDate"));
                                    mydic.setCustomerPhone(obj.getString("CustomerPhone"));

                                    cartList.add(mydic);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            rcAdapter1.notifyDataSetChanged();
                            // stop animating Shimmer and hide the layout
                            mShimmerViewContainer.stopShimmerAnimation();
                            mShimmerViewContainer.setVisibility(View.GONE);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        rcAdapter1.notifyDataSetChanged();

                        // stop animating Shimmer and hide the layout
                        mShimmerViewContainer.stopShimmerAnimation();
                        mShimmerViewContainer.setVisibility(View.GONE);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error in getting json
                Log.e(TAG, "Error: " + error.getMessage());
                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
                progress.dismiss();
                Toast.makeText(getApplicationContext(), "Opps! Something went wrong, please try again later ", Toast.LENGTH_SHORT).show();
            }
        });

        AppController.getInstance().addToRequestQueue(request);


    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        super.onPause();
    }
}
