package com.hitee.order24customerapp.TrackingUtils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
//import com.google.firebase.database.ValueEventListener;
//import com.hitee.order24customerapp.Models.LocationTracking;
//import com.hitee.order24customerapp.Models.MovementModel;
//import com.hitee.order24customerapp.R;
//import com.hitee.order24customerapp.Utils.PrefUtils;
//import com.mapbox.android.core.location.LocationEngine;
//import com.mapbox.android.core.location.LocationEngineCallback;
//import com.mapbox.android.core.location.LocationEngineProvider;
//import com.mapbox.android.core.location.LocationEngineRequest;
//import com.mapbox.android.core.location.LocationEngineResult;
//import com.mapbox.android.core.permissions.PermissionsListener;
//import com.mapbox.android.core.permissions.PermissionsManager;
//import com.mapbox.api.directions.v5.models.DirectionsResponse;
//import com.mapbox.api.directions.v5.models.DirectionsRoute;
//import com.mapbox.geojson.Feature;
//import com.mapbox.geojson.Point;
//import com.mapbox.mapboxsdk.Mapbox;
//import com.mapbox.mapboxsdk.location.LocationComponent;
//import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
//import com.mapbox.mapboxsdk.location.modes.CameraMode;
//import com.mapbox.mapboxsdk.location.modes.RenderMode;
//import com.mapbox.mapboxsdk.maps.MapView;
//import com.mapbox.mapboxsdk.maps.MapboxMap;
//import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
//import com.mapbox.mapboxsdk.maps.Style;
//import com.mapbox.mapboxsdk.style.layers.PropertyFactory;
//import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
//import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
//import com.mapbox.services.android.navigation.ui.v5.NavigationLauncher;
//import com.mapbox.services.android.navigation.ui.v5.NavigationLauncherOptions;
//import com.mapbox.services.android.navigation.ui.v5.route.NavigationMapRoute;
//import com.mapbox.services.android.navigation.v5.navigation.NavigationRoute;

import java.lang.ref.WeakReference;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
//
//import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconAllowOverlap;
//import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconIgnorePlacement;
//import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconImage;

//public class MovementActivity extends AppCompatActivity implements OnMapReadyCallback, PermissionsListener {
    // variables for adding location layer
//    private MapView mapView;
//    private MapboxMap mapboxMap;
//    // variables for adding location layer
//    private PermissionsManager permissionsManager;
//    private LocationComponent locationComponent;
//    // variables for calculating and drawing a route
//    private DirectionsRoute currentRoute;
//    private static final String TAG = "OrderTrackingActivity";
//    private NavigationMapRoute navigationMapRoute;
//    // variables needed to initialize navigation
//    private Button button;
//    private LocationEngine locationEngine;
//    private long DEFAULT_INTERVAL_IN_MILLISECONDS = 1000L;
//    private long DEFAULT_MAX_WAIT_TIME = DEFAULT_INTERVAL_IN_MILLISECONDS * 5;
//
//    private DatabaseReference mFirebaseDatabase;
//    private FirebaseDatabase mFirebaseInstance;
//    String BookingID;
//    //IStorage storage;
//
//    // Variables needed to listen to location updates
//  //  private MovementActivity.TrackActivityLocationCallback callback = new MovementActivity.TrackActivityLocationCallback(this);
//    TextView Mylat, Mylng;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        Mapbox.getInstance(this, getString(R.string.lorem_ipsum4));
//        setContentView(R.layout.activity_movement);
//
//        final ActionBar abar = getSupportActionBar();
//        View viewActionBar = getLayoutInflater().inflate(R.layout.custom_actionbar, null);
//        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
//                ActionBar.LayoutParams.WRAP_CONTENT,
//                ActionBar.LayoutParams.MATCH_PARENT,
//                Gravity.CENTER);
//        TextView txttitle = (TextView) viewActionBar.findViewById(R.id.txtTitle);
//        txttitle.setText("O R D E R  T R A C K I N G");
//        txttitle.setTextSize(14);
//        if (abar != null) {
//            abar.setCustomView(viewActionBar, params);
//
//            abar.setDisplayShowCustomEnabled(true);
//            abar.setDisplayShowTitleEnabled(false);
//            abar.setHomeButtonEnabled(false);
//        }
//
//        //storage = new SharedPreferenceStorage(this, "MainActivityPrefs");
//        mFirebaseInstance = FirebaseDatabase.getInstance();
//
//        // get reference to 'users' node
//        mFirebaseDatabase = mFirebaseInstance.getReference("tracking");
//       // mFirebaseInstance.goOnline();
//        Mylat = (TextView) findViewById(R.id.latitude);
//        Mylng = (TextView) findViewById(R.id.longitude);
//
//
//        mapView = findViewById(R.id.mapView);
//        mapView.onCreate(savedInstanceState);
//        mapView.getMapAsync(this);
//
//        Intent intent = getIntent();
//        String BookingId = "T-0000000000055"; //intent.getStringExtra("BOOKINGID");
//
//
//        mFirebaseDatabase.child(BookingId).addValueEventListener(new ValueEventListener() {
//
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//
//                LocationTracking locationTracking = dataSnapshot.getValue(LocationTracking.class);
//                Mylat.setText(locationTracking.lat);
//                Mylng.setText(locationTracking.lng);
//                //useValue(locationTracking);
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//                Log.w(TAG, "Failed to read value.", databaseError.toException());
//            }
//        });
//
//
//        //  getValue(BookingId);
//
//
////        String lat = intent.getStringExtra("DesLat");
////        String lng = intent.getStringExtra("DesLng");
////        MovementModel mm = PrefUtils.getMovementCode(MovementActivity.this);
////        Log.d("Movement", mm.getLatitude());
////
////
////        Double SourceLatitude = ParseDouble(mm.getLatitude());
////        Double SourceLongitude = ParseDouble(mm.getLongitude());
////
////        Double DestinationLatitude = Double.parseDouble(lat);
////        Double DestinationLongitude = Double.parseDouble(lng);
////
////
////        Point destinationPoint = Point.fromLngLat(DestinationLongitude, DestinationLatitude);
////        Point originPoint = Point.fromLngLat(SourceLongitude, SourceLatitude);
//
//
//        // getRoute(originPoint, destinationPoint);
//    }
//
////    private void getValue(String BookingId) {
////
////        mFirebaseDatabase.child(BookingId).addListenerForSingleValueEvent(new ValueEventListener() {
////
////            @Override
////            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
////
////                LocationTracking locationTracking = dataSnapshot.getValue(LocationTracking.class);
////                useValue(locationTracking);
////            }
////
////            @Override
////            public void onCancelled(@NonNull DatabaseError databaseError) {
////                Log.w(TAG, "Failed to read value.", databaseError.toException());
////            }
////        });
////
////    }
////
////    // Use Your Value
////    private LocationTracking useValue(LocationTracking yourValue) {
////
////        Log.d("FESTUS", "countryNameCode: " + yourValue.lat);
////        // storage.store("latt", yourValue.lat);
////        // storage.store("lngg", yourValue.lng);
////        MovementModel model = new MovementModel();
////        model.setLongitude(yourValue.lng);
////        model.setLatitude(yourValue.lat);
////        PrefUtils.setMovementModel(model, MovementActivity.this);
////        return null;
////
////    }
//
//
//
//
//    public double ParseDouble(String strNumber) {
//        if (strNumber != null && strNumber.length() > 0) {
//            try {
//                return Double.parseDouble(strNumber);
//            } catch (Exception e) {
//                return -1;   // or some value to mark this field is wrong. or make a function validates field first ...
//            }
//        } else return 0;
//    }
//
//
//    @Override
//    public void onMapReady(@NonNull final MapboxMap mapboxMap) {
//        this.mapboxMap = mapboxMap;
//        mapboxMap.setStyle(getString(R.string.navigation_guidance_day), new Style.OnStyleLoaded() {
//            @Override
//            public void onStyleLoaded(@NonNull Style style) {
//                //enableLocationComponent(style);
//
//                style.addImage("marker-icon-id",
//                        BitmapFactory.decodeResource(
//                                MovementActivity.this.getResources(), R.drawable.car));
//
//                GeoJsonSource geoJsonSource = new GeoJsonSource("source-id", Feature.fromGeometry(
//                        Point.fromLngLat(ParseDouble(Mylat.getText().toString()), 6.4590)));
//                style.addSource(geoJsonSource);
//
//                SymbolLayer symbolLayer = new SymbolLayer("layer-id", "source-id");
//                symbolLayer.withProperties(
//                        PropertyFactory.iconImage("marker-icon-id"),
//                        iconAllowOverlap(true),
//               iconIgnorePlacement(true)
//                );
//                style.addLayer(symbolLayer);
//
//            }
//        });
//    }
//
////    private void addDestinationIconSymbolLayer(@NonNull Style loadedMapStyle) {
////        loadedMapStyle.addImage("destination-icon-id",
////                BitmapFactory.decodeResource(this.getResources(), R.drawable.car));
////        GeoJsonSource geoJsonSource = new GeoJsonSource("destination-source-id");
////        loadedMapStyle.addSource(geoJsonSource);
////        SymbolLayer destinationSymbolLayer = new SymbolLayer("destination-symbol-layer-id", "destination-source-id");
////        destinationSymbolLayer.withProperties(
////                iconImage("destination-icon-id"),
////                iconAllowOverlap(true),
////                iconIgnorePlacement(true)
////        );
////        loadedMapStyle.addLayer(destinationSymbolLayer);
////    }
//
////
////    private void getRoute(Point origin, Point destination) {
////        Log.d("Origin", origin.toJson());
////        NavigationRoute.builder(this)
////                .accessToken(Mapbox.getAccessToken())
////                .origin(origin)
////                .destination(destination)
////                .build()
////                .getRoute(new Callback<DirectionsResponse>() {
////                    @Override
////                    public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
////                        Log.d(TAG, "Response code: " + response.code());
////                        if (response.body() == null) {
////                            Log.e(TAG, "No routes found, make sure you set the right user and access token.");
////                            return;
////                        } else if (response.body().routes().size() < 1) {
////                            Log.e(TAG, "No routes found");
////                            return;
////                        }
////
////                        currentRoute = response.body().routes().get(0);
////                        if (navigationMapRoute != null) {
////                            navigationMapRoute.removeRoute();
////                        } else {
////                            navigationMapRoute = new NavigationMapRoute(null, mapView, mapboxMap, R.style.NavigationMapRoute);
////                        }
////                        navigationMapRoute.addRoute(currentRoute);
////                    }
////
////                    @Override
////                    public void onFailure(Call<DirectionsResponse> call, Throwable throwable) {
////                        Log.e(TAG, "Error: " + throwable.getMessage());
////                    }
////                });
////    }
//
//
////    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
////// Check if permissions are enabled and if not request
////        if (PermissionsManager.areLocationPermissionsGranted(this)) {
////
////// Get an instance of the component
////            locationComponent = mapboxMap.getLocationComponent();
////
////// Set the LocationComponent activation options
////            LocationComponentActivationOptions locationComponentActivationOptions =
////                    LocationComponentActivationOptions.builder(this, loadedMapStyle)
////                            .useDefaultLocationEngine(false)
////                            .build();
////
////// Activate with the LocationComponentActivationOptions object
////            locationComponent.activateLocationComponent(locationComponentActivationOptions);
////
////// Enable to make component visible
////            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
////                // TODO: Consider calling
////                //    ActivityCompat#requestPermissions
////                // here to request the missing permissions, and then overriding
////                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
////                //                                          int[] grantResults)
////                // to handle the case where the user grants the permission. See the documentation
////                // for ActivityCompat#requestPermissions for more details.
////                return;
////            }
////            locationComponent.setLocationComponentEnabled(true);
////
////// Set the component's camera mode
////            locationComponent.setCameraMode(CameraMode.TRACKING);
////
////// Set the component's render mode
////            locationComponent.setRenderMode(RenderMode.COMPASS);
////
////           initLocationEngine();
////        } else {
////            permissionsManager = new PermissionsManager(this);
////            permissionsManager.requestLocationPermissions(this);
////        }
////    }
//
//    @SuppressLint("MissingPermission")
//    private void initLocationEngine() {
//        locationEngine = LocationEngineProvider.getBestLocationEngine(this);
//
//        LocationEngineRequest request = new LocationEngineRequest.Builder(DEFAULT_INTERVAL_IN_MILLISECONDS)
//                .setPriority(LocationEngineRequest.PRIORITY_HIGH_ACCURACY)
//                .setMaxWaitTime(DEFAULT_MAX_WAIT_TIME).build();
//
//        locationEngine.requestLocationUpdates(request, callback, getMainLooper());
//        locationEngine.getLastLocation(callback);
//
//
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
//    }
//
//    @Override
//    public void onExplanationNeeded(List<String> permissionsToExplain) {
//        Toast.makeText(this, R.string.user_location_permission_explanation, Toast.LENGTH_LONG).show();
//    }
//
//    @Override
//    public void onPermissionResult(boolean granted) {
//        if (granted) {
//            if (mapboxMap.getStyle() != null) {
//                // enableLocationComponent(mapboxMap.getStyle());
//            }
//        } else {
//            Toast.makeText(this, R.string.user_location_permission_not_granted, Toast.LENGTH_LONG).show();
//            finish();
//        }
//    }
//
//    private static class TrackActivityLocationCallback
//            implements LocationEngineCallback<LocationEngineResult> {
//
//        private final WeakReference<MovementActivity> activityWeakReference;
//
//        TrackActivityLocationCallback(MovementActivity activity) {
//            this.activityWeakReference = new WeakReference<>(activity);
//        }
//
//        /**
//         * The LocationEngineCallback interface's method which fires when the device's location has changed.
//         *
//         * @param result the LocationEngineResult object which has the last known location within it.
//         */
//        @Override
//        public void onSuccess(LocationEngineResult result) {
//            MovementActivity activity = activityWeakReference.get();
//
//            if (activity != null) {
//
//                Location location = result.getLastLocation();
//
//                if (location == null) {
//                    return;
//                }
////                activity.createLocationUpdatesToFirebase(String.valueOf(result.getLastLocation().getLatitude()),String.valueOf(result.getLastLocation().getLongitude()), activity.BookingID);
////                Log.d("Updates1", String.valueOf(result.getLastLocation().getLatitude()));
////                Log.d("Updates2", String.valueOf(result.getLastLocation().getLongitude()));
//
//
////              Pass the new location to the Maps SDK's LocationComponent
//                if (activity.mapboxMap != null && result.getLastLocation() != null) {
//                    activity.mapboxMap.getLocationComponent().forceLocationUpdate(result.getLastLocation());
//                }
//            }
//        }
//
//        /**
//         * The LocationEngineCallback interface's method which fires when the device's location can not be captured
//         *
//         * @param exception the exception message
//         */
//        @Override
//        public void onFailure(@NonNull Exception exception) {
//            Log.d("LocationChangeActivity", exception.getLocalizedMessage());
//            MovementActivity activity = activityWeakReference.get();
//            if (activity != null) {
//                Toast.makeText(activity, exception.getLocalizedMessage(),
//                        Toast.LENGTH_SHORT).show();
//            }
//        }
//    }
//
//    @Override
//    protected void onStart() {
//        super.onStart();
//        mapView.onStart();
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        mapView.onResume();
//    }
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//        mapView.onPause();
//    }
//
//    @Override
//    protected void onStop() {
//        super.onStop();
//        mapView.onStop();
//    }
//
//    @Override
//    protected void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
//        mapView.onSaveInstanceState(outState);
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//// Prevent leaks
//        if (locationEngine != null) {
//            locationEngine.removeLocationUpdates(callback);
//        }
//        mapView.onDestroy();
//    }
//
//    @Override
//    public void onLowMemory() {
//        super.onLowMemory();
//        mapView.onLowMemory();
//    }
//    }

//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//
//    }
//}

