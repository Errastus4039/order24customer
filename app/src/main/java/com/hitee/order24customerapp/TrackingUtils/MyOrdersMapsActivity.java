package com.hitee.order24customerapp.TrackingUtils;

import android.Manifest;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.geofire.GeoFire;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.JointType;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.SquareCap;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hitee.order24customerapp.Common.Common;
import com.hitee.order24customerapp.Models.LocationTracking;
import com.hitee.order24customerapp.R;
import com.hitee.order24customerapp.Remote.IGoogleAPI;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyOrdersMapsActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private GoogleMap mMap;
    private static final int MY_PERMISSION_REQUEST_CODE = 7000;
    private static final int PLAY_SERVICE_RES_REQUEST = 7001;

    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;

    private static int UPDATE_INTERVAL = 5000;
    private static int FATEST_INTERVAL = 3000;
    private static int DISPLACEMENT = 10;

    DatabaseReference drivers;
    GeoFire geoFire;
    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;
    Marker mCurrent;
    private IGoogleAPI mService;
    SupportMapFragment mapFragment;


    Double SourceLatitude;
    Double SourceLongitude;
    //Car animation
    private List<LatLng> polyLineList;
    private Marker carMarker;
    private float v;
    private double lat, lng;
    private Handler handler;
    private LatLng startPosition, endposition, currentPosition;
    private int index, next;
    private Button btnGo;
    private EditText editPlace;
    private String destination;
    private PolylineOptions polylineOptions, blackPolylineOptions;
    private Polyline blackPolyline, greyPolyline;
    private static final String TAG = "MyOrdersMapsActivity";

    TextView Mylat, Mylng;
    Runnable drawPathRunnable = new Runnable() {
        @Override
        public void run() {
            if (index < polyLineList.size() - 1) {
                index++;
                next = index + 1;
            }
            if (index < polyLineList.size() - 1) {
                startPosition = polyLineList.get(index);
                endposition = polyLineList.get(next);
            }

            final ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
            valueAnimator.setDuration(3000);
            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    v = valueAnimator.getAnimatedFraction();
                    lng = v * endposition.longitude + (1 - v) * startPosition.longitude;
                    lat = v * endposition.latitude + (1 - v) * startPosition.latitude;

                    LatLng newPos = new LatLng(lat, lng);
                    carMarker.setPosition(newPos);
                    carMarker.setAnchor(0.5f, 0.5f);
                    carMarker.setRotation(getBearing(startPosition, newPos));
                    mMap.moveCamera(CameraUpdateFactory.newCameraPosition(
                            new CameraPosition.Builder()
                                    .target(newPos)
                                    .zoom(15.5f)
                                    .build()));
                }
            });

            valueAnimator.start();
            handler.postDelayed(this, 3000);

        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_orders_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.

        final ActionBar abar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.custom_actionbar, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        TextView txttitle = (TextView) viewActionBar.findViewById(R.id.txtTitle);
        txttitle.setText("C U R R E N T  O R D E R  L O C A T I O N");
        txttitle.setTextSize(12);
        if (abar != null) {
            abar.setCustomView(viewActionBar, params);

            abar.setDisplayShowCustomEnabled(true);
            abar.setDisplayShowTitleEnabled(false);
            abar.setHomeButtonEnabled(false);
            abar.setDisplayHomeAsUpEnabled(true);
        }


        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.frg);
        mapFragment.getMapAsync(this);

        //storage = new SharedPreferenceStorage(this, "MainActivityPrefs");
        mFirebaseInstance = FirebaseDatabase.getInstance();
        Mylat = (TextView) findViewById(R.id.latitude);
        Mylng = (TextView) findViewById(R.id.longitude);
        // get reference to 'users' node
        mFirebaseDatabase = mFirebaseInstance.getReference("tracking");
        Intent intent = getIntent();
        String BookingId = intent.getStringExtra("BOOKINGID");
        String lat = intent.getStringExtra("DesLat");
        String lng = intent.getStringExtra("DesLng");

        String DriverEmail = intent.getStringExtra("DriverEmail");
        String DriverPhone = intent.getStringExtra("DriverPhone");
        String DriverName = intent.getStringExtra("DriverName");

//        String StoreName = intent.getStringExtra("StoreName");
//        String StoreAddress = intent.getStringExtra("BusinessAddress");
//        String StorePhone = intent.getStringExtra("BusinessPhone");



        mFirebaseDatabase.child(BookingId).addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                mMap.clear();
                LocationTracking locationTracking = dataSnapshot.getValue(LocationTracking.class);
                if (locationTracking == null) {
                    Toast.makeText(getApplicationContext(), "Rider has accepted the request but the rider is yet to start the trip", Toast.LENGTH_LONG).show();
                } else {
                    Mylat.setText(locationTracking.lat);
                    Mylng.setText(locationTracking.lng);
                    currentPosition = new LatLng(ParseDouble(Mylat.getText().toString()), ParseDouble(Mylng.getText().toString()));
                    String requestApi = null;
                    try {
                        requestApi = "https://maps.googleapis.com/maps/api/directions/json?" +
                                "mode=driving&" +
                                "transit_routing_preference=less_driving&" +
                                "origin=" + currentPosition.latitude + "," + currentPosition.longitude + "&" +
                                "destination=" + Double.parseDouble(lat) + "," + Double.parseDouble(lng) + "&" +
                                "key=" + getResources().getString(R.string.google_direction_api);
                        Log.d("URL", requestApi);

                        mService.getPath(requestApi)
                                .enqueue(new Callback<String>() {
                                    @Override
                                    public void onResponse(Call<String> call, Response<String> response) {
                                        try {
                                            JSONObject jsonObject = new JSONObject(response.body().toString());
                                            JSONArray jsonArray = jsonObject.getJSONArray("routes");
                                            for (int i = 0; i < jsonArray.length(); i++) {
                                                JSONObject route = jsonArray.getJSONObject(i);
                                                JSONObject poly = route.getJSONObject("overview_polyline");
                                                String polyline = poly.getString("points");
                                                polyLineList = decodePoly(polyline);
                                            }

                                            LatLngBounds.Builder builder = new LatLngBounds.Builder();
                                            for (LatLng latLng : polyLineList)
                                                builder.include(latLng);
                                            LatLngBounds bounds = builder.build();
                                            CameraUpdate mCameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, 2);
                                            mMap.animateCamera(mCameraUpdate);

                                            polylineOptions = new PolylineOptions();
                                            polylineOptions.color(Color.GRAY);
                                            polylineOptions.width(5);
                                            polylineOptions.startCap(new SquareCap());
                                            polylineOptions.endCap(new SquareCap());
                                            polylineOptions.jointType(JointType.ROUND);
                                            polylineOptions.addAll(polyLineList);
                                            greyPolyline = mMap.addPolyline(polylineOptions);


                                            blackPolylineOptions = new PolylineOptions();
                                            blackPolylineOptions.color(Color.BLACK);
                                            blackPolylineOptions.width(5);
                                            blackPolylineOptions.startCap(new SquareCap());
                                            blackPolylineOptions.endCap(new SquareCap());
                                            blackPolylineOptions.jointType(JointType.ROUND);
                                            //   blackPolylineOptions.addAll(polyLineList);
                                            blackPolyline = mMap.addPolyline(blackPolylineOptions);

                                            mMap.addMarker(new MarkerOptions().position(polyLineList.get(polyLineList.size() - 1))
                                                    .title("Driver Location"));

                                            ValueAnimator polyLineAnimator = ValueAnimator.ofInt(0, 100);
                                            polyLineAnimator.setDuration(2000);
                                            polyLineAnimator.setInterpolator(new LinearInterpolator());
                                            polyLineAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                                                @Override
                                                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                                                    List<LatLng> points = greyPolyline.getPoints();
                                                    int percentValue = (int) valueAnimator.getAnimatedValue();
                                                    int size = points.size();
                                                    int newPoints = (int) (size * (percentValue / 100.0f));
                                                    List<LatLng> p = points.subList(0, newPoints);
                                                    blackPolyline.setPoints(p);
                                                }
                                            });
                                            polyLineAnimator.start();

                                            carMarker = mMap.addMarker(new MarkerOptions().position(currentPosition)
                                                    .flat(true)
                                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.car)));

                                            handler = new Handler();
                                            index = -1;
                                            next = 1;
                                            //handler.postDelayed(drawPathRunnable, 3000);

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<String> call, Throwable t) {
                                        Toast.makeText(MyOrdersMapsActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                });

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.w(TAG, "Failed to read value.", databaseError.toException());
            }
        });


        Button btnPreview = (Button) findViewById(R.id.preview);
        btnPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Mylat.getText().toString().equals("") && Mylng.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "You cannot preview the location of the rider at this moment until the rider start the trip", Toast.LENGTH_LONG).show();
                } else {

                    Geocoder geocoder;
                    List<Address> addresses;
                    geocoder = new Geocoder(MyOrdersMapsActivity.this, Locale.getDefault());

                    try {
                        addresses = geocoder.getFromLocation(ParseDouble(Mylat.getText().toString()), ParseDouble(Mylng.getText().toString()), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                        String myaddress = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                        String city = addresses.get(0).getLocality();
                        String state = addresses.get(0).getAdminArea();
                        String country = addresses.get(0).getCountryName();

                        ViewGroup viewGroup = findViewById(android.R.id.content);

                        //then we will inflate the custom alert dialog xml that we created
                        View dialogView = LayoutInflater.from(MyOrdersMapsActivity.this).inflate(R.layout.dialog, viewGroup, false);


                        TextView address = (TextView) dialogView.findViewById(R.id.address);
                        TextView _state = (TextView) dialogView.findViewById(R.id.state);
                        TextView _country = (TextView) dialogView.findViewById(R.id.country);
                        TextView _city = (TextView) dialogView.findViewById(R.id.city);
                        address.setText(myaddress);
                        _state.setText(state);
                        _country.setText(country);
                        _city.setText(city);

                        TextView driverEmail = (TextView) dialogView.findViewById(R.id.driverEmail);
                        TextView driverName = (TextView) dialogView.findViewById(R.id.driverName);
                        TextView driverPhone = (TextView) dialogView.findViewById(R.id.driverPhone);

                        driverPhone.setText(DriverPhone);
                        driverName.setText(DriverName);
                        driverEmail.setText(DriverEmail);



                        //Now we need an AlertDialog.Builder object
                        AlertDialog.Builder builder = new AlertDialog.Builder(MyOrdersMapsActivity.this);

                        //setting the view of the builder to our custom view that we already inflated
                        builder.setView(dialogView);

                        //finally creating the alert dialog and displaying it
                        AlertDialog alertDialog = builder.create();
                        alertDialog.show();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        polyLineList = new ArrayList<>();

        setUpLocation();
        //GEO

        mService = Common.getGoogleAPI();


    }


    double ParseDouble(String strNumber) {
        if (strNumber != null && strNumber.length() > 0) {
            try {
                return Double.parseDouble(strNumber);
            } catch (Exception e) {
                return -1;   // or some value to mark this field is wrong. or make a function validates field first ...
            }
        } else return 0;
    }


    private List decodePoly(String encoded) {

        List poly = new ArrayList();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }

    private void setUpLocation() {
        if (ActivityCompat.checkSelfPermission(MyOrdersMapsActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(MyOrdersMapsActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MyOrdersMapsActivity.this, new String[]{
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
            }, MY_PERMISSION_REQUEST_CODE);
        } else {
            if (checkPlayServices()) {
                buildGoogleApiClient();
                createLocationRequest();
                // displayLocation();
                //if (location_switch.isChecked())

            }
        }

    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FATEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
    }

    private void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(MyOrdersMapsActivity.this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode))
                GooglePlayServicesUtil.getErrorDialog(resultCode, MyOrdersMapsActivity.this, PLAY_SERVICE_RES_REQUEST).show();
            else {
                Toast.makeText(MyOrdersMapsActivity.this, "This device is not supported", Toast.LENGTH_LONG).show();
                finish();
            }
            return false;
        }
        return true;
    }

    private void stopLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);

    }

    private void displayLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
//            if (location_switch.isChecked()) {
            final double latitude = mLastLocation.getLatitude();
            final double longitude = mLastLocation.getLongitude();


            //IF SUCCESS POST TO API
            if (mCurrent != null)
                mCurrent.remove(); // Remove already marker
            mCurrent = mMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.car))
                    .position(new LatLng(latitude, longitude))
                    .title("Driver Location"));

            //MOVE CAMERA
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 15.0f));
            //dRAW animation
            // rotateMarker(mCurrent, -360, mMap);
//            }
        } else {
            Log.d("ERROR", "Cannot get your location");
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        // super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (checkPlayServices()) {
                        buildGoogleApiClient();
                        createLocationRequest();
//                        if (location_switch.isChecked())
//                            displayLocation();
                    }
                }
        }
    }

    private void rotateMarker(final Marker mCurrent, final float i, GoogleMap mMap) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final float startRotation = mCurrent.getRotation();
        final long duration = 1500;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed / duration);
                float rot = t * i + (1 - t) * startRotation;

                mCurrent.setRotation(-rot > 180 ? rot / 2 : rot);
                if (t < 1.0) {
                    handler.postDelayed(this, 16);
                }
            }
        });
    }


    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

    }

    private float getBearing(LatLng startPosition, LatLng endPosition) {
        double lat = Math.abs(startPosition.latitude - endPosition.latitude);
        double lng = Math.abs(startPosition.longitude - endPosition.longitude);

        if (startPosition.latitude < endPosition.latitude && startPosition.longitude < endPosition.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)));


        else if (startPosition.latitude >= endPosition.latitude && startPosition.longitude < endPosition.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);

        else if (startPosition.latitude >= endPosition.latitude && startPosition.longitude >= endPosition.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);

        else if (startPosition.latitude < endPosition.latitude && startPosition.longitude >= endPosition.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);

        return -1;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        try {

            boolean isSuccess = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(this, R.raw.uber_style_map)
            );

            if (!isSuccess)
                Log.d("ERROR", "Map style load failed");
        } catch (Resources.NotFoundException ex) {
            ex.printStackTrace();
        }

        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.setTrafficEnabled(true);
        mMap.setIndoorEnabled(false);
        mMap.setBuildingsEnabled(false);
        mMap.getUiSettings().setZoomControlsEnabled(true);


    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        // displayLocation();
        //startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        // displayLocation();
    }


}
