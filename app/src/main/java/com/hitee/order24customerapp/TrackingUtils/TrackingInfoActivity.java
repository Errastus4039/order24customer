//package com.hitee.order24customerapp.TrackingUtils;
//
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.content.res.Configuration;
//import android.location.Location;
//import android.preference.PreferenceManager;
//import android.support.annotation.IdRes;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.support.design.widget.BottomSheetBehavior;
//import android.support.design.widget.CoordinatorLayout;
//import android.support.design.widget.FloatingActionButton;
//import android.support.v7.app.AppCompatActivity;
//import android.os.Bundle;
//import android.support.v7.app.AppCompatDelegate;
//import android.text.SpannableString;
//import android.text.Spanned;
//import android.text.style.AbsoluteSizeSpan;
//import android.util.Log;
//import android.view.View;
//import android.widget.TextView;
//
//import com.google.firebase.database.ChildEventListener;
//import com.google.firebase.database.DataSnapshot;
//import com.google.firebase.database.DatabaseError;
//import com.google.firebase.database.DatabaseReference;
//import com.google.firebase.database.FirebaseDatabase;
//import com.google.firebase.database.Query;
//import com.google.firebase.database.ValueEventListener;
//import com.hitee.order24customerapp.Models.LocationTracking;
//import com.hitee.order24customerapp.R;
//
//
//import com.mapbox.api.directions.v5.models.BannerInstructions;
//import com.mapbox.api.directions.v5.models.DirectionsResponse;
//import com.mapbox.api.directions.v5.models.DirectionsRoute;
//import com.mapbox.geojson.Point;
//import com.mapbox.mapboxsdk.Mapbox;
//import com.mapbox.mapboxsdk.camera.CameraPosition;
//import com.mapbox.mapboxsdk.geometry.LatLng;
//import com.mapbox.services.android.navigation.ui.v5.NavigationView;
//import com.mapbox.services.android.navigation.ui.v5.NavigationViewOptions;
//import com.mapbox.services.android.navigation.ui.v5.OnNavigationReadyCallback;
//import com.mapbox.services.android.navigation.ui.v5.listeners.BannerInstructionsListener;
//import com.mapbox.services.android.navigation.ui.v5.listeners.InstructionListListener;
//import com.mapbox.services.android.navigation.ui.v5.listeners.NavigationListener;
//import com.mapbox.services.android.navigation.ui.v5.listeners.SpeechAnnouncementListener;
//import com.mapbox.services.android.navigation.ui.v5.voice.SpeechAnnouncement;
//import com.mapbox.services.android.navigation.v5.navigation.MapboxNavigation;
//import com.mapbox.services.android.navigation.v5.navigation.NavigationRoute;
//import com.mapbox.services.android.navigation.v5.routeprogress.ProgressChangeListener;
//import com.mapbox.services.android.navigation.v5.routeprogress.RouteProgress;
//
//import org.jetbrains.annotations.NotNull;
//
//import java.io.File;
//
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//import timber.log.Timber;
//
//import static java.lang.Double.parseDouble;
//
//
//public class TrackingInfoActivity extends AppCompatActivity implements OnNavigationReadyCallback,
//        NavigationListener, ProgressChangeListener, InstructionListListener,
//        BannerInstructionsListener {
//
//    private static Point ORIGIN = null;//= Point.fromLngLat(3.4556132999999727, 6.4163352);
//    private static Point DESTINATION = null;//
//    private static final int INITIAL_ZOOM = 16;
//
//    private NavigationView navigationView;
//    private View spacer;
//    private TextView speedWidget;
//    private FloatingActionButton fabNightModeToggle;
//
//    private boolean bottomSheetVisible = true;
//    private boolean instructionListShown = false;
//
//    private DatabaseReference mFirebaseDatabase;
//    private FirebaseDatabase mFirebaseInstance;
//    private static final String TAG = TrackingInfoActivity.class.getSimpleName();
//    LocationTracking locationTracking;
//    TextView Mylat, Mylng;
//
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        setTheme(R.style.Theme_AppCompat_Light_NoActionBar);
//        initNightMode();
//        super.onCreate(savedInstanceState);
//        Mapbox.getInstance(this, getString(R.string.lorem_ipsum4));
//        setContentView(R.layout.activity_tracking_info);
//
//        navigationView = findViewById(R.id.navigationView);
//        fabNightModeToggle = findViewById(R.id.fabToggleNightMode);
//        speedWidget = findViewById(R.id.speed_limit);
//        spacer = findViewById(R.id.spacer);
//        setSpeedWidgetAnchor(R.id.summaryBottomSheet);
//
//        locationTracking = new LocationTracking();
//        Mylat = (TextView) findViewById(R.id.latitude);
//        Mylng = (TextView) findViewById(R.id.longitude);
//
//        mFirebaseInstance = FirebaseDatabase.getInstance();
//        Intent i = getIntent();
//        String BookingId = i.getStringExtra("BOOKINGID");
//
//        // get reference to 'users' node
//        mFirebaseDatabase = mFirebaseInstance.getReference("tracking");
//
//        getValue(BookingId);
//
//        String lat = i.getStringExtra("DesLat");
//        String lng = i.getStringExtra("DesLng");
//
//
//        ORIGIN = Point.fromLngLat(Double.parseDouble("0"), Double.parseDouble("0"));
//        DESTINATION = Point.fromLngLat(Double.parseDouble(lng), Double.parseDouble(lat));
//
//
//        CameraPosition initialPosition = new CameraPosition.Builder()
//                .target(new LatLng(ORIGIN.latitude(), ORIGIN.longitude()))
//                .zoom(INITIAL_ZOOM)
//                .build();
//
//        navigationView.onCreate(savedInstanceState);
//        navigationView.initialize(this, initialPosition);
//
//    }
//
//    private void getValue(String BookingId) {
//
//        mFirebaseDatabase.child(BookingId).addListenerForSingleValueEvent(new ValueEventListener() {
//
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//
//                LocationTracking locationTracking = dataSnapshot.getValue(LocationTracking.class);
//                useValue(locationTracking);
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//                Log.w(TAG, "Failed to read value.", databaseError.toException());
//            }
//        });
//
//    }
//
//    // Use Your Value
//    private LocationTracking useValue(LocationTracking yourValue) {
//
//        Log.d("FESTUS", "countryNameCode: " + yourValue.lat);
//
//        return locationTracking;
//
//    }
//
//    @Override
//    public void onNavigationReady(boolean isRunning) {
//        fetchRoute();
//    }
//
//    @Override
//    public void onStart() {
//        super.onStart();
//        navigationView.onStart();
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        navigationView.onResume();
//    }
//
//    @Override
//    public void onLowMemory() {
//        super.onLowMemory();
//        navigationView.onLowMemory();
//    }
//
//    @Override
//    public void onBackPressed() {
//// If the navigation view didn't need to do anything, call super
//        if (!navigationView.onBackPressed()) {
//            super.onBackPressed();
//        }
//    }
//
//    @Override
//    protected void onSaveInstanceState(Bundle outState) {
//        navigationView.onSaveInstanceState(outState);
//        super.onSaveInstanceState(outState);
//    }
//
//    @Override
//    protected void onRestoreInstanceState(Bundle savedInstanceState) {
//        super.onRestoreInstanceState(savedInstanceState);
//        navigationView.onRestoreInstanceState(savedInstanceState);
//    }
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        navigationView.onPause();
//    }
//
//    @Override
//    public void onStop() {
//        super.onStop();
//        navigationView.onStop();
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        navigationView.onDestroy();
//        if (isFinishing()) {
//            saveNightModeToPreferences(AppCompatDelegate.MODE_NIGHT_AUTO);
//            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_AUTO);
//        }
//    }
//
//    @Override
//    public void onCancelNavigation() {
//// Navigation canceled, finish the activity
//        finish();
//    }
//
//    @Override
//    public void onNavigationFinished() {
//// Intentionally empty
//    }
//
//    @Override
//    public void onNavigationRunning() {
//// Intentionally empty
//    }
//
//    @Override
//    public void onProgressChange(Location location, RouteProgress routeProgress) {
//        setSpeed(location);
//    }
//
//    @Override
//    public void onInstructionListVisibilityChanged(boolean shown) {
//        instructionListShown = shown;
//        speedWidget.setVisibility(shown ? View.GONE : View.VISIBLE);
//        if (instructionListShown) {
//            fabNightModeToggle.hide();
//        } else if (bottomSheetVisible) {
//            fabNightModeToggle.show();
//        }
//    }
//
//
//    @Override
//    public BannerInstructions willDisplay(BannerInstructions instructions) {
//        return instructions;
//    }
//
//    private void startNavigation(DirectionsRoute directionsRoute) {
//        NavigationViewOptions.Builder options =
//                NavigationViewOptions.builder()
//                        .navigationListener(this)
//                        .directionsRoute(directionsRoute)
//                        .shouldSimulateRoute(false)
//                        .progressChangeListener(this);
//        setBottomSheetCallback(options);
//        setupNightModeFab();
//
//        navigationView.startNavigation(options.build());
//    }
//
//    private void fetchRoute() {
//        //call the firebase database here
//        NavigationRoute.builder(this)
//                .accessToken("pk.eyJ1IjoicGhlc3R1czQwMzkxIiwiYSI6ImNqd2J2ZG81cDBuMXIzem1tZWRkOXh4ZXcifQ.78EXz57VQEbzIZd5EEmOsA")
//                .origin(ORIGIN)
//                .destination(DESTINATION)
//                .alternatives(true)
//                .build()
//                .getRoute(new Callback<DirectionsResponse>() {
//                    @Override
//                    public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
//                        DirectionsRoute directionsRoute = response.body().routes().get(0);
//                        startNavigation(directionsRoute);
//                    }
//
//                    @Override
//                    public void onFailure(Call<DirectionsResponse> call, Throwable t) {
//
//                    }
//                });
//    }
//
//
//    /**
//     * Sets the anchor of the spacer for the speed widget, thus setting the anchor for the speed widget
//     * (The speed widget is anchored to the spacer, which is there because padding between items and
//     * their anchors in CoordinatorLayouts is finicky.
//     *
//     * @param res resource for view of which to anchor the spacer
//     */
//    private void setSpeedWidgetAnchor(@IdRes int res) {
//        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) spacer.getLayoutParams();
//        layoutParams.setAnchorId(res);
//        spacer.setLayoutParams(layoutParams);
//    }
//
//    private void setBottomSheetCallback(NavigationViewOptions.Builder options) {
//        options.bottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
//            @Override
//            public void onStateChanged(@NonNull View bottomSheet, int newState) {
//                switch (newState) {
//                    case BottomSheetBehavior.STATE_HIDDEN:
//                        bottomSheetVisible = false;
//                        fabNightModeToggle.hide();
//                        setSpeedWidgetAnchor(R.id.recenterBtn);
//                        break;
//                    case BottomSheetBehavior.STATE_EXPANDED:
//                        bottomSheetVisible = true;
//                        break;
//                    case BottomSheetBehavior.STATE_SETTLING:
//                        if (!bottomSheetVisible) {
//                            // View needs to be anchored to the bottom sheet before it is finished expanding
//                            // because of the animation
//                            fabNightModeToggle.show();
//                            setSpeedWidgetAnchor(R.id.summaryBottomSheet);
//                        }
//                        break;
//                    default:
//                        return;
//                }
//            }
//
//            @Override
//            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
//            }
//        });
//    }
//
//    private void setupNightModeFab() {
//        fabNightModeToggle.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                toggleNightMode();
//            }
//        });
//    }
//
//    private void toggleNightMode() {
//        int currentNightMode = getCurrentNightMode();
//        alternateNightMode(currentNightMode);
//    }
//
//    private void initNightMode() {
//        int nightMode = retrieveNightModeFromPreferences();
//        AppCompatDelegate.setDefaultNightMode(nightMode);
//    }
//
//    private int getCurrentNightMode() {
//        return getResources().getConfiguration().uiMode
//                & Configuration.UI_MODE_NIGHT_MASK;
//    }
//
//    private void alternateNightMode(int currentNightMode) {
//        int newNightMode;
//        if (currentNightMode == Configuration.UI_MODE_NIGHT_YES) {
//            newNightMode = AppCompatDelegate.MODE_NIGHT_NO;
//        } else {
//            newNightMode = AppCompatDelegate.MODE_NIGHT_YES;
//        }
//        saveNightModeToPreferences(newNightMode);
//        recreate();
//    }
//
//    private int retrieveNightModeFromPreferences() {
//        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
//        return preferences.getInt(getString(R.string.current_night_mode), AppCompatDelegate.MODE_NIGHT_AUTO);
//    }
//
//    private void saveNightModeToPreferences(int nightMode) {
//        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
//        SharedPreferences.Editor editor = preferences.edit();
//        editor.putInt(getString(R.string.current_night_mode), nightMode);
//        editor.apply();
//    }
//
//    private void setSpeed(Location location) {
//        String string = String.format("%d\nMPH", (int) (location.getSpeed() * 2.2369));
//        int mphTextSize = getResources().getDimensionPixelSize(R.dimen.mph_text_size);
//        int speedTextSize = getResources().getDimensionPixelSize(R.dimen.speed_text_size);
//
//        SpannableString spannableString = new SpannableString(string);
//        spannableString.setSpan(new AbsoluteSizeSpan(mphTextSize),
//                string.length() - 4, string.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
//
//        spannableString.setSpan(new AbsoluteSizeSpan(speedTextSize),
//                0, string.length() - 3, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
//
//        speedWidget.setText(spannableString);
//        if (!instructionListShown) {
//            speedWidget.setVisibility(View.VISIBLE);
//        }
//    }
//}
