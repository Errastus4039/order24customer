package com.hitee.order24customerapp.TrackingUtils;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.hitee.order24customerapp.Adapter.CheckoutAddOnAdapter;
import com.hitee.order24customerapp.Adapter.OrderListAdapter;
import com.hitee.order24customerapp.Models.AddOnList;
import com.hitee.order24customerapp.Models.OrderDetailModel;
import com.hitee.order24customerapp.Models.UserModel;
import com.hitee.order24customerapp.OrderUtils.OrderConfirmationActivity;
import com.hitee.order24customerapp.OrderUtils.OrderListActivity;
import com.hitee.order24customerapp.R;
import com.hitee.order24customerapp.ShoppingUtils.ShoppingCartActivity;
import com.hitee.order24customerapp.Utils.Constant;
import com.hitee.order24customerapp.Utils.PrefUtils;
import com.hitee.order24customerapp.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class TrackingListActivity extends AppCompatActivity {

    private ArrayList<OrderDetailModel> rowListItem;
    private RecyclerView rView;

    OrderListAdapter rcAdapter;
    TextView txtPrice, subtotal, txtCounter;
    private ArrayList<AddOnList> addOnLists;

    private RecyclerView AddOnrView;
    CheckoutAddOnAdapter checkoutAddOnAdapter;

    String Latitude;
    String Longitude;
    String DriverPhone;
    String DriverEmail;
    String DriverName;
    String BusinessPhone;
    String StoreName;
    String BusinessAddress;
    TextView store_phone, store_address, store_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracking_list);


        final ActionBar abar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.custom_actionbar, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        TextView txttitle = (TextView) viewActionBar.findViewById(R.id.txtTitle);
        txttitle.setText("T R A C K I N G  O R D E R  L I S T");
        txttitle.setTextSize(14);
        if (abar != null) {
            abar.setCustomView(viewActionBar, params);

            abar.setDisplayShowCustomEnabled(true);
            abar.setDisplayShowTitleEnabled(false);
            abar.setHomeButtonEnabled(false);
            abar.setDisplayHomeAsUpEnabled(true);
        }


        String OrderId = getIntent().getStringExtra("getOrderId");
        String getBookingID = getIntent().getStringExtra("getBookingID");
        String getOrderValue = getIntent().getStringExtra("getOrderValue");
        String getBusinessAddress = getIntent().getStringExtra("getBusinessAddress");
        String getBusinessID = getIntent().getStringExtra("getBusinessID");
        String getBusinessPhone = getIntent().getStringExtra("getBusinessPhone");
        String getCustomerId = getIntent().getStringExtra("getCustomerId");
        String getCustomerPhone = getIntent().getStringExtra("getCustomerPhone");
        String getCustomerName = getIntent().getStringExtra("getCustomerName");
        String getCreatedDate = getIntent().getStringExtra("getCreatedDate");
        //Toast.makeText(this, getCustomerName, Toast.LENGTH_SHORT).show();
        UserModel userModel = PrefUtils.getCurrentUserModel(TrackingListActivity.this);


        //final TextView name = (TextView) findViewById(R.id.name);
        final TextView history = (TextView) findViewById(R.id.history);
        final TextView order_at_time = (TextView) findViewById(R.id.order_at_time);
        final TextView delivery_type = (TextView) findViewById(R.id.delivery_type);
        final TextView deliver_address = (TextView) findViewById(R.id.deliver_address);

        store_phone = (TextView) findViewById(R.id.store_phone);
        store_address = (TextView) findViewById(R.id.store_address);
        store_name = (TextView) findViewById(R.id.store_name);

        // name.setText(getCustomerName);
        //  history.setText(getBusinessPhone);
        order_at_time.setText(getCreatedDate);
        deliver_address.setText(getBusinessAddress);
        delivery_type.setText(getBookingID);


        String URL = Constant.GET_ORDER_LIST + "/" + OrderId; //userModel.getId();
        Log.d("URL", URL);
        rowListItem = new ArrayList<>();
        getAllItemList(URL);


        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        rView = (RecyclerView) findViewById(R.id.recycle_view);
        rView.setHasFixedSize(false);
        rView.setLayoutManager(layoutManager);
        rView.setNestedScrollingEnabled(false);
        rcAdapter = new OrderListAdapter(this, rowListItem);
        rView.setAdapter(rcAdapter);


        String URL_ADDON = Constant.GET_ORDER_LIST_ADD_ONS + "/" + OrderId;
        addOnLists = new ArrayList<>();
        getAllAddOnItemList(URL_ADDON);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);


        AddOnrView = (RecyclerView) findViewById(R.id.recycle_view_addon);
        AddOnrView.setHasFixedSize(false);


        AddOnrView.setLayoutManager(layoutManager1);
        AddOnrView.setNestedScrollingEnabled(false);
        checkoutAddOnAdapter = new CheckoutAddOnAdapter(this, addOnLists);
        AddOnrView.setAdapter(checkoutAddOnAdapter);


        Button btnTrack = (Button) findViewById(R.id.btnTrack);
        btnTrack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ii = new Intent(TrackingListActivity.this, MyOrdersMapsActivity.class);
                ii.putExtra("BOOKINGID", getBookingID);
                ii.putExtra("DesLat", Latitude);
                ii.putExtra("DesLng", Longitude);
                ii.putExtra("DriverEmail", DriverEmail);
                ii.putExtra("DriverName", DriverName);
                ii.putExtra("DriverPhone", DriverPhone);
                ii.putExtra("BusinessPhone", BusinessPhone);
                ii.putExtra("StoreName", StoreName);
                ii.putExtra("BusinessAddress", BusinessAddress);
                startActivity(ii);
            }
        });


    }


    private void getAllItemList(String url) {
        final ProgressDialog progress = new ProgressDialog(TrackingListActivity.this);
        progress.setTitle("O R D E R  L I S T");
        progress.setMessage("Loading...");
        progress.show();


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null,
                new Response.Listener<JSONObject>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(JSONObject response) {
                        progress.dismiss();
                        Log.d("LL", response.toString());
                        try {
                            Latitude = response.getString("Latitude");
                            Longitude = response.getString("Longitude");
                            DriverEmail = response.getString("DriverEmail");
                            DriverName = response.getString("DriverName");
                            DriverPhone = response.getString("DriverPhone");

                            store_address.setText(response.getString("BusinessAddress"));
                            store_phone.setText(response.getString("BusinessPhone"));
                            store_name.setText(response.getString("StoreName"));

                            JSONArray genreArry = response.getJSONArray("OrderDetails");
                            for (int i = 0; i < genreArry.length(); i++) {

                                try {
                                    final JSONObject obj = genreArry.getJSONObject(i);
                                    OrderDetailModel mydic = new OrderDetailModel();
                                    mydic.setProductId(obj.getString("ProductId"));
                                    mydic.setProductImage(obj.getString("ProductImage"));
                                    mydic.setPrice(obj.getString("Price"));
                                    mydic.setQuantity(obj.getString("Quantity"));
                                    mydic.setStatus(obj.getString("Status"));
                                    mydic.setProductName(obj.getString("ProductName"));
                                    rowListItem.add(mydic);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                            rcAdapter.notifyDataSetChanged();


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error in getting json
                Log.e("TAG", "Error: " + error.getMessage());
                progress.dismiss();
                Toast.makeText(getApplicationContext(), "Opps! Something went wrong, please try again later ", Toast.LENGTH_SHORT).show();
            }
        });


        AppController.getInstance().addToRequestQueue(jsonObjReq);

    }

    private void getAllAddOnItemList(String url) {


        JsonArrayRequest request = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        //progress.dismiss();
                        Log.d("LL", response.toString());
                        try {
                            if (response.toString().equals("[]")) {
                                //     Toast.makeText(TrackingListActivity.this, "There is no addon found.", Toast.LENGTH_LONG).show();

                                return;
                            }

                            for (int i = 0; i < response.length(); i++) {

                                try {
                                    final JSONObject obj = response.getJSONObject(i);

                                    AddOnList mydic = new AddOnList();
                                    mydic.setId(obj.getString("Id"));
                                    mydic.setName(obj.getString("Name"));
                                    mydic.setQuantity(obj.getString("Quantity"));
                                    mydic.setTotal(obj.getString("Total"));

                                    addOnLists.add(mydic);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            checkoutAddOnAdapter.notifyDataSetChanged();
                            // stop animating Shimmer and hide the layout

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        checkoutAddOnAdapter.notifyDataSetChanged();

                        // stop animating Shimmer and hide the layout

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error in getting json

                //  progress.dismiss();
                Toast.makeText(getApplicationContext(), "Opps! Something went wrong, please try again later ", Toast.LENGTH_SHORT).show();
            }
        });

        AppController.getInstance().addToRequestQueue(request);

    }


}
