//package com.hitee.order24customerapp.TrackingUtils;
//
//import android.graphics.BitmapFactory;
//import android.os.Bundle;
//import android.os.Handler;
//import android.support.annotation.NonNull;
//import android.support.v7.app.AppCompatActivity;
//
//import com.google.android.gms.maps.model.LatLng;
//import com.hitee.order24customerapp.R;
//import com.mapbox.api.directions.v5.MapboxDirections;
//import com.mapbox.geojson.Feature;
//import com.mapbox.geojson.Point;
//import com.mapbox.mapboxsdk.Mapbox;
//import com.mapbox.mapboxsdk.maps.MapView;
//import com.mapbox.mapboxsdk.maps.MapboxMap;
//import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
//import com.mapbox.mapboxsdk.maps.Style;
//import com.mapbox.mapboxsdk.style.layers.PropertyFactory;
//import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
//import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
//
//import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconAllowOverlap;
//import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconIgnorePlacement;
//
//public class TestMapActivity extends AppCompatActivity implements OnMapReadyCallback {
//
//    private MapView mapView;
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        Mapbox.getInstance(this, getString(R.string.lorem_ipsum4));
//        setContentView(R.layout.activity_test_map);
//        mapView = findViewById(R.id.mapView);
//        mapView.onCreate(savedInstanceState);
//        mapView.getMapAsync(this);
//    }
//
//    // Add the mapView's own lifecycle methods to the activity's lifecycle methods
//    @Override
//    public void onStart() {
//        super.onStart();
//        mapView.onStart();
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        mapView.onResume();
//    }
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        mapView.onPause();
//    }
//
//    @Override
//    public void onStop() {
//        super.onStop();
//        mapView.onStop();
//    }
//
//    @Override
//    public void onLowMemory() {
//        super.onLowMemory();
//        mapView.onLowMemory();
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        mapView.onDestroy();
//    }
//
//    @Override
//    protected void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
//        mapView.onSaveInstanceState(outState);
//    }
//
//    @Override
//    public void onMapReady(@NonNull final MapboxMap mapboxMap) {
//
////
//        mapboxMap.setStyle(Style.LIGHT, new Style.OnStyleLoaded() {
//            @Override
//            public void onStyleLoaded(@NonNull Style style) {
//// Add the marker image to map
//                style.addImage("marker-icon-id",
//                        BitmapFactory.decodeResource(
//                                TestMapActivity.this.getResources(), R.drawable.car));
//
//
//                GeoJsonSource geoJsonSource = new GeoJsonSource("source-id", Feature.fromGeometry(
//                        Point.fromLngLat(3.5979, 6.4590)));
//                style.addSource(geoJsonSource);
//
//                SymbolLayer symbolLayer = new SymbolLayer("layer-id", "source-id");
//                symbolLayer.withProperties(
//                        PropertyFactory.iconImage("marker-icon-id"),
//                        iconAllowOverlap(true),
//                        iconIgnorePlacement(true)
//                );
//                style.addLayer(symbolLayer);
//
//            }
//        });
//
//
//    }
//}