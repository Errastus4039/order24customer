package com.hitee.order24customerapp.AddressUtils;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.hitee.order24customerapp.Adapter.AddressAdapter;
import com.hitee.order24customerapp.Adapter.MenuAdapter;
import com.hitee.order24customerapp.Adapter.ProductAdapter;
import com.hitee.order24customerapp.Bottom.CustomerActivity;
import com.hitee.order24customerapp.BusinessUtils.BusinessMenuActivity;
import com.hitee.order24customerapp.LoginUtils.LoginActivity;
import com.hitee.order24customerapp.Models.AddressModel;
import com.hitee.order24customerapp.Models.MenuModel;
import com.hitee.order24customerapp.Models.ProductModel;
import com.hitee.order24customerapp.Models.UserModel;
import com.hitee.order24customerapp.OrderUtils.OrderConfirmationActivity;
import com.hitee.order24customerapp.ProductsUtils.ProductListActivity;
import com.hitee.order24customerapp.R;
import com.hitee.order24customerapp.Utils.Constant;
import com.hitee.order24customerapp.Utils.PrefUtils;
import com.hitee.order24customerapp.Utils.RecyclerTouchListener;
import com.hitee.order24customerapp.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AddressActivity extends AppCompatActivity {
    private static final String TAG = ProductListActivity.class.getSimpleName();
    private RecyclerView recyclerView;
    private ArrayList<AddressModel> cartList;
    private AddressAdapter rcAdapter1;
    private ShimmerFrameLayout mShimmerViewContainer;
    String CategoryId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);


        final ActionBar abar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.custom_actionbar, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        TextView txttitle = (TextView) viewActionBar.findViewById(R.id.txtTitle);
        txttitle.setText("A D D R E S S E S");
        txttitle.setTextSize(14);
        if (abar != null) {
            abar.setCustomView(viewActionBar, params);

            abar.setDisplayShowCustomEnabled(true);
            abar.setDisplayShowTitleEnabled(false);
            abar.setHomeButtonEnabled(false);
            abar.setDisplayHomeAsUpEnabled(true);
        }
        UserModel userModel = PrefUtils.getCurrentUserModel(AddressActivity.this);
        if (userModel == null) {

            startActivity(new Intent(AddressActivity.this, LoginActivity.class));
            finish();
        } else {

            recyclerView = findViewById(R.id.recycle_view);
            cartList = new ArrayList<>();
            rcAdapter1 = new AddressAdapter(AddressActivity.this, cartList);


            FloatingActionButton floatingActionButton = (FloatingActionButton) findViewById(R.id.fab);
            floatingActionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(AddressActivity.this, AddNewAddressActivity.class);
                    //intent.putExtra("BusinessId", BusinessId);
                    startActivity(intent);

                }
            });


            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(rcAdapter1);
            recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

            fetchMenu(Constant.GET_ADDRESS + "/" + userModel.getId());
            mShimmerViewContainer = findViewById(R.id.shimmer_view_container);

            recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerView, new RecyclerTouchListener.ClickListener() {
                @Override
                public void onClick(View view, int position) {
                    final AddressModel doc = cartList.get(position);
                    Intent i = new Intent(AddressActivity.this, AddressInfoActivity.class);
                    i.putExtra("getId", doc.getId());
                    i.putExtra("getAddress", doc.getAddress());
                    i.putExtra("getCustomerId", doc.getCustomerId());
                    i.putExtra("getDoor", doc.getDoor());
                    i.putExtra("getLandmark", doc.getLandmark());
                    i.putExtra("getLatitude", doc.getLatitude());
                    i.putExtra("getLongitude", doc.getLongitude());
                    i.putExtra("getAddresstype", doc.getAddresstype());
                    startActivity(i);
                }

                @Override
                public void onLongClick(View view, int position) {

                }
            }));
        }

    }

    private void fetchMenu(String URL) {

        Log.d("MENU", URL);
        final ProgressDialog progress = new ProgressDialog(AddressActivity.this);
        progress.setTitle("A D D R E S S E S");
        progress.setMessage("Loading...");
        progress.show();
        progress.setCancelable(false);

        JsonArrayRequest request = new JsonArrayRequest(URL,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        progress.dismiss();
                        Log.d("LL", response.toString());
                        try {
                            if (response.toString().equals("[]")) {
                                Toast.makeText(AddressActivity.this, "There is no address found.", Toast.LENGTH_LONG).show();
                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);
                                return;
                            }

                            for (int i = 0; i < response.length(); i++) {

                                try {
                                    final JSONObject obj = response.getJSONObject(i);

                                    AddressModel mydic = new AddressModel();
                                    mydic.setId(obj.getString("Id"));
                                    mydic.setAddress(obj.getString("Address"));
                                    mydic.setCustomerId(obj.getString("CustomerId"));
                                    mydic.setLatitude(obj.getString("latitude"));
                                    mydic.setLongitude(obj.getString("longitude"));
                                    mydic.setLandmark(obj.getString("landmark"));
                                    mydic.setDoor(obj.getString("door"));
                                    mydic.setAddresstype(obj.getString("addresstype"));

                                    cartList.add(mydic);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            rcAdapter1.notifyDataSetChanged();
                            // stop animating Shimmer and hide the layout
                            mShimmerViewContainer.stopShimmerAnimation();
                            mShimmerViewContainer.setVisibility(View.GONE);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        rcAdapter1.notifyDataSetChanged();

                        // stop animating Shimmer and hide the layout
                        mShimmerViewContainer.stopShimmerAnimation();
                        mShimmerViewContainer.setVisibility(View.GONE);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error in getting json
                Log.e(TAG, "Error: " + error.getMessage());
                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
                progress.dismiss();
                Toast.makeText(getApplicationContext(), "Opps! Something went wrong, please try again later ", Toast.LENGTH_SHORT).show();
            }
        });

        AppController.getInstance().addToRequestQueue(request);


    }

    @Override
    public void onResume() {
        super.onResume();
       // mShimmerViewContainer.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        //mShimmerViewContainer.stopShimmerAnimation();
        super.onPause();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                Intent ii = new Intent(AddressActivity.this, CustomerActivity.class);
                startActivity(ii);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}
