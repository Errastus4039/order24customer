package com.hitee.order24customerapp.AddressUtils;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.hitee.order24customerapp.Bottom.CustomerActivity;
import com.hitee.order24customerapp.Models.CartModel;
import com.hitee.order24customerapp.Models.UserModel;
import com.hitee.order24customerapp.OrderUtils.OrderConfirmationActivity;
import com.hitee.order24customerapp.R;
import com.hitee.order24customerapp.RegisterUtils.RegisterActivity;
import com.hitee.order24customerapp.Utils.AlertDialogManager;
import com.hitee.order24customerapp.Utils.Constant;
import com.hitee.order24customerapp.Utils.PrefUtils;
import com.hitee.order24customerapp.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

public class AddressInfoActivity extends AppCompatActivity {

    AlertDialogManager alertDialogManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_info);

        final ActionBar abar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.custom_actionbar, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        TextView txttitle = (TextView) viewActionBar.findViewById(R.id.txtTitle);
        txttitle.setText("A D D R E S S  D E T A I L S");
        txttitle.setTextSize(14);
        if (abar != null) {
            abar.setCustomView(viewActionBar, params);

            abar.setDisplayShowCustomEnabled(true);
            abar.setDisplayShowTitleEnabled(false);
            abar.setHomeButtonEnabled(false);
            abar.setDisplayHomeAsUpEnabled(true);
        }
        alertDialogManager = new AlertDialogManager();

        TextView address = (TextView) findViewById(R.id.address);
        TextView addtype = (TextView) findViewById(R.id.addtype);
        TextView phonumber = (TextView) findViewById(R.id.phonumber);
        TextView addtype2 = (TextView) findViewById(R.id.addtype2);
        TextView email = (TextView) findViewById(R.id.email);
        TextView addtype3 = (TextView) findViewById(R.id.addtype3);
        TextView landmark = (TextView) findViewById(R.id.landmark);
        TextView addtype4 = (TextView) findViewById(R.id.addtype4);

        UserModel model = PrefUtils.getCurrentUserModel(AddressInfoActivity.this);

        String getId = getIntent().getStringExtra("getId");
        String getAddress = getIntent().getStringExtra("getAddress");
        String getCustomerId = getIntent().getStringExtra("getCustomerId");
        String getDoor = getIntent().getStringExtra("getDoor");
        String getLandmark = getIntent().getStringExtra("getLandmark");
        String getLatitude = getIntent().getStringExtra("getLatitude");
        String getLongitude = getIntent().getStringExtra("getLongitude");
        String getAddresstype = getIntent().getStringExtra("getAddresstype");


        address.setText(getAddress);
        addtype.setText(getAddresstype);
        addtype4.setText(getAddresstype);
        addtype3.setText(getAddresstype);
        addtype2.setText(getAddresstype);

        email.setText(model.getEmail());
        landmark.setText(getLandmark);
        phonumber.setText(model.getPhoneNumber());

        Button btnRemove = (Button) findViewById(R.id.btnRemove);

        final ProgressDialog waitingDialog = new ProgressDialog(this);
        waitingDialog.setTitle("Address");
        waitingDialog.setMessage("Please wait while we delete the address");
        final String url = Constant.DELETE_ADDRESS + "/" + getId;

        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                waitingDialog.show();
                JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                        url, null,
                        new Response.Listener<JSONObject>() {
                            @SuppressLint("SetTextI18n")
                            @Override
                            public void onResponse(JSONObject response) {
                                waitingDialog.dismiss();
                                Log.d("LL", response.toString());
                                try {
                                    if (response.getString("result").equals("200")) {
                                        alertDialogManager.showAlertDialog(AddressInfoActivity.this, "Success", "The address have been deleted", true);
                                    } else {
                                        alertDialogManager.showAlertDialog(AddressInfoActivity.this, "Failed", "Opps!!!, Please try again.", false);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error in getting json
                        Log.e("TAG", "Error: " + error.getMessage());

                        waitingDialog.dismiss();
                        Toast.makeText(AddressInfoActivity.this, "Opps! Something went wrong, please try again later ", Toast.LENGTH_SHORT).show();
                    }
                });

                AppController.getInstance().addToRequestQueue(jsonObjReq);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                Intent ii = new Intent(AddressInfoActivity.this, CustomerActivity.class);
                startActivity(ii);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
