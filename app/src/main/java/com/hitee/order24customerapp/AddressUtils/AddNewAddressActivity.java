package com.hitee.order24customerapp.AddressUtils;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.hitee.order24customerapp.Bottom.CustomerActivity;
import com.hitee.order24customerapp.LoginUtils.LoginActivity;
import com.hitee.order24customerapp.Models.UserModel;
import com.hitee.order24customerapp.OrderUtils.OrderConfirmationActivity;
import com.hitee.order24customerapp.ProductsUtils.ProductListActivity;
import com.hitee.order24customerapp.R;
import com.hitee.order24customerapp.ShoppingUtils.ShoppingInformationActivity;
import com.hitee.order24customerapp.Utils.AlertDialogManager;
import com.hitee.order24customerapp.Utils.Constant;
import com.hitee.order24customerapp.Utils.PrefUtils;


import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.api.Places;
import com.hitee.order24customerapp.app.AppController;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class AddNewAddressActivity extends AppCompatActivity {
    String TAG = "placeautocomplete";
    TextView txtView;
    String placename;

    LatLng latLng;
    EditText door, addresstype, landmark;
    AlertDialogManager alertDialogManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_address);

        final ActionBar abar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.custom_actionbar, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        TextView txttitle = (TextView) viewActionBar.findViewById(R.id.txtTitle);
        txttitle.setText("A D D  N E W  A D D R E S S");
        txttitle.setTextSize(14);
        if (abar != null) {
            abar.setCustomView(viewActionBar, params);
            abar.setDisplayShowCustomEnabled(true);
            abar.setDisplayShowTitleEnabled(false);
            abar.setHomeButtonEnabled(false);
            abar.setDisplayHomeAsUpEnabled(true);
        }
        alertDialogManager = new AlertDialogManager();

        Intent ii = getIntent();
        String shp = ii.getStringExtra("ShoppingInformation");
        String TotalPrice = ii.getStringExtra("TotalPrice");

        final UserModel userModel = PrefUtils.getCurrentUserModel(AddNewAddressActivity.this);
        if (userModel == null || userModel.equals("null")) {

            startActivity(new Intent(AddNewAddressActivity.this, LoginActivity.class));
        } else {


            txtView = findViewById(R.id.txtView);
            door = (EditText) findViewById(R.id.door);
            addresstype = (EditText) findViewById(R.id.addresstype);
            landmark = (EditText) findViewById(R.id.landmark);

            // Initialize Places.
            Places.initialize(getApplicationContext(), "AIzaSyDxslmBCBD8ECpQKZ-AJIfxRFbUt7kmndA");
            // Create a new Places client instance.
            PlacesClient placesClient = Places.createClient(this);

            // Initialize the AutocompleteSupportFragment.
            AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment)
                    getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);

            // Specify the types of place data to return.
            autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG));

            // Set up a PlaceSelectionListener to handle the response.
            autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
                @Override
                public void onPlaceSelected(Place place) {
                    // TODO: Get info about the selected place.
                    txtView.setText(place.getName());
                    placename = place.getName();
                    latLng = place.getLatLng();


                    Log.i(TAG, "Place: " + latLng.latitude);
                }

                @Override
                public void onError(Status status) {
                    // TODO: Handle the error.
                    Log.i(TAG, "An error occurred: " + status);
                }
            });

            Button btnAddAddress = (Button) findViewById(R.id.btnadd_address);
            btnAddAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (latLng == null) {
                        alertDialogManager.showAlertDialog(AddNewAddressActivity.this, "Error", "You need to search for your closest address", false);
                    } else if (landmark.getText().toString().equals("")) {
                        alertDialogManager.showAlertDialog(AddNewAddressActivity.this, "Error", "Kindly add a landmark", false);
                    } else {
                        final ProgressDialog progress = new ProgressDialog(AddNewAddressActivity.this);
                        progress.setTitle("A D D  A D D R E S S");
                        progress.setMessage("Loading...");
                        progress.show();

                        StringRequest strReq = new StringRequest(Request.Method.POST, Constant.POST_ADDRESS,
                                new com.android.volley.Response.Listener<String>() {

                                    @Override
                                    public void onResponse(String response) {
                                        Log.d("TAGGG", String.valueOf(response));
                                        progress.dismiss();
                                        try {
                                            if (response.equals("200")) {
                                                alertDialogManager.showAlertDialog(AddNewAddressActivity.this, "Success", "Address saved successfully", true);
                                                if (shp.equals("")) {
                                                    startActivity(new Intent(AddNewAddressActivity.this, AddressActivity.class));
                                                    landmark.setText("");
                                                    door.setText("");
                                                    addresstype.setText("");
                                                } else {
                                                    Intent ii = new Intent(AddNewAddressActivity.this, ShoppingInformationActivity.class);
                                                    ii.putExtra("TotalPrice", TotalPrice);
                                                    startActivity(ii);
                                                    finish();

                                                }

                                            } else {
                                                alertDialogManager.showAlertDialog(AddNewAddressActivity.this, "Failed", "Opps!, Error occurred while saving address", false);
                                            }

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    }
                                }, new com.android.volley.Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                                NetworkResponse response = error.networkResponse;
                                if (error instanceof ServerError && response != null) {

                                }
                            }
                        }) {

                            @Override
                            protected Map<String, String> getParams() {

                                Map<String, String> params = new HashMap<String, String>();
                                params.put("latitude", String.valueOf(latLng.latitude));
                                params.put("longitude", String.valueOf(latLng.longitude));
                                params.put("DoorOrFlatNo", door.getText().toString());
                                params.put("Landmark", landmark.getText().toString());
                                params.put("Address", placename);
                                params.put("AddressType", addresstype.getText().toString());
                                params.put("CustomerId", userModel.getId());

                                Log.d("POST_TAG", params.toString());
                                return params;
                            }
                        };
                        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                        strReq.setRetryPolicy(retryPolicy);
                        AppController.getInstance().addToRequestQueue(strReq);
                    }
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                Intent ii = new Intent(AddNewAddressActivity.this, CustomerActivity.class);
                startActivity(ii);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
