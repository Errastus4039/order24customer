package com.hitee.order24customerapp.Adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.hitee.order24customerapp.AnimationDir.MyAnimation;
import com.hitee.order24customerapp.Models.AddOnModel;
import com.hitee.order24customerapp.Models.ProductModel;
import com.hitee.order24customerapp.Models.UserModel;
import com.hitee.order24customerapp.ProductsUtils.ProductClickListener;
import com.hitee.order24customerapp.R;
import com.hitee.order24customerapp.ShoppingUtils.ShoppingCartActivity;
import com.hitee.order24customerapp.Utils.Constant;
import com.hitee.order24customerapp.Utils.Formmatting;
import com.hitee.order24customerapp.Utils.PrefUtils;
import com.hitee.order24customerapp.app.AppController;
import com.tubb.smrv.SwipeHorizontalMenuLayout;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AddOnAdapter extends RecyclerView.Adapter<AddOnAdapter.ItemViewHolder> {
    private static ArrayList<AddOnModel> dataList;
    private LayoutInflater mInflater;
    private Context context;
    private ProductClickListener clicklistener = null;

    public AddOnAdapter(Context ctx, ArrayList<AddOnModel> data) {
        context = ctx;
        dataList = data;
        mInflater = LayoutInflater.from(ctx);
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView image;
        private SwipeHorizontalMenuLayout sml;
        private CardView buttonMinus, buttonPlus;
        private ImageView buttonEdit, buttonDelete;
        private TextView title, numItem, price, productiontime;

        public ItemViewHolder(View itemView) {
            super(itemView);

            image = (ImageView) itemView.findViewById(R.id.add_on_btn);
            title = (TextView) itemView.findViewById(R.id.title);
            price = (TextView) itemView.findViewById(R.id.price);
            numItem = (TextView) itemView.findViewById(R.id.numItem);
            sml = (SwipeHorizontalMenuLayout) itemView.findViewById(R.id.sml);
            buttonMinus = (CardView) itemView.findViewById(R.id.buttonMinus);
            buttonPlus = (CardView) itemView.findViewById(R.id.buttonPlus);
            buttonEdit = (ImageView) itemView.findViewById(R.id.buttonEdit);
            buttonDelete = (ImageView) itemView.findViewById(R.id.delete);
            buttonMinus.setOnClickListener(this);
            buttonPlus.setOnClickListener(this);
            image.setOnClickListener(this);
            //  buttonDelete.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.buttonMinus:
                    int num = Integer.parseInt(numItem.getText().toString());
                    if (num > 1) {
                        num--;
                    }
                    numItem.setText(Integer.toString(num));
                    break;
                case R.id.buttonPlus:
                    int num2 = Integer.parseInt(numItem.getText().toString());
                    num2++;
                    numItem.setText(Integer.toString(num2));
                    break;

                case R.id.add_on_btn:
                    AddOnItemCart(String.valueOf(dataList.get(getAdapterPosition()).getId()),  String.valueOf(numItem.getText().toString()), String.valueOf(dataList.get(getAdapterPosition()).getBusinessAddOnId()));

                    break;
                default:
//                    if (clicklistener != null) {
//                        clicklistener.itemClicked(view, getAdapterPosition());
//                    }
                    break;
            }
        }
    }

    public void setClickListener(ProductClickListener listener) {
        this.clicklistener = listener;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_on_row, parent, false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        holder.title.setText(dataList.get(position).getName());
        holder.price.setText("₦ " + Formmatting.priceToString(Double.parseDouble(dataList.get(position).getPrice())));

        MyAnimation.animateYearbookUser(holder);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    private void AddOnItemCart(final String product_id,final String quantity, final  String businessAddonId) {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Cart");
        progressDialog.setMessage("Please wait while we remove the cart");
        progressDialog.show();
        final UserModel model = PrefUtils.getCurrentUserModel(context);
        // String url = Constant.DELETE_CARTS;
        StringRequest strReq = new StringRequest(Request.Method.POST, Constant.ADD_ADD_ON,
                new com.android.volley.Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.d("TAGGG", String.valueOf(response));
                        progressDialog.dismiss();
                        try {
                            //JSONObject object = new JSONObject(response);
                            //String result = object.getString("result");
                            if (response.equals("200")) {
                                Toast.makeText(context, "The item have been added", Toast.LENGTH_LONG).show();
//                                Intent ii = new Intent(context, ShoppingCartActivity.class);
//                                context.startActivity(ii);
//                                ((Activity)context).finish();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {

                }
            }
        }) {

            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("customer_id", model.getId());
                params.put("Id", product_id);
                params.put("Quantity", quantity);
                params.put("businessAddonId", businessAddonId);

                Log.d("POST_TAG", params.toString());
                return params;
            }
        };
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        strReq.setRetryPolicy(retryPolicy);
        AppController.getInstance().addToRequestQueue(strReq);

    }

}
