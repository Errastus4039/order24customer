package com.hitee.order24customerapp.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.hitee.order24customerapp.Bottom.CustomerActivity;
import com.hitee.order24customerapp.LoginUtils.LoginActivity;
import com.hitee.order24customerapp.Models.BusinessModel;
import com.hitee.order24customerapp.Models.CartModel;
import com.hitee.order24customerapp.Models.UserModel;
import com.hitee.order24customerapp.ProductsUtils.ProductListActivity;
import com.hitee.order24customerapp.R;
import com.hitee.order24customerapp.ShoppingUtils.ShoppingCartActivity;
import com.hitee.order24customerapp.ShoppingUtils.ShoppingClickListener;
import com.hitee.order24customerapp.Utils.Constant;
import com.hitee.order24customerapp.Utils.PrefUtils;
import com.hitee.order24customerapp.app.AppController;
import com.tubb.smrv.SwipeHorizontalMenuLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Wahyu on 06/08/2015.
 */
public class ShoppingAdapter extends RecyclerView.Adapter<ShoppingAdapter.ItemViewHolder> {
    private static ArrayList<CartModel> dataList;
    private LayoutInflater mInflater;
    private Context context;
    private ShoppingClickListener clicklistener = null;

    public ShoppingAdapter(Context ctx, ArrayList<CartModel> data) {
        context = ctx;
        dataList = data;
        mInflater = LayoutInflater.from(context);
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView imgProduct;
        private SwipeHorizontalMenuLayout sml;
        private CardView buttonMinus, buttonPlus;
        private ImageView buttonEdit, buttonDelete;
        private TextView title, numItem, price, productiontime;

        public ItemViewHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            title = (TextView) itemView.findViewById(R.id.title);
            price = (TextView) itemView.findViewById(R.id.price);
            productiontime = (TextView) itemView.findViewById(R.id.productiontime);
            numItem = (TextView) itemView.findViewById(R.id.numItem);
            imgProduct = (ImageView) itemView.findViewById(R.id.imgProduct);
            sml = (SwipeHorizontalMenuLayout) itemView.findViewById(R.id.sml);
            buttonMinus = (CardView) itemView.findViewById(R.id.buttonMinus);
            buttonPlus = (CardView) itemView.findViewById(R.id.buttonPlus);
            buttonEdit = (ImageView) itemView.findViewById(R.id.buttonEdit);
            buttonDelete = (ImageView) itemView.findViewById(R.id.delete);
            buttonMinus.setOnClickListener(this);
            buttonPlus.setOnClickListener(this);
            buttonEdit.setOnClickListener(this);
            buttonDelete.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.buttonMinus:
                    int num = Integer.parseInt(numItem.getText().toString());
                    if (num > 1) {
                        num--;
                    }
                    numItem.setText(Integer.toString(num));
                    break;
                case R.id.buttonPlus:
                    int num2 = Integer.parseInt(numItem.getText().toString());
                    num2++;
                    numItem.setText(Integer.toString(num2));
                    break;
                case R.id.buttonEdit:
                    Toast.makeText(context, "Button Edit clicked!", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.delete:
                    DeleteItemCart(String.valueOf(dataList.get(getAdapterPosition()).getProductId()));
                    onItemDismiss(getAdapterPosition());

                    break;
                default:
                    if (clicklistener != null) {
                        clicklistener.itemClicked(view, getAdapterPosition());
                    }
                    break;
            }
        }
    }

    public void setClickListener(ShoppingClickListener listener) {
        this.clicklistener = listener;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_shopping, parent, false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        return itemViewHolder;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final ItemViewHolder holder, final int position) {
        boolean swipeEnable = true;
        final CartModel album = dataList.get(position);
        //Toast.makeText(context, album.getId(), Toast.LENGTH_SHORT).show();
        holder.sml.setSwipeEnable(swipeEnable);

        Glide.with(context)
                .load(Constant.IMAGE_URL + dataList.get(position).getImageProductUrl())
                .apply(new RequestOptions()
                        .placeholder(R.mipmap.fff)
                        .fitCenter())
                .into(holder.imgProduct);


        holder.title.setText(dataList.get(position).getName());
        holder.price.setText("₦ " + dataList.get(position).getPrice());
        holder.numItem.setText(dataList.get(position).getQuantity());
        holder.productiontime.setText(dataList.get(position).getProductionTime());
    }


    @Override
    public int getItemCount() {
        return dataList.size();
    }

    private void onItemDismiss(int position) {

        dataList.remove(position);

        notifyItemRemoved(position);
    }

    private void DeleteItemCart(final String product_id) {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Cart");
        progressDialog.setMessage("Please wait while we remove the cart");
        progressDialog.show();
        final UserModel model = PrefUtils.getCurrentUserModel(context);
        // String url = Constant.DELETE_CARTS;
        StringRequest strReq = new StringRequest(Request.Method.POST, Constant.DELETE_CARTS,
                new com.android.volley.Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.d("TAGGG", String.valueOf(response));
                        progressDialog.dismiss();
                        try {
                            JSONObject object = new JSONObject(response);
                            String result = object.getString("result");
                            if (result.equals("OK")) {
                                Toast.makeText(context, "The item have been deleted", Toast.LENGTH_LONG).show();
                                Intent ii = new Intent(context, ShoppingCartActivity.class);
                                context.startActivity(ii);
                                ((Activity)context).finish();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {

                }
            }
        }) {

            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("CustomerId", model.getId());
                params.put("ProductId", product_id);

                Log.d("POST_TAG", params.toString());
                return params;
            }
        };
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        strReq.setRetryPolicy(retryPolicy);
        AppController.getInstance().addToRequestQueue(strReq);

    }


}
