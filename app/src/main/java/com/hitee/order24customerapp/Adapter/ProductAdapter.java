package com.hitee.order24customerapp.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.hitee.order24customerapp.AnimationDir.MyAnimation;
import com.hitee.order24customerapp.Models.ProductModel;
import com.hitee.order24customerapp.ProductsUtils.ProductClickListener;
import com.hitee.order24customerapp.R;
import com.hitee.order24customerapp.Utils.Constant;

import java.util.ArrayList;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ItemViewHolder> {
    private static ArrayList<ProductModel> dataList;
    private LayoutInflater mInflater;
    private Context context;
    private ProductClickListener clicklistener = null;

    public ProductAdapter(Context ctx, ArrayList<ProductModel> data) {
        context = ctx;
        dataList = data;
        mInflater = LayoutInflater.from(ctx);
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private ImageView image;
        private TextView title, price;

        public ItemViewHolder(View itemView) {
            super(itemView);

            image = (ImageView) itemView.findViewById(R.id.imageMain);
            title = (TextView) itemView.findViewById(R.id.title);
            price = (TextView) itemView.findViewById(R.id.price);
        }


    }

    public void setClickListener(ProductClickListener listener) {
        this.clicklistener = listener;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_row, parent, false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        holder.title.setText(dataList.get(position).getName());
        holder.price.setText("₦ " + dataList.get(position).getPrice());

        Log.d("JI", Constant.IMAGE_URL + dataList.get(position).getThumbnail());
//        Glide.with(context).load(Constant.IMAGE_URL + dataList.get(position).getThumbnail())
//                .placeholder(R.mipmap.placeholder)
//                .dontAnimate()
//                .thumbnail(0.5f)
//                .crossFade()
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .into(holder.image);

        Glide.with(context)
                .load(Constant.IMAGE_URL + dataList.get(position).getThumbnail())
                .apply(new RequestOptions()
                        .placeholder(R.mipmap.fff)
                        .fitCenter())
                .into(holder.image);


        MyAnimation.animateYearbookUser(holder);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}
