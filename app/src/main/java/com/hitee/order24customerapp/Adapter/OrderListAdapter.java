package com.hitee.order24customerapp.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.hitee.order24customerapp.AnimationDir.MyAnimation;
import com.hitee.order24customerapp.Models.OrderDetailModel;
import com.hitee.order24customerapp.Models.OrderHistoryModel;
import com.hitee.order24customerapp.R;
import com.hitee.order24customerapp.Utils.Constant;

import java.util.ArrayList;

/**
 * Created by Wahyu on 06/08/2015.
 */
public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.ItemViewHolder> {
    private static ArrayList<OrderDetailModel> dataList;
    private LayoutInflater mInflater;
    private Context context;

    public OrderListAdapter(Context ctx, ArrayList<OrderDetailModel> data) {
        context = ctx;
        dataList = data;
        mInflater = LayoutInflater.from(context);
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgProduct;
        private TextView title, quantity, price, status, bookingId;

        public ItemViewHolder(View itemView) {
            super(itemView);

            title = (TextView) itemView.findViewById(R.id.title);
            price = (TextView) itemView.findViewById(R.id.price);
            quantity = (TextView) itemView.findViewById(R.id.quantity);
            status = (TextView) itemView.findViewById(R.id.status);
            imgProduct = (ImageView) itemView.findViewById(R.id.imageMain);
        }

    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.orders, parent, false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder holder, final int position) {

        holder.title.setText(dataList.get(position).getProductName());
        holder.price.setText("Price: " + " ₦ " + dataList.get(position).getPrice());
        holder.quantity.setText("Quantity : " + dataList.get(position).getQuantity());
        holder.status.setText(dataList.get(position).getStatus());

        Glide.with(context)
                .load(Constant.IMAGE_URL + dataList.get(position).getProductImage())
                .apply(new RequestOptions()
                        .placeholder(R.mipmap.fff)
                        .fitCenter())
                .into(holder.imgProduct);


        MyAnimation.animateYearbookUser(holder);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

}
