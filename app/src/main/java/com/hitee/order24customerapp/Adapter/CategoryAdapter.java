package com.hitee.order24customerapp.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;


import com.hitee.order24customerapp.AnimationDir.MyAnimation;
import com.hitee.order24customerapp.Models.Category;
import com.hitee.order24customerapp.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyViewHolder> {

    private List<Category> bookingSearchModelList;
    Bitmap bitmap;
    private LayoutInflater mInflater;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, description, timestamp, chef, price, status;
        public ImageView imageView;
        public CardView leftLayout;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            description = (TextView) view.findViewById(R.id.desc);
            leftLayout = (CardView) view.findViewById(R.id.leftLayout);
        }
    }


    public CategoryAdapter(Context mContext, List<Category> bookingSearchModelList) {
        context = mContext;
        this.bookingSearchModelList = bookingSearchModelList;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_ecommerce, parent, false);
        return new MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        final Category album = bookingSearchModelList.get(position);
        holder.title.setText(album.getCategoryName().toUpperCase());
        holder.description.setText(album.getDescription());
        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(90), rnd.nextInt(90), rnd.nextInt(90));
        holder.leftLayout.setCardBackgroundColor(color);
        MyAnimation.animateFindACorper(holder);

    }

    @Override
    public int getItemCount() {
        // Log.d("TAG old", String.valueOf(yearbookList.size()));
        return bookingSearchModelList.size();
    }


}

