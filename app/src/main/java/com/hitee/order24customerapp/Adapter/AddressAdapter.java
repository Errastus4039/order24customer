package com.hitee.order24customerapp.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hitee.order24customerapp.AnimationDir.MyAnimation;
import com.hitee.order24customerapp.Models.AddressModel;
import com.hitee.order24customerapp.Models.MenuModel;
import com.hitee.order24customerapp.R;

import java.util.ArrayList;

/**
 * Created by Wahyu on 06/08/2015.
 */
public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.ItemViewHolder> {
    private static ArrayList<AddressModel> dataList;
    private LayoutInflater mInflater;
    private Context context;

    public AddressAdapter(Context ctx, ArrayList<AddressModel> data) {
        context = ctx;
        dataList = data;
        mInflater = LayoutInflater.from(context);
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private TextView name, review, time;

        public ItemViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.name);
            review = (TextView) itemView.findViewById(R.id.review);
            time = (TextView) itemView.findViewById(R.id.time);
        }

    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.full_address_list, parent, false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder holder, final int position) {
        holder.name.setText(dataList.get(position).getAddress());
        if (dataList.get(position).getDoor().equals("null")) {

        } else {
            holder.review.setText(dataList.get(position).getDoor());
        }
        holder.time.setText(dataList.get(position).getLandmark());

        MyAnimation.animateYearbookUser(holder);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

}
