package com.hitee.order24customerapp.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hitee.order24customerapp.AnimationDir.MyAnimation;
import com.hitee.order24customerapp.Models.MenuModel;
import com.hitee.order24customerapp.R;

import java.util.ArrayList;

/**
 * Created by Wahyu on 06/08/2015.
 */
public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.ItemViewHolder> {
    private static ArrayList<MenuModel> dataList;
    private LayoutInflater mInflater;
    private Context context;

    public MenuAdapter(Context ctx, ArrayList<MenuModel> data) {
        context = ctx;
        dataList = data;
        mInflater = LayoutInflater.from(context);
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private TextView name, review, time;

        public ItemViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.name);
            review = (TextView) itemView.findViewById(R.id.review);
            time = (TextView) itemView.findViewById(R.id.time);
        }

    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.menu_list, parent, false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder holder, final int position) {
        holder.name.setText(dataList.get(position).getMenu());
        if (dataList.get(position).getDescription().equals("null")) {

            holder.review.setText(dataList.get(position).getMenu());
            holder.review.setTextSize(12);
        } else {
            holder.review.setText(dataList.get(position).getDescription());
        }
        holder.time.setText(dataList.get(position).getDate());

        MyAnimation.animateYearbookUser(holder);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

}
