package com.hitee.order24customerapp.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.Image;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.hitee.order24customerapp.AnimationDir.MyAnimation;
import com.hitee.order24customerapp.Models.BusinessModel;
import com.hitee.order24customerapp.Models.Category;
import com.hitee.order24customerapp.R;
import com.hitee.order24customerapp.Utils.Constant;

import java.util.List;
import java.util.Random;

public class BusinessAdapter extends RecyclerView.Adapter<BusinessAdapter.MyViewHolder> {

    private List<BusinessModel> bookingSearchModelList;
    Bitmap bitmap;
    private LayoutInflater mInflater;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, description, star, kilometer, mins;
        public de.hdodenhof.circleimageview.CircleImageView imageView;
        public TextView timestamp;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            description = (TextView) view.findViewById(R.id.chef);
            imageView = (de.hdodenhof.circleimageview.CircleImageView) view.findViewById(R.id.thumbnail);
            timestamp = (TextView) view.findViewById(R.id.timestamp);
            star = (TextView) view.findViewById(R.id.star);
            kilometer = (TextView) view.findViewById(R.id.kilometer);
            mins = (TextView) view.findViewById(R.id.mins);
        }
    }


    public BusinessAdapter(Context mContext, List<BusinessModel> bookingSearchModelList) {
        context = mContext;
        this.bookingSearchModelList = bookingSearchModelList;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.search, parent, false);
        return new MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        final BusinessModel album = bookingSearchModelList.get(position);
        holder.name.setText(album.getBusinessName().toUpperCase());
        if (album.getDescription() == "null") {
            holder.description.setText(album.getBusinessName());
            holder.description.setTextSize(12);
        } else {
            holder.description.setText(album.getDescription());
        }
        if (album.getPhoneNumber() == "null") {
            //holder.timestamp.setText(album.getPhoneNumber());
        } else {
            holder.timestamp.setText(album.getPhoneNumber());
        }


        holder.star.setText(album.getCategory());
        holder.kilometer.setText(album.getDistance());
        holder.mins.setText(album.getDuration());

        Glide.with(context)
                .load(Constant.STORELOGO + album.getLogo())
                .apply(new RequestOptions()
                        .placeholder(R.mipmap.fff)
                        .fitCenter())
                .into(holder.imageView);


        MyAnimation.animateYearbookUser(holder);
    }

    @Override
    public int getItemCount() {
        // Log.d("TAG old", String.valueOf(yearbookList.size()));
        return Math.min(bookingSearchModelList.size(), 20);
    }


}

