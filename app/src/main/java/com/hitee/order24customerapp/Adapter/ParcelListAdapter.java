package com.hitee.order24customerapp.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hitee.order24customerapp.AnimationDir.MyAnimation;
import com.hitee.order24customerapp.Models.ParcelListModel;
import com.hitee.order24customerapp.Models.WalletModel;
import com.hitee.order24customerapp.R;

import java.util.ArrayList;

public class ParcelListAdapter extends RecyclerView.Adapter<ParcelListAdapter.ItemViewHolder> {
    private static ArrayList<ParcelListModel> dataList;
    private LayoutInflater mInflater;
    private Context context;

    public ParcelListAdapter(Context ctx, ArrayList<ParcelListModel> data) {
        context = ctx;
        dataList = data;
        mInflater = LayoutInflater.from(context);
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private TextView name, ratingBar, nn, mm, review, status;

        public ItemViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.name);
            ratingBar = (TextView) itemView.findViewById(R.id.ratingBar);
            nn = (TextView) itemView.findViewById(R.id.nn);
            mm = (TextView) itemView.findViewById(R.id.mm);
            review = (TextView) itemView.findViewById(R.id.review);
            status = (TextView) itemView.findViewById(R.id.status);

        }

    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.parcel_list_row, parent, false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder holder, final int position) {

        holder.name.setText(dataList.get(position).getItemName());
        holder.ratingBar.setText(dataList.get(position).getContactNo());
        holder.nn.setText(dataList.get(position).getItemDesc());
        holder.mm.setText(dataList.get(position).getDeliveryAddress());
        holder.review.setText(dataList.get(position).getPickup());
        if (dataList.get(position).getStatus().equals("1")) {

            holder.status.setText("DRIVER NOTIFICATION");
        } else if (dataList.get(position).getStatus().equals("2")) {

            holder.status.setText("DRIVER ACCEPTED");
        }
        MyAnimation.animateYearbookUser(holder);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

}
