package com.hitee.order24customerapp.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hitee.order24customerapp.AnimationDir.MyAnimation;
import com.hitee.order24customerapp.Models.MenuModel;
import com.hitee.order24customerapp.Models.WalletModel;
import com.hitee.order24customerapp.R;
import com.hitee.order24customerapp.Utils.Formmatting;

import java.util.ArrayList;

/**
 * Created by Wahyu on 06/08/2015.
 */
public class WalletAdapter extends RecyclerView.Adapter<WalletAdapter.ItemViewHolder> {
    private static ArrayList<WalletModel> dataList;
    private LayoutInflater mInflater;
    private Context context;

    public WalletAdapter(Context ctx, ArrayList<WalletModel> data) {
        context = ctx;
        dataList = data;
        mInflater = LayoutInflater.from(context);
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private TextView txRef, balance, desc, time;

        public ItemViewHolder(View itemView) {
            super(itemView);

            txRef = (TextView) itemView.findViewById(R.id.txRef);
            desc = (TextView) itemView.findViewById(R.id.desc);
            time = (TextView) itemView.findViewById(R.id.time);
            balance = (TextView) itemView.findViewById(R.id.balance);
        }

    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.wallet_list, parent, false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder holder, final int position) {
        holder.txRef.setText("Trans ID: " + dataList.get(position).getTxRef());
        if (dataList.get(position).getDescription().equals("CREDIT")) {
            holder.desc.setText(dataList.get(position).getDescription());
            holder.desc.setTextColor(Color.parseColor("#279b37"));
        }
        if (dataList.get(position).getDescription().equals("DEBIT")) {
            holder.desc.setText(dataList.get(position).getDescription());
            holder.desc.setTextColor(Color.parseColor("RED"));
        }
        holder.time.setText(dataList.get(position).getDate());
        holder.balance.setText("₦" + Formmatting.priceToString(Double.valueOf(dataList.get(position).getAmount())));
        MyAnimation.animateYearbookUser(holder);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

}
