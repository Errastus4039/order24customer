package com.hitee.order24customerapp.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.hitee.order24customerapp.AnimationDir.MyAnimation;
import com.hitee.order24customerapp.Models.AddressInfoModel;
import com.hitee.order24customerapp.Models.AddressModel;
import com.hitee.order24customerapp.R;
import com.hitee.order24customerapp.Utils.PrefUtils;

import java.util.ArrayList;

/**
 * Created by Wahyu on 06/08/2015.
 */
public class DeliveryAddressAdapter extends RecyclerView.Adapter<DeliveryAddressAdapter.ItemViewHolder> {
    private static ArrayList<AddressModel> dataList;
    private LayoutInflater mInflater;
    private Context context;
    private int lastSelectedPosition = -1;


    public DeliveryAddressAdapter(Context ctx, ArrayList<AddressModel> data) {
        context = ctx;
        dataList = data;
        mInflater = LayoutInflater.from(context);
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private TextView address, review, time;
        private RadioButton radioButton;

        public ItemViewHolder(View itemView) {
            super(itemView);

            address = (TextView) itemView.findViewById(R.id.address);
            radioButton = (RadioButton) itemView.findViewById(R.id.shippingCost1);

            radioButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastSelectedPosition = getAdapterPosition();
                    notifyDataSetChanged();

                    AddressInfoModel model = new AddressInfoModel();
                    model.setId(address.getText().toString());
                    PrefUtils.setAddress(model, context);
//                    Toast.makeText(DeliveryAddressAdapter.this.context,
//                            "selected offer is " + address.getText(),
//                            Toast.LENGTH_LONG).show();
                }
            });
        }

    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.address_list, parent, false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder holder, final int position) {
        holder.address.setText(dataList.get(position).getId());
        holder.radioButton.setText(dataList.get(position).getAddress());

        holder.radioButton.setChecked(lastSelectedPosition == position);
        MyAnimation.animateYearbookUser(holder);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

}
