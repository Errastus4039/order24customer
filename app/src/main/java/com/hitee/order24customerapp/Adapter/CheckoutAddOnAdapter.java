package com.hitee.order24customerapp.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.hitee.order24customerapp.Models.AddOnList;
import com.hitee.order24customerapp.Models.UserModel;
import com.hitee.order24customerapp.R;
import com.hitee.order24customerapp.ShoppingUtils.ShoppingCartActivity;
import com.hitee.order24customerapp.ShoppingUtils.ShoppingClickListener;
import com.hitee.order24customerapp.Utils.Constant;
import com.hitee.order24customerapp.Utils.Formmatting;
import com.hitee.order24customerapp.Utils.PrefUtils;
import com.hitee.order24customerapp.app.AppController;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Wahyu on 06/08/2015.
 */
public class CheckoutAddOnAdapter extends RecyclerView.Adapter<CheckoutAddOnAdapter.ItemViewHolder> {
    private static ArrayList<AddOnList> dataList;
    private LayoutInflater mInflater;
    private Context context;

    public CheckoutAddOnAdapter(Context ctx, ArrayList<AddOnList> data) {
        context = ctx;
        dataList = data;
        mInflater = LayoutInflater.from(context);
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgProduct;
        private CardView buttonMinus, buttonPlus;
        private TextView title, quantity, price, productiontime;

        public ItemViewHolder(View itemView) {
            super(itemView);

            title = (TextView) itemView.findViewById(R.id.title);
            price = (TextView) itemView.findViewById(R.id.price);
            quantity = (TextView) itemView.findViewById(R.id.quantity);

        }


    }



    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_addon_checkout, parent, false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        return itemViewHolder;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final ItemViewHolder holder, final int position) {
        boolean swipeEnable = true;
        final AddOnList album = dataList.get(position);
        //Toast.makeText(context, album.getId(), Toast.LENGTH_SHORT).show();

        holder.title.setText(dataList.get(position).getName());
        holder.price.setText("₦ " + Formmatting.priceToString(Double.parseDouble(dataList.get(position).getTotal())));
        holder.quantity.setText(dataList.get(position).getQuantity());
    }


    @Override
    public int getItemCount() {
        return dataList.size();
    }


}
