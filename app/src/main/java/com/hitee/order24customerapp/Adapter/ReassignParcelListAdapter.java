package com.hitee.order24customerapp.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hitee.order24customerapp.AnimationDir.MyAnimation;
import com.hitee.order24customerapp.Models.ParcelListModel;
import com.hitee.order24customerapp.R;

import java.util.ArrayList;

public class ReassignParcelListAdapter extends RecyclerView.Adapter<ReassignParcelListAdapter.ItemViewHolder> {
    private static ArrayList<ParcelListModel> dataList;
    private LayoutInflater mInflater;
    private Context context;

    public ReassignParcelListAdapter(Context ctx, ArrayList<ParcelListModel> data) {
        context = ctx;
        dataList = data;
        mInflater = LayoutInflater.from(context);
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private TextView name, ratingBar, nn, mm, review, status;

        public ItemViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.name);
            ratingBar = (TextView) itemView.findViewById(R.id.ratingBar);
            nn = (TextView) itemView.findViewById(R.id.nn);
            mm = (TextView) itemView.findViewById(R.id.mm);

        }

    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.reassign_parcel_list, parent, false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder holder, final int position) {

        holder.name.setText(dataList.get(position).getItemName());
        holder.ratingBar.setText(dataList.get(position).getContactNo());
        holder.nn.setText(dataList.get(position).getItemDesc());
        holder.mm.setText(dataList.get(position).getDeliveryAddress());
        MyAnimation.animateYearbookUser(holder);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

}
