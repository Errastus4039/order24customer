package com.hitee.order24customerapp.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.hitee.order24customerapp.Models.CartModel2;
import com.hitee.order24customerapp.Models.OrderHistoryModel;
import com.hitee.order24customerapp.R;
import com.hitee.order24customerapp.Utils.Constant;

import java.util.ArrayList;

/**
 * Created by Wahyu on 06/08/2015.
 */
public class OrderHistoryAdapter extends RecyclerView.Adapter<OrderHistoryAdapter.ItemViewHolder> {
    private static ArrayList<OrderHistoryModel> dataList;
    private LayoutInflater mInflater;
    private Context context;

    public OrderHistoryAdapter(Context ctx, ArrayList<OrderHistoryModel> data) {
        context = ctx;
        dataList = data;
        mInflater = LayoutInflater.from(context);
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgProduct;
        private TextView title, category, price,businessphone,bookingId;

        public ItemViewHolder(View itemView) {
            super(itemView);

            title = (TextView) itemView.findViewById(R.id.title);
            price = (TextView) itemView.findViewById(R.id.price);
            category = (TextView) itemView.findViewById(R.id.category);
            businessphone = (TextView) itemView.findViewById(R.id.oldPrice);
            bookingId = (TextView) itemView.findViewById(R.id.bookingId);
            imgProduct = (ImageView) itemView.findViewById(R.id.imgProduct);
        }

    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_list, parent, false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder holder, final int position) {

        holder.title.setText(dataList.get(position).getStoreName());
        holder.price.setText("₦ " + dataList.get(position).getOrderValue());
        holder.category.setText(dataList.get(position).getDropAddress());
        holder.businessphone.setText(dataList.get(position).getBusinessPhone());
        holder.bookingId.setText(dataList.get(position).getBookingID());
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

}
