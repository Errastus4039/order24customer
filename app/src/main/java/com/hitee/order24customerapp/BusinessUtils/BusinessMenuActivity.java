package com.hitee.order24customerapp.BusinessUtils;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hitee.order24customerapp.Adapter.BusinessAdapter;
import com.hitee.order24customerapp.Adapter.MenuAdapter;
import com.hitee.order24customerapp.Bottom.CustomerActivity;
import com.hitee.order24customerapp.ChatUtils.NewMessageActivity;
import com.hitee.order24customerapp.Models.BusinessModel;
import com.hitee.order24customerapp.Models.MenuModel;
import com.hitee.order24customerapp.OrderUtils.OrderConfirmationActivity;
import com.hitee.order24customerapp.ProductsUtils.ProductListActivity;
import com.hitee.order24customerapp.R;
import com.hitee.order24customerapp.Utils.Constant;
import com.hitee.order24customerapp.Utils.RecyclerTouchListener;
import com.hitee.order24customerapp.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BusinessMenuActivity extends AppCompatActivity {

    private static final String TAG = CustomerActivity.class.getSimpleName();
    private RecyclerView recyclerView;
    private ArrayList<MenuModel> cartList;
    private MenuAdapter mAdapter;
    private ShimmerFrameLayout mShimmerViewContainer;
    String CategoryId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_menu);


        final ActionBar abar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.custom_actionbar, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        TextView txttitle = (TextView) viewActionBar.findViewById(R.id.txtTitle);
        txttitle.setText("M E N U  L I S T");
        txttitle.setTextSize(14);
        if (abar != null) {
            abar.setCustomView(viewActionBar, params);

            abar.setDisplayShowCustomEnabled(true);
            abar.setDisplayShowTitleEnabled(false);
            abar.setHomeButtonEnabled(false);
            abar.setDisplayHomeAsUpEnabled(true);
        }

        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.rell);
        relativeLayout.setBackgroundColor(getColorWithAlpha(Color.WHITE, 0.9f));

        recyclerView = findViewById(R.id.recycle_view);
        cartList = new ArrayList<>();
        mAdapter = new MenuAdapter(BusinessMenuActivity.this, cartList);

        Intent intent = getIntent();
        final String BusinessId = intent.getStringExtra("BusinessId");
        String getAddress = intent.getStringExtra("getAddress");
        String getBusinessName = intent.getStringExtra("getBusinessName");
        String getDescription = intent.getStringExtra("getDescription");
        String getDistance = intent.getStringExtra("getDistance");
        String getDuration = intent.getStringExtra("getDuration");
        String getEmail = intent.getStringExtra("getEmail");
        String getPhoneNumber = intent.getStringExtra("getPhoneNumber");
        String getLogo = intent.getStringExtra("getLogo");


        TextView name = (TextView) findViewById(R.id.name);
        de.hdodenhof.circleimageview.CircleImageView logo = (de.hdodenhof.circleimageview.CircleImageView) findViewById(R.id.thumbnail);
        TextView address = (TextView) findViewById(R.id.address);
        TextView timestamp = (TextView) findViewById(R.id.timestamp);
        TextView kilometer = (TextView) findViewById(R.id.kilometer);
        TextView mins = (TextView) findViewById(R.id.mins);
        TextView star = (TextView) findViewById(R.id.star);

        name.setText(getBusinessName);
        address.setText(getAddress);
        mins.setText(getDuration);
        star.setText(getPhoneNumber);
        timestamp.setText(getDescription);
        kilometer.setText(getDistance);



        Glide.with(this)
                .load(Constant.STORELOGO + getLogo)
                .apply(new RequestOptions()
                        .placeholder(R.mipmap.fff)
                        .fitCenter())
                .into(logo);


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        fetchMenu(Constant.GET_MENU + "/" + BusinessId);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                final MenuModel doc = cartList.get(position);
                Intent i = new Intent(BusinessMenuActivity.this, ProductListActivity.class);
                i.putExtra("MenuId", doc.getId());
                startActivity(i);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Click action
                Intent intent = new Intent(BusinessMenuActivity.this, NewMessageActivity.class);
                startActivity(intent);
            }
        });


    }

    public static int getColorWithAlpha(int color, float ratio) {
        int newColor = 0;
        int alpha = Math.round(Color.alpha(color) * ratio);
        int r = Color.red(color);
        int g = Color.green(color);
        int b = Color.blue(color);
        newColor = Color.argb(alpha, r, g, b);
        return newColor;
    }

    private void fetchMenu(String URL) {

        Log.d("MENU", URL);
        final ProgressDialog progress = new ProgressDialog(BusinessMenuActivity.this);
        progress.setMessage("Loading...");
        progress.show();

        JsonArrayRequest request = new JsonArrayRequest(URL,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        progress.dismiss();
                        Log.d("LL", response.toString());
                        try {
                            if (response.toString().equals("[]")) {
                                Toast.makeText(BusinessMenuActivity.this, "There is no menu found.", Toast.LENGTH_LONG).show();
                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);
                                return;
                            }

                            for (int i = 0; i < response.length(); i++) {

                                try {
                                    final JSONObject obj = response.getJSONObject(i);

                                    MenuModel mydic = new MenuModel();
                                    mydic.setId(obj.getString("Id"));
                                    mydic.setMenu(obj.getString("Menu"));
                                    mydic.setDescription(obj.getString("Description"));
                                    mydic.setDate(obj.getString("date"));

                                    cartList.add(mydic);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            mAdapter.notifyDataSetChanged();
                            // stop animating Shimmer and hide the layout
                            mShimmerViewContainer.stopShimmerAnimation();
                            mShimmerViewContainer.setVisibility(View.GONE);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        mAdapter.notifyDataSetChanged();

                        // stop animating Shimmer and hide the layout
                        mShimmerViewContainer.stopShimmerAnimation();
                        mShimmerViewContainer.setVisibility(View.GONE);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error in getting json
                Log.e(TAG, "Error: " + error.getMessage());
                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
                progress.dismiss();
                Toast.makeText(getApplicationContext(), "Opps! Something went wrong, please try again later ", Toast.LENGTH_SHORT).show();
            }
        });

        AppController.getInstance().addToRequestQueue(request);


    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                Intent ii = new Intent(BusinessMenuActivity.this, CustomerActivity.class);
                startActivity(ii);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
