package com.hitee.order24customerapp.Utils;

/**
 * Created by Babatunde on 10/08/2017.
 */

public class Constant {

    public static String Base() {

        return "http://api.order24.ng/api/";
    }

    public static String FAILED = "";
    public static String SUCCESS = "";
    public static String DATA = "";
    public static String EMPTY = "EMPTY";
    public static String EmptyMessage = "Please all the empty(s) !!!";
    public static String LOGIN = Constant.Base() + "/Account/customer_login";
    public static String REGISTER = Constant.Base() + "Account/customer_registration";
    public static String GET_STOREADMIN_PROFILE = Constant.Base() + "account/PostStoreAdminProfile";
    public static String GET_MENU = Constant.Base() + "menu/allmenu";
    public static String GET_PRODUCTS = Constant.Base() + "product/getallproductbybusinessid";
    public static String GET_PRODUCTS_MENU = Constant.Base() + "product/getallproductsbycatId";
    public static String GET_PRODUCTS_ADDON = Constant.Base() + "product_add_on/get_products";
    public static String GET_LIST_PRODUCTS_ADDON = Constant.Base() + "product_add_on/get_productsbycustomerid";
    public static String ADD_ADD_ON = Constant.Base() + "product_add_on/post_add_on";
    public static String GET_DASHBOARD = Constant.Base() + "storeadmin/dashboard";
    public static String GET_PRODUCT_BY_CAT_ID = Constant.Base() + "product/getallproductsbycatId";
    public static String GET_CATEGORIES = Constant.Base() + "category/getallcategories";
    public static String POST_LOCATION = Constant.Base() + "location/allbusiness";
    public static String POST_CART = Constant.Base() + "shopping_cart/addcart";
    public static String GET_CARTS = Constant.Base() + "shopping_cart/getcarts";
    public static String DELETE_CARTS = Constant.Base() + "shopping_cart/Deletecart";
    public static String IMAGE_URL = "https://storeadmin.order24.ng/ProductImages/";
    public static String STORELOGO = "https://storeadmin.order24.ng/StoreProfileImagesLogo/";
    public static String GET_ADDRESS = Constant.Base() + "customer_address/getaddresses";
    public static String POST_ADDRESS = Constant.Base() + "customer_address/add_address";
    public static String POST_PREVIEW_CHECKOUT = Constant.Base() + "shopping_cart/preview_checkout";
    public static String POST_CHECKOUT = Constant.Base() + "shopping_cart/checkout";
    public static String PAYWITHWALLET = Constant.Base() + "shopping_cart/paywithwallet";
    public static String GET_ORDER_HISTORY = Constant.Base() + "order/getordersbyuserId";
    public static String GET_ORDER_LIST = Constant.Base() + "order/viewordersdetails";
    public static String GET_ORDER_LIST_ADD_ONS = Constant.Base() + "order/vieworders_add_ondetails";
    public static String SEND_PARCEL = Constant.Base() + "sendparcel/post";
    public static String SEND_PAYMENT = Constant.Base() + "sendparcel/pay";
    public static String PAY_WITH_CARD = Constant.Base() + "sendparcel/pay_with_card";
    public static String SEND_WALLET_CODE = Constant.Base() + "customer_wallet/create_code";
    public static String POST_WALLET = Constant.Base() + "customer_wallet/wallet";
    public static String GET_WALLET_BALANCE = Constant.Base() + "customer_wallet/wallet_balance";
    public static String DELETE_ADDRESS = Constant.Base() + "customer_address/deleteaddress";
    public static String GET_PARCEL = Constant.Base() + "sendparcel/getall_awaiting_rider";
    public static String Accepted_PARCEL = Constant.Base() + "sendparcel/getall_rider_accepted";
    public static String Completed_PARCEL = Constant.Base() + "sendparcel/getall_rider_completed";
    public static String Reassign_PARCEL = Constant.Base() + "sendparcel/getall_rider_reassign";
    public static String PIN_WALLET = Constant.Base() + "customer_wallet/code";
    public static String SEARCH_BUSINEES = Constant.Base() + "location/search";
    public static String REMOVE_ADDON = Constant.Base() + "product_add_on/remove";
    public static String SEND_CHECKOUT_WALLET_PAYMENT = Constant.Base() + "sendparcel/pay";
    public static String UPDATE_ORDER_STATUS = Constant.Base() + "paymentprocessor/settle";
    public static String RESET_PASSWORD = Constant.Base() + "account/resetmypassword";
    public static String RAVE_PAYEMNT = Constant.Base() + "paymentprocessor/payforfood";
    public static String OTP_RAVE_PAYEMNT = Constant.Base() + "paymentprocessor/verification";
    public static String GETREFERRAL = Constant.Base() + "referral/get_referral";
    public static String PARCEL_RAVE_PAYEMNT = Constant.Base() + "sendparcel/payforparcel";
    public static String PARCEL_OTP_RAVE_PAYEMNT = Constant.Base() + "sendparcel/verification";
    public static String POST_VBSECURE_VERIFICATION = Constant.Base() + "sendparcel/vbsecureverification";
    public static String POST_REASSIGN = Constant.Base() + "sendparcel/reassign_parcel";
    public static String POST_BACK_REASSIGN = Constant.Base() + "sendparcel/reassign_back_parcel";

}
