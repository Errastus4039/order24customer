package com.hitee.order24customerapp.Utils;

import android.content.Context;

import com.hitee.order24customerapp.Models.AddressInfoModel;
import com.hitee.order24customerapp.Models.LocationInfoModel;
import com.hitee.order24customerapp.Models.LoginResponseModel;
import com.hitee.order24customerapp.Models.MovementModel;
import com.hitee.order24customerapp.Models.ReferralModel;
import com.hitee.order24customerapp.Models.UserModel;
import com.hitee.order24customerapp.Models.WalletCodeInfoModel;


/**
 * Created by BABATUNDE on 8/8/2016.
 */
public class PrefUtils {

    public static void setCurrentUser(LoginResponseModel currentDealers, Context ctx) {
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "user_prefs", 0);
        complexPreferences.putObject("current_user_value", currentDealers);
        complexPreferences.commit();
    }


    public static LoginResponseModel getCurrentUser(Context ctx) {
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "user_prefs", 0);
        LoginResponseModel currentUser = complexPreferences.getObject("current_user_value", LoginResponseModel.class);
        return currentUser;
    }

    public static void clearCurrentUser(Context ctx) {
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "user_prefs", 0);
        complexPreferences.clearObject();
        complexPreferences.commit();
    }


    public static void setCurrentUserModel(UserModel currentDealers, Context ctx) {
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "user_model_prefs", 0);
        complexPreferences.putObject("current_user_model_value", currentDealers);
        complexPreferences.commit();
    }


    public static UserModel getCurrentUserModel(Context ctx) {
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "user_model_prefs", 0);
        UserModel currentUser = complexPreferences.getObject("current_user_model_value", UserModel.class);
        return currentUser;
    }

    public static void clearCurrentUserModel(Context ctx) {
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "user_model_prefs", 0);
        complexPreferences.clearObject();
        complexPreferences.commit();
    }


    public static void setAddress(AddressInfoModel currentDealers, Context ctx) {
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "address_model_prefs", 0);
        complexPreferences.putObject("current_address_model_value", currentDealers);
        complexPreferences.commit();
    }


    public static AddressInfoModel getAddress(Context ctx) {
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "address_model_prefs", 0);
        AddressInfoModel currentUser = complexPreferences.getObject("current_address_model_value", AddressInfoModel.class);
        return currentUser;
    }

    public static void clearAddress(Context ctx) {
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "address_model_prefs", 0);
        complexPreferences.clearObject();
        complexPreferences.commit();
    }


    public static void setLocation(LocationInfoModel currentDealers, Context ctx) {
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "location_model_prefs", 0);
        complexPreferences.putObject("current_location_model_value", currentDealers);
        complexPreferences.commit();
    }


    public static LocationInfoModel getLocation(Context ctx) {
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "location_model_prefs", 0);
        LocationInfoModel currentUser = complexPreferences.getObject("current_location_model_value", LocationInfoModel.class);
        return currentUser;
    }

    public static void clearLocation(Context ctx) {
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "location_model_prefs", 0);
        complexPreferences.clearObject();
        complexPreferences.commit();
    }


    public static void setWalletCode(WalletCodeInfoModel currentDealers, Context ctx) {
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "wallet_model_prefs", 0);
        complexPreferences.putObject("wallet_location_model_value", currentDealers);
        complexPreferences.commit();
    }


    public static WalletCodeInfoModel getWalletCode(Context ctx) {
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "wallet_model_prefs", 0);
        WalletCodeInfoModel currentUser = complexPreferences.getObject("wallet_location_model_value", WalletCodeInfoModel.class);
        return currentUser;
    }

    public static void clearWalletCode(Context ctx) {
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "wallet_model_prefs", 0);
        complexPreferences.clearObject();
        complexPreferences.commit();
    }


    public static void setReferralCodeModel(ReferralModel currentDealers, Context ctx) {
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "referral_model_prefs", 0);
        complexPreferences.putObject("referral_location_model_value", currentDealers);
        complexPreferences.commit();
    }


    public static ReferralModel getReferralCode(Context ctx) {
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "referral_model_prefs", 0);
        ReferralModel currentUser = complexPreferences.getObject("referral_location_model_value", ReferralModel.class);
        return currentUser;
    }

    public static void clearReferralCodeModel(Context ctx) {
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "referral_model_prefs", 0);
        complexPreferences.clearObject();
        complexPreferences.commit();
    }


    public static void setMovementModel(MovementModel currentDealers, Context ctx) {
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "movement_model_prefs", 0);
        complexPreferences.putObject("movement_location_model_value", currentDealers);
        complexPreferences.commit();
    }


    public static MovementModel getMovementCode(Context ctx) {
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "movement_model_prefs", 0);
        MovementModel currentUser = complexPreferences.getObject("movement_location_model_value", MovementModel.class);
        return currentUser;
    }

    public static void clearMovementCode(Context ctx) {
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "movement_model_prefs", 0);
        complexPreferences.clearObject();
        complexPreferences.commit();
    }


}
