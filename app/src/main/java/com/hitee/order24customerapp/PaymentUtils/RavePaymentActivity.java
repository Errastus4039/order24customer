package com.hitee.order24customerapp.PaymentUtils;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.InputType;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.braintreepayments.cardform.view.CardForm;
import com.hitee.order24customerapp.Bottom.CustomerActivity;
import com.hitee.order24customerapp.LoginUtils.LoginActivity;
import com.hitee.order24customerapp.Models.UserModel;
import com.hitee.order24customerapp.OrderUtils.OrderConfirmationActivity;
import com.hitee.order24customerapp.ParcelUtils.DeliveryActivity;
import com.hitee.order24customerapp.ParcelUtils.PaymentActivity;
import com.hitee.order24customerapp.ParcelUtils.SendParcelActivity;
import com.hitee.order24customerapp.R;
import com.hitee.order24customerapp.Utils.Constant;
import com.hitee.order24customerapp.Utils.PrefUtils;
import com.hitee.order24customerapp.app.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RavePaymentActivity extends AppCompatActivity {
    AlertDialog.Builder alertBuilder;

    private AppCompatCheckBox checkbox;
    String AddressId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rave_payment);

        final ActionBar abar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.custom_actionbar, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        TextView txttitle = (TextView) viewActionBar.findViewById(R.id.txtTitle);
        txttitle.setText("P A Y M E N T");
        txttitle.setTextSize(14);
        if (abar != null) {
            abar.setCustomView(viewActionBar, params);

            abar.setDisplayShowCustomEnabled(true);
            abar.setDisplayShowTitleEnabled(false);
            abar.setHomeButtonEnabled(false);
            abar.setDisplayHomeAsUpEnabled(true);
        }

        Intent ii = getIntent();
        String amnt = ii.getStringExtra("Amount");
        AddressId = ii.getStringExtra("AddressId");
        final UserModel userModel = PrefUtils.getCurrentUserModel(RavePaymentActivity.this);
        if (userModel == null || userModel.equals("null")) {
            startActivity(new Intent(RavePaymentActivity.this, LoginActivity.class));
            finish();
        } else {
            CardForm cardForm = findViewById(R.id.card_form);
            EditText password = findViewById(R.id.pswd);

            cardForm.cardRequired(true)
                    .expirationRequired(true)
                    .cvvRequired(true)
                    .postalCodeRequired(true)
                    .setup(RavePaymentActivity.this);
            cardForm.getCvvEditText().setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);

            checkbox = (AppCompatCheckBox) findViewById(R.id.checkbox);


            checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean value) {
                    if (value) {
                        // Show Password
                        password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    } else {
                        // Hide Password
                        password.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    }
                }
            });
            final ProgressDialog waitingDialog = new ProgressDialog(this);
            waitingDialog.setTitle("Validating...");
            waitingDialog.setMessage("Please wait while we validate your card");


            Button btnPay = (Button) findViewById(R.id.btnPay);
            btnPay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (cardForm.isValid()) {

                        alertBuilder = new AlertDialog.Builder(RavePaymentActivity.this);
                        alertBuilder.setTitle("Confirm before purchase");
                        alertBuilder.setMessage("Card number: " + cardForm.getCardNumber() + "\n" +
                                "Card expiry date: " + cardForm.getExpirationDateEditText().getText().toString() + "\n" +
                                "Card CVV: " + cardForm.getCvv() + "\n" +
                                "Postal code: " + cardForm.getPostalCode());
                        alertBuilder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();

                                if (password.getText().length() == 0) {
                                    Toast.makeText(RavePaymentActivity.this, "Pin Code is empty", Toast.LENGTH_LONG).show();
                                    return;
                                }
                                waitingDialog.show();
                                StringRequest strReq = new StringRequest(Request.Method.POST, Constant.RAVE_PAYEMNT,
                                        new com.android.volley.Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String response) {
                                                Log.d("RAVE_RESPONSE", String.valueOf(response));
                                                waitingDialog.dismiss();
                                                try {

                                                    JSONObject obj = new JSONObject(response);
                                                    String authUrl = obj.getString("authUrl");
                                                    String authModelUsed = obj.getString("authModelUsed");
                                                    String txId = obj.getString("id");
                                                    String transactionRef = obj.getString("flwRef");
                                                    if (authModelUsed.equals("PIN")) {
                                                        Toast.makeText(RavePaymentActivity.this, "Card validate successful", Toast.LENGTH_LONG).show();
                                                        Intent ii = new Intent(RavePaymentActivity.this, PINVerificationActivity.class);
                                                        ii.putExtra("url", authUrl);
                                                        ii.putExtra("txId", txId);
                                                        ii.putExtra("authModelUsed", authModelUsed);
                                                        ii.putExtra("transaction_reference", transactionRef);
                                                        ii.putExtra("AddressId", AddressId);

                                                        startActivity(ii);
                                                    } else {

                                                        Intent ii = new Intent(RavePaymentActivity.this, OTPPageActivity.class);
                                                        ii.putExtra("url", authUrl);
                                                        ii.putExtra("txId", txId);
                                                        ii.putExtra("authModelUsed", authModelUsed);
                                                        ii.putExtra("AddressId", AddressId);
                                                        startActivity(ii);
                                                    }

                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }

                                            }
                                        }, new com.android.volley.Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {

                                        NetworkResponse response = error.networkResponse;
                                        if (error instanceof ServerError && response != null) {

                                        }
                                    }
                                }) {

                                    @Override
                                    protected Map<String, String> getParams() {

                                        Map<String, String> params = new HashMap<String, String>();
                                        params.put("cardno", cardForm.getCardNumber());
                                        params.put("expirymonth", cardForm.getExpirationMonth().toString());
                                        params.put("expiryyear", cardForm.getExpirationYear().toString());
                                        params.put("cvv", cardForm.getCvv());
                                       // params.put("amount", "5");
                                        params.put("amount", amnt);
                                        params.put("lastname", userModel.getLastName());
                                        params.put("firstname", userModel.getFirstName());
                                        params.put("phonenumber", userModel.getPhoneNumber());
                                        params.put("email", userModel.getEmail());
                                        params.put("userId", userModel.getId());
                                        params.put("pin", password.getText().toString().trim());
                                        Log.d("POST_TAG", params.toString());
                                        return params;
                                    }
                                };
                                DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                                strReq.setRetryPolicy(retryPolicy);
                                AppController.getInstance().addToRequestQueue(strReq);
                                // Toast.makeText(MainActivity.this, "Thank you for purchase", Toast.LENGTH_LONG).show();
                            }
                        });
                        alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                        AlertDialog alertDialog = alertBuilder.create();
                        alertDialog.show();

                    } else {
                        Toast.makeText(RavePaymentActivity.this, "Please complete the form", Toast.LENGTH_LONG).show();
                    }

                }

            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_send_parcel:
                Intent ii = new Intent(RavePaymentActivity.this, CustomerActivity.class);
                startActivity(ii);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
