package com.hitee.order24customerapp.PaymentUtils;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.hitee.order24customerapp.Bottom.CustomerActivity;
import com.hitee.order24customerapp.R;

public class FailedTransactionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_failed_transaction);

        Intent ii = getIntent();
        String resp = ii.getStringExtra("VBV");

        final ActionBar abar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.custom_actionbar, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        TextView txttitle = (TextView) viewActionBar.findViewById(R.id.txtTitle);
        txttitle.setText("F A I L E D  T R A N S A C T I O N");
        txttitle.setTextSize(12);
        if (abar != null) {
            abar.setCustomView(viewActionBar, params);

            abar.setDisplayShowCustomEnabled(true);
            abar.setDisplayShowTitleEnabled(false);
            abar.setHomeButtonEnabled(false);
            abar.setDisplayHomeAsUpEnabled(true);
        }


        TextView txtView = (TextView) findViewById(R.id.resp);
        txtView.setText(resp);

    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(FailedTransactionActivity.this, CustomerActivity.class));
        finish();
    }
}
