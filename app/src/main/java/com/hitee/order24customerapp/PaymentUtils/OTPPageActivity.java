package com.hitee.order24customerapp.PaymentUtils;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.hitee.order24customerapp.LoginUtils.LoginActivity;
import com.hitee.order24customerapp.Models.AddressInfoModel;
import com.hitee.order24customerapp.Models.MyJavaScriptInterface;
import com.hitee.order24customerapp.Models.UserModel;
import com.hitee.order24customerapp.OrderUtils.OrderConfirmationActivity;
import com.hitee.order24customerapp.OrderUtils.OrderSubmittedActivity;
import com.hitee.order24customerapp.R;
import com.hitee.order24customerapp.Utils.AlertDialogManager;
import com.hitee.order24customerapp.Utils.Constant;
import com.hitee.order24customerapp.Utils.PrefUtils;
import com.hitee.order24customerapp.app.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class OTPPageActivity extends AppCompatActivity {
    // private String TAG = BrowserActivity.class.getSimpleName();
    private String url;
    private WebView webView;
    private ProgressBar progressBar;
    private float m_downX;
    CoordinatorLayout coordinatorLayout;
    String addressId;
    AddressInfoModel addressInfoModel;
    AlertDialogManager alertDialogManager;
    UserModel userModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otppage);

        final ActionBar abar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.custom_actionbar, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        TextView txttitle = (TextView) viewActionBar.findViewById(R.id.txtTitle);
        txttitle.setText("B A N K  O T P");
        txttitle.setTextSize(12);
        if (abar != null) {
            abar.setCustomView(viewActionBar, params);

            abar.setDisplayShowCustomEnabled(true);
            abar.setDisplayShowTitleEnabled(false);
            abar.setHomeButtonEnabled(false);
            abar.setDisplayHomeAsUpEnabled(true);
        }
        Intent ii = getIntent();
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.main_content);

        addressId = ii.getStringExtra("AddressId");
        userModel = PrefUtils.getCurrentUserModel(OTPPageActivity.this);
        addressInfoModel = PrefUtils.getAddress(OTPPageActivity.this);
        if (userModel == null) {
            startActivity(new Intent(OTPPageActivity.this, LoginActivity.class));
            return;
        }
        alertDialogManager = new AlertDialogManager();

        url = getIntent().getStringExtra("url");

        // if no url is passed, close the activity
        if (TextUtils.isEmpty(url)) {
            finish();
        }

        webView = (WebView) findViewById(R.id.webView);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        initWebView();

        webView.loadUrl(url);
    }

    @SuppressLint({"SetJavaScriptEnabled", "JavascriptInterface"})
    private void initWebView() {
        webView.setWebChromeClient(new MyWebChromeClient(this));
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                progressBar.setVisibility(View.VISIBLE);
                invalidateOptionsMenu();
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                webView.loadUrl(url);
                return true;
            }

            int i = 0;

            @Override
            public void onPageFinished(WebView view, String url) {
                view.loadUrl("javascript:console.log('MAGIC'+document.getElementsByTagName('body')[0].innerHTML);");
                //view.loadUrl("javascript:(function() { " + "document.getElementsByTagName('body')[0].style.display = 'none'; " + "})()");
                progressBar.setVisibility(View.GONE);
                i = i + 1;
                if (i > 3){
                    Log.d("IIII", String.valueOf(i));
                    coordinatorLayout.setBackgroundColor(Color.WHITE);
                    webView.setVisibility(View.INVISIBLE);
                }
                invalidateOptionsMenu();
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                progressBar.setVisibility(View.GONE);
                invalidateOptionsMenu();
            }


        });
        //webView.loadUrl("http://www.google.com");
        webView.clearCache(true);
        webView.clearHistory();
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setHorizontalScrollBarEnabled(false);
        webView.addJavascriptInterface(new MyJavaScriptInterface(OTPPageActivity.this), "HtmlViewer");

    }

    private class MyWebChromeClient extends WebChromeClient {
        Context context;

        public MyWebChromeClient(Context context) {
            super();
            this.context = context;
        }

        @Override
        public boolean onConsoleMessage(ConsoleMessage consoleMessage) {


            // check secret prefix
            if (consoleMessage.message().startsWith("MAGIC")) {

                String msg = consoleMessage.message().substring(5); // strip off prefix
                // String all = consoleMessage.message().substring(0, msg.length());
                Log.d("MSG", msg);

                try {

                    JSONObject arr = new JSONObject(msg);
                    final String status = arr.getString("status");

                    if (status.equals("successful")) {

                        final String amount = arr.getString("amount");
                        final String IP = arr.getString("IP");
                        final String appfee = arr.getString("appfee");
                        final String charged_amount = arr.getString("charged_amount");
                        final String currency = arr.getString("currency");
                        final String flwRef = arr.getString("flwRef");
                        final String narration = arr.getString("narration");
                        final String orderRef = arr.getString("orderRef");
                        final String raveRef = arr.getString("raveRef");
                        final String txRef = arr.getString("txRef");
                        final String id = arr.getString("id");
                        final String vbRespcode = arr.getString("vbvrespcode");
                        final String vbrespmessage = arr.getString("vbvrespmessage");

                        final ProgressDialog progress = new ProgressDialog(OTPPageActivity.this);
                        progress.setTitle("O R D E R  C O N F I R M A T I O N");
                        progress.setMessage("Submitting Order to restaurant...");
                        progress.show();


                        StringRequest strReq = new StringRequest(Request.Method.POST, Constant.POST_CHECKOUT,
                                new com.android.volley.Response.Listener<String>() {

                                    @Override
                                    public void onResponse(String response) {
                                        //    Log.d("TAGGGGGGGG", String.valueOf(response));
                                        progress.dismiss();
                                        try {
                                            if (response.equals("200")) {
                                                startActivity(new Intent(OTPPageActivity.this, OrderSubmittedActivity.class));
                                            } else {
                                                alertDialogManager.showAlertDialog(OTPPageActivity.this, "Submission failed", "Your order is not submitted", false);
                                            }

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    }
                                }, new com.android.volley.Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                                NetworkResponse response = error.networkResponse;
                                if (error instanceof ServerError && response != null) {
                                }
                            }
                        }) {

                            @Override
                            protected Map<String, String> getParams() {

                                Map<String, String> params = new HashMap<String, String>();
                                params.put("CustomerId", userModel.getId());
                                params.put("amount", amount);
                                params.put("IP", IP);
                                params.put("appfee", appfee);
                                params.put("charged_amount", charged_amount);
                                params.put("currency", currency);
                                params.put("flwRef", flwRef);
                                params.put("narration", narration);
                                params.put("orderRef", orderRef);
                                params.put("raveRef", raveRef);
                                params.put("txRef", txRef);
                                params.put("vbRespcode", vbRespcode);
                                params.put("vbrespmessage", vbrespmessage);
                                params.put("AddressId", addressId);
                                params.put("FirstName", userModel.getFirstName());
                                params.put("LastName", userModel.getLastName());
                                params.put("PhoneNumber", userModel.getPhoneNumber());
                                params.put("Email", userModel.getEmail());
                                params.put("id", id);

                                return params;
                            }
                        };
                        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                        strReq.setRetryPolicy(retryPolicy);
                        AppController.getInstance().addToRequestQueue(strReq);
                    } else if (status.equals("failed")) {
                        final String vbrespmessage = arr.getString("vbvrespmessage");
                        alertDialogManager.showAlertDialog(OTPPageActivity.this, "Failed", "Your payment was not successful", false);
                        Intent i = new Intent(OTPPageActivity.this, FailedTransactionActivity.class);
                        i.putExtra("VBV", vbrespmessage);
                        startActivity(i);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return true;
            }

            return false;
        }


    }

}
