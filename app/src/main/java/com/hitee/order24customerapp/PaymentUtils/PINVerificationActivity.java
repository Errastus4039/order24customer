package com.hitee.order24customerapp.PaymentUtils;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.hitee.order24customerapp.LoginUtils.LoginActivity;
import com.hitee.order24customerapp.Models.UserModel;
import com.hitee.order24customerapp.OrderUtils.OrderSubmittedActivity;
import com.hitee.order24customerapp.R;
import com.hitee.order24customerapp.Utils.AlertDialogManager;
import com.hitee.order24customerapp.Utils.Constant;
import com.hitee.order24customerapp.Utils.PrefUtils;
import com.hitee.order24customerapp.app.AppController;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class PINVerificationActivity extends AppCompatActivity {

    String otpValue;
    AlertDialogManager alertDialogManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pinverification);

        final ActionBar abar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.custom_actionbar, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        TextView txttitle = (TextView) viewActionBar.findViewById(R.id.txtTitle);
        txttitle.setText("P I N  C O D E");
        txttitle.setTextSize(14);
        if (abar != null) {
            abar.setCustomView(viewActionBar, params);

            abar.setDisplayShowCustomEnabled(true);
            abar.setDisplayShowTitleEnabled(false);
            abar.setHomeButtonEnabled(false);
            abar.setDisplayHomeAsUpEnabled(true);
        }


        EditText txtCode = (EditText) findViewById(R.id.input_otp);

        alertDialogManager = new AlertDialogManager();

        Intent ii = getIntent();
        String transactionReference = ii.getStringExtra("transaction_reference");
        String AddressId = ii.getStringExtra("AddressId");
        final UserModel userModel = PrefUtils.getCurrentUserModel(PINVerificationActivity.this);
        if (userModel == null || userModel.equals("null")) {
            startActivity(new Intent(PINVerificationActivity.this, LoginActivity.class));
            finish();
        } else {


            final ProgressDialog waitingDialog = new ProgressDialog(this);
            waitingDialog.setTitle("Validating OTP...");
            waitingDialog.setMessage("Please wait while we validate your OTP");
            Button btnValidate = (Button) findViewById(R.id.validate_button);
            btnValidate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    waitingDialog.show();
                    // String ROM = ;
                    // String tra = transactionReference.substring(16);
                    StringRequest strReq = new StringRequest(Request.Method.POST, Constant.OTP_RAVE_PAYEMNT,
                            new com.android.volley.Response.Listener<String>() {

                                @Override
                                public void onResponse(String response) {
                                    Log.d("RAVE_OTP_RESPONSE", String.valueOf(response));
                                    waitingDialog.dismiss();
                                    try {

                                        JSONObject obj = new JSONObject(response);
                                        String status = obj.getString("HttpStatusCode");
                                        if (status.equals("200")) {
                                            alertDialogManager.showAlertDialog(PINVerificationActivity.this, "Payment Success", "Payment successfully sent", true);
                                            Intent ii = new Intent(PINVerificationActivity.this, OrderSubmittedActivity.class);
                                            startActivity(ii);
                                        } else {
                                            alertDialogManager.showAlertDialog(PINVerificationActivity.this, "Payment Error", "Your payment was not successfully", false);
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                }
                            }, new com.android.volley.Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            NetworkResponse response = error.networkResponse;
                            if (error instanceof ServerError && response != null) {

                            }
                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("txRef", transactionReference);
                            params.put("userId", userModel.getId());
                            params.put("pin", txtCode.getText().toString());
                            params.put("redirect_url", AddressId);
                            Log.d("OTP_VERIFY", params.toString());
                            return params;
                        }
                    };
                    DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                    strReq.setRetryPolicy(retryPolicy);
                    AppController.getInstance().addToRequestQueue(strReq);
                }
            });

        }
    }
}
