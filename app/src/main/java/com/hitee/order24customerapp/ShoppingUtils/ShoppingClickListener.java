package com.hitee.order24customerapp.ShoppingUtils;

import android.view.View;

public interface ShoppingClickListener {
    void itemClicked(View view, int position);
}