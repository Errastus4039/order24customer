package com.hitee.order24customerapp.ShoppingUtils;

import android.view.View;

public interface ShoppingInfomationClickListener {
    void itemClicked(View view, int position);
}