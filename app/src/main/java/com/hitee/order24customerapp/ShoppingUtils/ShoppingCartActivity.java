package com.hitee.order24customerapp.ShoppingUtils;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.hitee.order24customerapp.Adapter.AddOnAdapter;
import com.hitee.order24customerapp.Adapter.CartViewAddOnAdapter;
import com.hitee.order24customerapp.Adapter.ShoppingAdapter;
import com.hitee.order24customerapp.Adapter.ViewAddOnAdapter;
import com.hitee.order24customerapp.Bottom.CustomerActivity;
import com.hitee.order24customerapp.LoginUtils.LoginActivity;
import com.hitee.order24customerapp.Models.AddOnList;
import com.hitee.order24customerapp.Models.AddOnModel;
import com.hitee.order24customerapp.Models.CartModel;
import com.hitee.order24customerapp.Models.FullPriceModel;
import com.hitee.order24customerapp.Models.ProductModel;
import com.hitee.order24customerapp.Models.UserModel;
import com.hitee.order24customerapp.ParcelUtils.ParcelListActivity;
import com.hitee.order24customerapp.ParcelUtils.SendParcelActivity;
import com.hitee.order24customerapp.ProductsUtils.ProductListActivity;
import com.hitee.order24customerapp.R;
import com.hitee.order24customerapp.Utils.Constant;
import com.hitee.order24customerapp.Utils.PrefUtils;
import com.hitee.order24customerapp.Utils.RecyclerTouchListener;
import com.hitee.order24customerapp.app.AppController;
import com.tubb.smrv.SwipeMenuRecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ShoppingCartActivity extends AppCompatActivity implements ShoppingClickListener {
    private ArrayList<CartModel> rowListItem;
    private ArrayList<AddOnList> addOnLists;
    private SwipeMenuRecyclerView rView;

    private SwipeMenuRecyclerView AddOnrView;
    ShoppingAdapter rcAdapter;
    CartViewAddOnAdapter AddOnAdapter;
    TextView txtPrice, subtotal, txtCounter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_cart);

        final ActionBar abar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.custom_actionbar, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        TextView txttitle = (TextView) viewActionBar.findViewById(R.id.txtTitle);
        txttitle.setText("S H O P P I N G  C A R T");
        txttitle.setTextSize(14);
        if (abar != null) {
            abar.setCustomView(viewActionBar, params);

            abar.setDisplayShowCustomEnabled(true);
            abar.setDisplayShowTitleEnabled(false);
            abar.setHomeButtonEnabled(false);
            abar.setDisplayHomeAsUpEnabled(true);
        }



        UserModel userModel = PrefUtils.getCurrentUserModel(ShoppingCartActivity.this);
        if (userModel == null) {
            startActivity(new Intent(ShoppingCartActivity.this, LoginActivity.class));
            finish();
        } else {
            String URL = Constant.GET_CARTS + "/" + userModel.getId();
            String URL_ADDON = Constant.GET_LIST_PRODUCTS_ADDON + "/" + userModel.getId();
            rowListItem = new ArrayList<>();
            addOnLists = new ArrayList<>();
            getAllItemList(URL);
            getAllAddOnItemList(URL_ADDON);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            LinearLayoutManager layoutManager1 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

            txtPrice = (TextView) findViewById(R.id.price);
          //  subtotal = (TextView) findViewById(R.id.subtotal);
            rView = (SwipeMenuRecyclerView) findViewById(R.id.recyclerView);
            AddOnrView = (SwipeMenuRecyclerView) findViewById(R.id.recyclerView2);
            rView.setHasFixedSize(false);
            AddOnrView.setHasFixedSize(false);
            rView.setLayoutManager(layoutManager);
            rView.setNestedScrollingEnabled(false);
            rcAdapter = new ShoppingAdapter(this, rowListItem);
            rView.setAdapter(rcAdapter);


            AddOnrView.setLayoutManager(layoutManager1);
            AddOnrView.setNestedScrollingEnabled(false);
            AddOnAdapter = new CartViewAddOnAdapter(this, addOnLists);
            AddOnrView.setAdapter(AddOnAdapter);


            rcAdapter.setClickListener(this);

            ProgressDialog progressDialog  = new ProgressDialog(ShoppingCartActivity.this);
            progressDialog.setMessage("Please wait...");


            AddOnrView.addOnItemTouchListener(new RecyclerTouchListener(this, AddOnrView, new RecyclerTouchListener.ClickListener() {
                @Override
                public void onClick(View view, int position) {
                    final AddOnList doc = addOnLists.get(position);
                    progressDialog.show();
                    //   String url = Constant.REMOVE_ADDON;
                    StringRequest strReq = new StringRequest(Request.Method.POST, Constant.REMOVE_ADDON,
                            new com.android.volley.Response.Listener<String>() {

                                @Override
                                public void onResponse(String response) {
                                    Log.d("TAGGG", String.valueOf(response));
                                    progressDialog.dismiss();
                                    if (response.equals("200")) {
                                        Toast.makeText(ShoppingCartActivity.this, "The addon have been removed", Toast.LENGTH_LONG).show();
                                        Intent ii = new Intent(ShoppingCartActivity.this, ShoppingCartActivity.class);
                                        startActivity(ii);
                                        finish();
                                    }

                                }
                            }, new com.android.volley.Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            NetworkResponse response = error.networkResponse;
                            if (error instanceof ServerError && response != null) {

                            }
                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("customer_id", userModel.getId());
                            params.put("Id", doc.getId());

                            Log.d("POST_TAG", params.toString());
                            return params;
                        }
                    };
                    DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                    strReq.setRetryPolicy(retryPolicy);
                    AppController.getInstance().addToRequestQueue(strReq);

                }

                @Override
                public void onLongClick(View view, int position) {

                }


            }));



            Button btnCheckout = (Button) findViewById(R.id.btnCheckout);

            Button btnAdons = (Button) findViewById(R.id.btnaddon);
            btnCheckout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (txtPrice.getText().toString().equals("") || txtPrice.getText().toString().equals("null") || txtPrice.getText().toString().equals(null)) {

                        Toast.makeText(ShoppingCartActivity.this, "You need to cart some items", Toast.LENGTH_SHORT).show();
                    } else {
                        Intent ii = new Intent(ShoppingCartActivity.this, ShoppingInformationActivity.class);
                        ii.putExtra("TotalPrice", txtPrice.getText().toString());
                        startActivity(ii);
                    }
                }
            });

            btnAdons.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent ii = new Intent(ShoppingCartActivity.this, ViewAddonActivity.class);
                    startActivity(ii);
                }
            });
        }
    }


    private void getAllItemList(String url) {
        final ProgressDialog progress = new ProgressDialog(ShoppingCartActivity.this);
        progress.setTitle("S H O P P I N G  C A R T S");
        progress.setMessage("Loading...");
        progress.show();


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null,
                new Response.Listener<JSONObject>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(JSONObject response) {
                        progress.dismiss();
                        Log.d("LL", response.toString());
                        try {
                            JSONArray genreArry = response.getJSONArray("Carts");

                            // FullPriceModel full = new FullPriceModel();
                            txtPrice.setText("₦ " + response.getString("ProductPrice"));
                            //subtotal.setText("₦ " + response.getString("ProductPrice"));
                            for (int i = 0; i < genreArry.length(); i++) {

                                try {
                                    final JSONObject obj = genreArry.getJSONObject(i);


                                    CartModel mydic = new CartModel();
                                    mydic.setId(obj.getString("Id"));
                                    mydic.setProductId(obj.getString("product_id"));
                                    mydic.setImageProductUrl(obj.getString("ProductImage"));
                                    mydic.setPrice(obj.getString("total_price"));
                                    mydic.setQuantity(obj.getString("quantity"));
                                    mydic.setName(obj.getString("Name"));
                                    mydic.setProductionTime(obj.getString("ProductionTime"));
                                    rowListItem.add(mydic);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            rcAdapter.notifyDataSetChanged();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error in getting json
                Log.e("TAG", "Error: " + error.getMessage());
//                mShimmerViewContainer.stopShimmerAnimation();
//                mShimmerViewContainer.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), "Opps! Something went wrong, please try again later ", Toast.LENGTH_SHORT).show();
            }
        });


        AppController.getInstance().addToRequestQueue(jsonObjReq);

    }


    private void getAllAddOnItemList(String url) {
//        final ProgressDialog progress = new ProgressDialog(ShoppingCartActivity.this);
//        progress.setTitle("A D D O N");
//        progress.setMessage("Loading...");
//        progress.show();


        JsonArrayRequest request = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        //progress.dismiss();
                        Log.d("LL", response.toString());
                        try {
                            if (response.toString().equals("[]")) {
                               // Toast.makeText(ShoppingCartActivity.this, "There is no addon found.", Toast.LENGTH_LONG).show();

                                return;
                            }

                            for (int i = 0; i < response.length(); i++) {

                                try {
                                    final JSONObject obj = response.getJSONObject(i);

                                    AddOnList mydic = new AddOnList();
                                    mydic.setId(obj.getString("Id"));
                                    mydic.setName(obj.getString("Name"));
                                    mydic.setQuantity(obj.getString("Quantity"));
                                    mydic.setTotal(obj.getString("Total"));

                                    addOnLists.add(mydic);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            AddOnAdapter.notifyDataSetChanged();
                            // stop animating Shimmer and hide the layout

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        AddOnAdapter.notifyDataSetChanged();

                        // stop animating Shimmer and hide the layout

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error in getting json

              //  progress.dismiss();
                Toast.makeText(getApplicationContext(), "Opps! Something went wrong, please try again later ", Toast.LENGTH_SHORT).show();
            }
        });

        AppController.getInstance().addToRequestQueue(request);

    }




    @Override
    public void itemClicked(View view, int position) {
        int pos = position + 1;
        Toast.makeText(this, "Position " + pos + " clicked!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                Intent ii = new Intent(ShoppingCartActivity.this, CustomerActivity.class);
                startActivity(ii);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
