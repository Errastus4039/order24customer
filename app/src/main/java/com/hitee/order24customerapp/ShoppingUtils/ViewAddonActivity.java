package com.hitee.order24customerapp.ShoppingUtils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.hitee.order24customerapp.Adapter.ShoppingAdapter;
import com.hitee.order24customerapp.Adapter.ViewAddOnAdapter;
import com.hitee.order24customerapp.Bottom.CustomerActivity;
import com.hitee.order24customerapp.LoginUtils.LoginActivity;
import com.hitee.order24customerapp.Models.AddOnList;
import com.hitee.order24customerapp.Models.CartModel;
import com.hitee.order24customerapp.Models.ParcelListModel;
import com.hitee.order24customerapp.Models.ProductModel;
import com.hitee.order24customerapp.Models.UserModel;
import com.hitee.order24customerapp.Models.WalletModel;
import com.hitee.order24customerapp.ParcelUtils.ParcelListActivity;
import com.hitee.order24customerapp.ProductsUtils.ProductDetailsActivity;
import com.hitee.order24customerapp.ProductsUtils.ProductListActivity;
import com.hitee.order24customerapp.R;
import com.hitee.order24customerapp.Utils.Constant;
import com.hitee.order24customerapp.Utils.Formmatting;
import com.hitee.order24customerapp.Utils.PrefUtils;
import com.hitee.order24customerapp.Utils.RecyclerTouchListener;
import com.hitee.order24customerapp.app.AppController;
import com.tubb.smrv.SwipeMenuRecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ViewAddonActivity extends AppCompatActivity {
    private ArrayList<AddOnList> rowListItem;
    private SwipeMenuRecyclerView rView;

    ViewAddOnAdapter rcAdapter;
    TextView txtPrice, subtotal, txtCounter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_addon);

        final ActionBar abar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.custom_actionbar, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        TextView txttitle = (TextView) viewActionBar.findViewById(R.id.txtTitle);
        txttitle.setText("V I E W  A D D E D  A D D O N");
        txttitle.setTextSize(14);
        if (abar != null) {
            abar.setCustomView(viewActionBar, params);

            abar.setDisplayShowCustomEnabled(true);
            abar.setDisplayShowTitleEnabled(false);
            abar.setHomeButtonEnabled(false);
            abar.setDisplayHomeAsUpEnabled(true);
        }


        Button btnbackcheckout = (Button) findViewById(R.id.btnbackcheckout);
        btnbackcheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ii = new Intent(ViewAddonActivity.this, ShoppingCartActivity.class);
                startActivity(ii);
            }
        });
        UserModel userModel = PrefUtils.getCurrentUserModel(ViewAddonActivity.this);
        if (userModel == null) {
            startActivity(new Intent(ViewAddonActivity.this, LoginActivity.class));
            finish();
        } else {
            String URL = Constant.GET_LIST_PRODUCTS_ADDON + "/" + userModel.getId();
            rowListItem = new ArrayList<>();
            getAllItemList(URL);

            LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

            rView = (SwipeMenuRecyclerView) findViewById(R.id.recyclerView);
            rView.setHasFixedSize(false);
            rView.setLayoutManager(layoutManager);
            rView.setNestedScrollingEnabled(false);
            rcAdapter = new ViewAddOnAdapter(this, rowListItem);
            rView.setAdapter(rcAdapter);
            ProgressDialog progressDialog = new ProgressDialog(ViewAddonActivity.this);
            progressDialog.setTitle("Add On");
            progressDialog.setMessage("Please wait ...");

            rView.addOnItemTouchListener(new RecyclerTouchListener(this, rView, new RecyclerTouchListener.ClickListener() {
                @Override
                public void onClick(View view, int position) {
                    final AddOnList doc = rowListItem.get(position);
                    progressDialog.show();
                    //   String url = Constant.REMOVE_ADDON;
                    StringRequest strReq = new StringRequest(Request.Method.POST, Constant.REMOVE_ADDON,
                            new com.android.volley.Response.Listener<String>() {

                                @Override
                                public void onResponse(String response) {
                                    Log.d("TAGGG", String.valueOf(response));
                                    progressDialog.dismiss();
                                    if (response.equals("200")) {
                                        Toast.makeText(ViewAddonActivity.this, "The addon has been remove", Toast.LENGTH_LONG).show();
                                        Intent ii = new Intent(ViewAddonActivity.this, ViewAddonActivity.class);
                                        startActivity(ii);
                                        finish();
                                    }

                                }
                            }, new com.android.volley.Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            NetworkResponse response = error.networkResponse;
                            if (error instanceof ServerError && response != null) {

                            }
                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("customer_id", userModel.getId());
                            params.put("Id", doc.getId());

                            Log.d("POST_TAG", params.toString());
                            return params;
                        }
                    };
                    DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                    strReq.setRetryPolicy(retryPolicy);
                    AppController.getInstance().addToRequestQueue(strReq);

                }

                @Override
                public void onLongClick(View view, int position) {

                }


            }));

        }

    }


    private void getAllItemList(String url) {
        final ProgressDialog progress = new ProgressDialog(ViewAddonActivity.this);
        progress.setTitle("A D D O N");
        progress.setMessage("Loading...");
        progress.show();


        JsonArrayRequest request = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        progress.dismiss();
                        Log.d("LL", response.toString());
                        try {
                            if (response.toString().equals("[]")) {
                                Toast.makeText(ViewAddonActivity.this, "There is no addon found.", Toast.LENGTH_LONG).show();

                                return;
                            }

                            for (int i = 0; i < response.length(); i++) {

                                try {
                                    final JSONObject obj = response.getJSONObject(i);

                                    AddOnList mydic = new AddOnList();
                                    mydic.setId(obj.getString("Id"));
                                    mydic.setName(obj.getString("Name"));
                                    mydic.setQuantity(obj.getString("Quantity"));
                                    mydic.setTotal(obj.getString("Total"));

                                    rowListItem.add(mydic);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            rcAdapter.notifyDataSetChanged();
                            // stop animating Shimmer and hide the layout

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        rcAdapter.notifyDataSetChanged();

                        // stop animating Shimmer and hide the layout

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error in getting json

                progress.dismiss();
                Toast.makeText(getApplicationContext(), "Opps! Something went wrong, please try again later ", Toast.LENGTH_SHORT).show();
            }
        });

        AppController.getInstance().addToRequestQueue(request);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                Intent ii = new Intent(ViewAddonActivity.this, CustomerActivity.class);
                startActivity(ii);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}