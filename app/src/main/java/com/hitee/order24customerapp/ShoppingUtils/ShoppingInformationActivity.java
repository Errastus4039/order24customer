package com.hitee.order24customerapp.ShoppingUtils;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.hitee.order24customerapp.Adapter.DeliveryAddressAdapter;
import com.hitee.order24customerapp.Adapter.ShoppingAdapter;
import com.hitee.order24customerapp.AddressUtils.AddNewAddressActivity;
import com.hitee.order24customerapp.AddressUtils.AddressActivity;
import com.hitee.order24customerapp.Bottom.CustomerActivity;
import com.hitee.order24customerapp.BusinessUtils.BusinessMenuActivity;
import com.hitee.order24customerapp.LoginUtils.LoginActivity;
import com.hitee.order24customerapp.Models.AddressInfoModel;
import com.hitee.order24customerapp.Models.AddressModel;
import com.hitee.order24customerapp.Models.CartModel;
import com.hitee.order24customerapp.Models.CartModel2;
import com.hitee.order24customerapp.Models.MenuModel;
import com.hitee.order24customerapp.Models.OthersModel;
import com.hitee.order24customerapp.Models.UserModel;
import com.hitee.order24customerapp.OrderUtils.OrderConfirmationActivity;
import com.hitee.order24customerapp.ProductsUtils.ProductListActivity;
import com.hitee.order24customerapp.R;
import com.hitee.order24customerapp.Utils.AlertDialogManager;
import com.hitee.order24customerapp.Utils.Constant;
import com.hitee.order24customerapp.Utils.PrefUtils;
import com.hitee.order24customerapp.app.AppController;
import com.tubb.smrv.SwipeMenuRecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ShoppingInformationActivity extends AppCompatActivity {
    private ArrayList<AddressModel> rowListItem;
    private ArrayList<CartModel2> cartModels;

    private RecyclerView rView;

    DeliveryAddressAdapter rcAdapter;
    TextView txtPrice, subtotal, txtCounter;
    //  addressInfoModel;
    AlertDialogManager alertDialogManager;
    AddressInfoModel addressInfoModel;
    String TotalPrice;
    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_information);

        final ActionBar abar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.custom_actionbar, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        TextView txttitle = (TextView) viewActionBar.findViewById(R.id.txtTitle);
        txttitle.setText("D E L I V E R Y  I N F O R M A T I O N");
        txttitle.setTextSize(12);
        if (abar != null) {
            abar.setCustomView(viewActionBar, params);
            abar.setDisplayShowCustomEnabled(true);
            abar.setDisplayShowTitleEnabled(false);
            abar.setHomeButtonEnabled(false);
            abar.setDisplayHomeAsUpEnabled(true);
        }

        //linearLayout = (LinearLayout) findViewById(R.id.address_create);
        // linearLayout.setVisibility(View.GONE);

        Intent intent = getIntent();
        TotalPrice = intent.getStringExtra("TotalPrice");

        Button btnAddress = (Button) findViewById(R.id.btnAddress);
        btnAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ii = new Intent(ShoppingInformationActivity.this, AddNewAddressActivity.class);
                ii.putExtra("ShoppingInformation", "ShoppingInformation");
                ii.putExtra("TotalPrice", TotalPrice);
                startActivity(ii);
                // startActivity(new Intent(ShoppingInformationActivity.this, AddNewAddressActivity.class));
                // finish();
            }
        });

        final UserModel userModel = PrefUtils.getCurrentUserModel(ShoppingInformationActivity.this);
        if (userModel == null) {
            startActivity(new Intent(ShoppingInformationActivity.this, LoginActivity.class));
        } else {
            String URL = Constant.GET_ADDRESS + "/" + userModel.getId();
            fetchAddress(URL);
            rowListItem = new ArrayList<>();
            alertDialogManager = new AlertDialogManager();
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
            rView = (RecyclerView) findViewById(R.id.recyclerView1);
            rView.setHasFixedSize(false);
            rView.setLayoutManager(mLayoutManager);
            rView.setItemAnimator(new DefaultItemAnimator());
            rView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

            cartModels = new ArrayList<>();
            rcAdapter = new DeliveryAddressAdapter(this, rowListItem);
            rView.setAdapter(rcAdapter);


            TextView txtPrice = (TextView) findViewById(R.id.price);
            TextView txtTotalCost = (TextView) findViewById(R.id.total_cost);
            txtTotalCost.setText(TotalPrice);
            txtPrice.setText(TotalPrice);

            final ProgressDialog progressDialog = new ProgressDialog(ShoppingInformationActivity.this);
            progressDialog.setTitle("Shopping Information");
            progressDialog.setMessage("Please wait...");
            addressInfoModel = new AddressInfoModel();
            Button btnNext = (Button) findViewById(R.id.btnNext);
            btnNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {


                    addressInfoModel = PrefUtils.getAddress(ShoppingInformationActivity.this);
                    if (addressInfoModel == null) {
                        alertDialogManager.showAlertDialog(ShoppingInformationActivity.this, "Error", "You need to select an address. Kindly create an address from the side-menu", false);
                    } else {
                        progressDialog.show();

                        StringRequest strReq = new StringRequest(Request.Method.POST, Constant.POST_PREVIEW_CHECKOUT,
                                new com.android.volley.Response.Listener<String>() {

                                    @Override
                                    public void onResponse(String response) {
                                        Log.d("TAGGG", String.valueOf(response));
                                        progressDialog.dismiss();
                                        try {
                                            JSONObject object = new JSONObject(response);


                                            JSONArray array = object.getJSONArray("Carts");

                                            for (int i = 0; i < array.length(); i++) {

                                                try {
                                                    final JSONObject obj = array.getJSONObject(i);


                                                    CartModel2 mydic = new CartModel2();
                                                    mydic.setId(obj.getString("Id"));
                                                    mydic.setProductId(obj.getString("product_id"));
                                                    mydic.setImageProductUrl(obj.getString("ProductImage"));
                                                    mydic.setPrice(obj.getString("total_price"));
                                                    mydic.setQuantity(obj.getString("quantity"));
                                                    mydic.setName(obj.getString("Name"));
                                                    mydic.setProductionTime(obj.getString("ProductionTime"));
                                                    cartModels.add(mydic);

                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }

                                                //  Toast.makeText(getApplicationContext(), rowListItem.size(), Toast.LENGTH_LONG).show();
                                            }
                                            Bundle bundle = new Bundle();
                                            Intent ii = new Intent(ShoppingInformationActivity.this, OrderConfirmationActivity.class);
                                            ii.putExtra("BaseFare", object.getString("BaseFare"));
                                            ii.putExtra("BaseFareEstimate", object.getString("BaseFareEstimate"));
                                            ii.putExtra("Distance", object.getString("Distance"));
                                            ii.putExtra("DistanceCalc", object.getString("DistanceCalc"));
                                            ii.putExtra("Duration", object.getString("Duration"));
                                            ii.putExtra("Email", object.getString("Email"));
                                            ii.putExtra("KMCalc", object.getString("KMCalc"));
                                            ii.putExtra("PricePerKilometer", object.getString("PricePerKilometer"));
                                            ii.putExtra("PriceperMins", object.getString("PriceperMins"));
                                            ii.putExtra("ProductPrice", object.getString("ProductPrice"));
                                            ii.putExtra("TOTAL_PRICE", object.getString("TOTAL_PRICE"));
                                            ii.putExtra("Address", object.getString("Address"));
                                            ii.putExtra("landmark", object.getString("landmark"));
                                            ii.putExtra("door", object.getString("door"));
                                            ii.putExtra("initialprice", TotalPrice);
                                            ii.putExtra("FareRate", object.getString("FareRate"));
                                            bundle.putParcelableArrayList("carts", cartModels);


                                            ii.putExtras(bundle);
                                            startActivity(ii);

                                            Log.d("CARTS", String.valueOf(cartModels.size()));

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    }
                                }, new com.android.volley.Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                                NetworkResponse response = error.networkResponse;
                                if (error instanceof ServerError && response != null) {

                                }
                            }
                        }) {

                            @Override
                            protected Map<String, String> getParams() {

                                Map<String, String> params = new HashMap<String, String>();
                                params.put("CustomerId", userModel.getId());
                                params.put("AddressId", addressInfoModel.getId());
                                Log.d("CHECKOUT", params.toString());
                                return params;
                            }
                        };
                        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                        strReq.setRetryPolicy(retryPolicy);
                        AppController.getInstance().addToRequestQueue(strReq);
                    }
                }
            });


        }

    }

    private void fetchAddress(String URL) {

        final ProgressDialog progress = new ProgressDialog(ShoppingInformationActivity.this);
        progress.setTitle("A D D R E S S E S");
        progress.setMessage("Loading...");
        progress.show();
        progress.setCancelable(false);
        JsonArrayRequest request = new JsonArrayRequest(URL,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        progress.dismiss();
                        Log.d("LL", response.toString());
                        try {
                            if (response.toString().equals("[]")) {
                                linearLayout.setVisibility(View.VISIBLE);
                               // Toast.makeText(ShoppingInformationActivity.this, "There is no address found.", Toast.LENGTH_LONG).show();

                                return;
                            }

                            for (int i = 0; i < response.length(); i++) {

                                try {
                                    final JSONObject obj = response.getJSONObject(i);

                                    AddressModel mydic = new AddressModel();
                                    mydic.setId(obj.getString("Id"));
                                    mydic.setAddress(obj.getString("Address"));
                                    mydic.setCustomerId(obj.getString("CustomerId"));
                                    mydic.setLatitude(obj.getString("latitude"));
                                    mydic.setLongitude(obj.getString("longitude"));
                                    mydic.setLandmark(obj.getString("landmark"));
                                    mydic.setDoor(obj.getString("door"));

                                    rowListItem.add(mydic);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            rcAdapter.notifyDataSetChanged();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        rcAdapter.notifyDataSetChanged();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error in getting json

                progress.dismiss();
               // linearLayout.setVisibility(View.VISIBLE);
                Toast.makeText(getApplicationContext(), "Opps! Something went wrong, please try again later ", Toast.LENGTH_SHORT).show();
            }
        });

        AppController.getInstance().addToRequestQueue(request);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                Intent ii = new Intent(ShoppingInformationActivity.this, CustomerActivity.class);
                startActivity(ii);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
