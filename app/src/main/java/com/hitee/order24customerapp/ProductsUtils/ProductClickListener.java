package com.hitee.order24customerapp.ProductsUtils;

import android.view.View;

public interface ProductClickListener {
    void itemProductClicked(View view, int position);
}