package com.hitee.order24customerapp.ProductsUtils;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.hitee.order24customerapp.Adapter.ProductAdapter;
import com.hitee.order24customerapp.Models.ProductModel;
import com.hitee.order24customerapp.Models.UserModel;
import com.hitee.order24customerapp.R;
import com.hitee.order24customerapp.ShoppingUtils.ShoppingCartActivity;
import com.hitee.order24customerapp.Utils.Constant;
import com.hitee.order24customerapp.Utils.PrefUtils;
import com.hitee.order24customerapp.Utils.RecyclerTouchListener;
import com.hitee.order24customerapp.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ProductListActivity extends AppCompatActivity {

    private static final String TAG = ProductListActivity.class.getSimpleName();
    private RecyclerView recyclerView;
    private ArrayList<ProductModel> cartList;
    private ProductAdapter rcAdapter1;
    private ShimmerFrameLayout mShimmerViewContainer;
    String CategoryId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);

        final ActionBar abar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.custom_actionbar, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        TextView txttitle = (TextView) viewActionBar.findViewById(R.id.txtTitle);
        txttitle.setText("L I S T O F P R O D U C T S");
        txttitle.setTextSize(14);
        if (abar != null) {
            abar.setCustomView(viewActionBar, params);

            abar.setDisplayShowCustomEnabled(true);
            abar.setDisplayShowTitleEnabled(false);
            abar.setHomeButtonEnabled(false);
            abar.setDisplayHomeAsUpEnabled(true);
        }


        cartList = new ArrayList<>();
        Intent intent = getIntent();
        final String MenuId = intent.getStringExtra("MenuId");
        fetchProducts(Constant.GET_PRODUCTS_MENU + "/" + MenuId);
        Log.d("URLL", Constant.GET_PRODUCTS_MENU + "/" + MenuId);


        GridLayoutManager lLayout1 = new GridLayoutManager(this, 2);

        RecyclerView rView1 = (RecyclerView) findViewById(R.id.recyclerView1);
        rView1.setLayoutManager(lLayout1);
        rView1.setNestedScrollingEnabled(false);
        rView1.setHasFixedSize(false);

        rcAdapter1 = new ProductAdapter(this, cartList);
        rView1.setAdapter(rcAdapter1);


        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);

        rView1.addOnItemTouchListener(new RecyclerTouchListener(this, rView1, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                final ProductModel doc = cartList.get(position);
                Intent i = new Intent(ProductListActivity.this, ProductDetailsActivity.class);
                i.putExtra("getProductId", doc.getProductId());
                i.putExtra("getBusinessName", doc.getBusinessName());
                i.putExtra("getDescription", doc.getDescription());
                i.putExtra("getImageUrl", doc.getImageUrl());
                i.putExtra("getName", doc.getName());
                i.putExtra("getPrice", doc.getPrice());
                i.putExtra("getProductionTime", doc.getProductionTime());
                i.putExtra("getThumbnail", doc.getThumbnail());
                startActivity(i);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

    }

    private void fetchProducts(String URL) {

        Log.d("MENU", URL);
        final ProgressDialog progress = new ProgressDialog(ProductListActivity.this);
        progress.setMessage("Loading...");
        progress.show();

        JsonArrayRequest request = new JsonArrayRequest(URL,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        progress.dismiss();
                        Log.d("LL", response.toString());
                        try {
                            if (response.toString().equals("[]")) {
                                Toast.makeText(ProductListActivity.this, "There is no menu found.", Toast.LENGTH_LONG).show();
                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);
                                return;
                            }

                            for (int i = 0; i < response.length(); i++) {

                                try {
                                    final JSONObject obj = response.getJSONObject(i);

                                    ProductModel mydic = new ProductModel();
                                    mydic.setProductId(obj.getString("ProductId"));
                                    mydic.setBusinessName(obj.getString("BusinessName"));
                                    mydic.setDescription(obj.getString("Description"));
                                    mydic.setThumbnail(obj.getString("Thumbnail"));
                                    mydic.setImageUrl(obj.getString("ImageUrl"));
                                    mydic.setProductionTime(obj.getString("ProductionTime"));
                                    mydic.setPrice(obj.getString("Price"));
                                    mydic.setName(obj.getString("Name"));

                                    cartList.add(mydic);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            rcAdapter1.notifyDataSetChanged();
                            // stop animating Shimmer and hide the layout
                            mShimmerViewContainer.stopShimmerAnimation();
                            mShimmerViewContainer.setVisibility(View.GONE);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        rcAdapter1.notifyDataSetChanged();

                        // stop animating Shimmer and hide the layout
                        mShimmerViewContainer.stopShimmerAnimation();
                        mShimmerViewContainer.setVisibility(View.GONE);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error in getting json
                Log.e(TAG, "Error: " + error.getMessage());
                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
                progress.dismiss();
                Toast.makeText(getApplicationContext(), "Opps! Something went wrong, please try again later ", Toast.LENGTH_SHORT).show();
            }
        });

        AppController.getInstance().addToRequestQueue(request);


    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.product_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.action_shopping_cart:
                UserModel userModel = PrefUtils.getCurrentUserModel(ProductListActivity.this);
                if (userModel == null) {

                } else {

                    startActivity(new Intent(ProductListActivity.this, ShoppingCartActivity.class));
                }
                break;
            default:
                break;
        }
        return true;
    }


}
