package com.hitee.order24customerapp.ProductsUtils;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.hitee.order24customerapp.Adapter.AddOnAdapter;
import com.hitee.order24customerapp.Adapter.ProductAdapter;
import com.hitee.order24customerapp.Bottom.CustomerActivity;
import com.hitee.order24customerapp.LoginUtils.LoginActivity;
import com.hitee.order24customerapp.Models.AddOnModel;
import com.hitee.order24customerapp.Models.ProductModel;
import com.hitee.order24customerapp.Models.UserModel;
import com.hitee.order24customerapp.OrderUtils.OrderConfirmationActivity;
import com.hitee.order24customerapp.R;
import com.hitee.order24customerapp.RegisterUtils.RegisterActivity;
import com.hitee.order24customerapp.ShoppingUtils.ShoppingCartActivity;
import com.hitee.order24customerapp.Utils.AlertDialogManager;
import com.hitee.order24customerapp.Utils.Constant;
import com.hitee.order24customerapp.Utils.PrefUtils;
import com.hitee.order24customerapp.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ProductDetailsActivity extends AppCompatActivity {
    AlertDialogManager alertDialogManager;

    private static final String TAG = ProductListActivity.class.getSimpleName();
    private RecyclerView recyclerView;
    private ArrayList<AddOnModel> cartList;
    private AddOnAdapter rcAdapter1;
    private ShimmerFrameLayout mShimmerViewContainer;
    String CategoryId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);

        final ActionBar abar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.custom_actionbar, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        TextView txttitle = (TextView) viewActionBar.findViewById(R.id.txtTitle);
        txttitle.setText("P R O D U C T  D E T A I L S");
        txttitle.setTextSize(14);
        if (abar != null) {
            abar.setCustomView(viewActionBar, params);

            abar.setDisplayShowCustomEnabled(true);
            abar.setDisplayShowTitleEnabled(false);
            abar.setHomeButtonEnabled(false);
            abar.setDisplayHomeAsUpEnabled(true);
        }
        Intent intent = getIntent();
        final String getProductId = intent.getStringExtra("getProductId");
        alertDialogManager = new AlertDialogManager();
        cartList = new ArrayList<>();

        String FULL_URL = Constant.GET_PRODUCTS_ADDON + "/" + getProductId;
        fetchProducts(FULL_URL);

        Log.d("URL_ME", FULL_URL);

        RecyclerView rView1 = (RecyclerView) findViewById(R.id.recyclerView1);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        rView1.setLayoutManager(mLayoutManager);
        rView1.setNestedScrollingEnabled(false);
        rView1.setHasFixedSize(false);

        rcAdapter1 = new AddOnAdapter(this, cartList);
        rView1.setAdapter(rcAdapter1);


        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);


        final String getBusinessName = intent.getStringExtra("getBusinessName");
        final String getDescription = intent.getStringExtra("getDescription");
        final String getImageUrl = intent.getStringExtra("getImageUrl");
        final String getName = intent.getStringExtra("getName");
        final String getPrice = intent.getStringExtra("getPrice");
        final String getProductionTime = intent.getStringExtra("getProductionTime");
        final String getThumbnail = intent.getStringExtra("getThumbnail");

        ImageView imageMain = (ImageView) findViewById(R.id.imageMain);
        TextView title = (TextView) findViewById(R.id.title);
        TextView description = (TextView) findViewById(R.id.description);
        TextView price = (TextView) findViewById(R.id.price);
        TextView productiontime = (TextView) findViewById(R.id.category);
        TextView businessname = (TextView) findViewById(R.id.businessname);


        title.setText(getName);
        description.setText(getDescription);
        price.setText("₦ " + getPrice);
        productiontime.setText(getProductionTime);
        businessname.setText(getBusinessName);


        Glide.with(this)
                .load(Constant.IMAGE_URL + getImageUrl)
                .apply(new RequestOptions()
                        .placeholder(R.mipmap.fff)
                        .fitCenter())
                .into(imageMain);


        final TextView numItem = (TextView) findViewById(R.id.numItem);
        CardView btnbuttonMinus = (CardView) findViewById(R.id.buttonMinus);

        CardView btnbuttonPlus = (CardView) findViewById(R.id.buttonPlus);

        final ProgressDialog waitingDialog = new ProgressDialog(this);
        waitingDialog.setTitle("Adding cart");
        waitingDialog.setMessage("Please wait while we add item to your cart");
        Button btnAddCart = (Button) findViewById(R.id.btnAddToChart);
        btnAddCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                waitingDialog.show();

                final UserModel userModel = PrefUtils.getCurrentUserModel(ProductDetailsActivity.this);
                if (userModel == null) {

                    Intent ii = new Intent(ProductDetailsActivity.this, LoginActivity.class);
                    ii.putExtra("PRODUCT_DETAILS", "PD");
                    ii.putExtra("getProductId", getProductId);
                    ii.putExtra("getBusinessName", getBusinessName);
                    ii.putExtra("getDescription", getDescription);
                    ii.putExtra("getImageUrl", getImageUrl);
                    ii.putExtra("getName", getName);
                    ii.putExtra("getPrice", getPrice);
                    ii.putExtra("getProductionTime", getProductionTime);
                    ii.putExtra("getThumbnail", getThumbnail);

                    startActivity(ii);

                } else {

                    StringRequest strReq = new StringRequest(Request.Method.POST, Constant.POST_CART,
                            new com.android.volley.Response.Listener<String>() {

                                @Override
                                public void onResponse(String response) {
                                    Log.d("TAGGG", String.valueOf(response));
                                    waitingDialog.dismiss();
                                    try {
                                        if (response.equals("200")) {
                                            startActivity(new Intent(ProductDetailsActivity.this, ShoppingCartActivity.class));

                                        } else {
                                            alertDialogManager.showAlertDialog(ProductDetailsActivity.this, "Failed", "Opps!!!, You cannot order from another store at the same time.", false);
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                }
                            }, new com.android.volley.Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            NetworkResponse response = error.networkResponse;
                            if (error instanceof ServerError && response != null) {

                            }
                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("ProductId", getProductId);
                            params.put("Quantity", numItem.getText().toString());
                            params.put("UserId", userModel.getId());

                            Log.d("POST_TAG", params.toString());
                            return params;
                        }
                    };
                    DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                    strReq.setRetryPolicy(retryPolicy);
                    AppController.getInstance().addToRequestQueue(strReq);

                }
            }
        });


        btnbuttonMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int num = Integer.parseInt(numItem.getText().toString());
                if (num > 1) {
                    num--;
                }
                numItem.setText(Integer.toString(num));
            }
        });

        btnbuttonPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int num2 = Integer.parseInt(numItem.getText().toString());
                num2++;
                numItem.setText(Integer.toString(num2));
            }
        });


    }

    private void fetchProducts(String URL) {
        Log.d("URL_ME", URL);
        final ProgressDialog progress = new ProgressDialog(ProductDetailsActivity.this);
        progress.setMessage("Loading...");
        progress.show();

        JsonArrayRequest request = new JsonArrayRequest(URL,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        progress.dismiss();
                        Log.d("LL", response.toString());
                        try {
                            if (response.toString().equals("[]")) {
                               // Toast.makeText(ProductDetailsActivity.this, "There is no add found.", Toast.LENGTH_LONG).show();
                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);
                                return;
                            }

                            for (int i = 0; i < response.length(); i++) {

                                try {
                                    final JSONObject obj = response.getJSONObject(i);

                                    AddOnModel mydic = new AddOnModel();
                                    mydic.setId(obj.getString("Id"));
                                    mydic.setBusinessAddOnId(obj.getString("businessAddonId"));
                                    mydic.setPrice(obj.getString("Price"));
                                    mydic.setName(obj.getString("Name"));

                                    cartList.add(mydic);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            rcAdapter1.notifyDataSetChanged();
                            // stop animating Shimmer and hide the layout
                            mShimmerViewContainer.stopShimmerAnimation();
                            mShimmerViewContainer.setVisibility(View.GONE);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        rcAdapter1.notifyDataSetChanged();

                        // stop animating Shimmer and hide the layout
                        mShimmerViewContainer.stopShimmerAnimation();
                        mShimmerViewContainer.setVisibility(View.GONE);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error in getting json
                Log.e(TAG, "Error: " + error.getMessage());
                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
                progress.dismiss();
                Toast.makeText(getApplicationContext(), "Opps! Something went wrong, please try again later ", Toast.LENGTH_SHORT).show();
            }
        });

        AppController.getInstance().addToRequestQueue(request);


    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                Intent ii = new Intent(ProductDetailsActivity.this, CustomerActivity.class);
                startActivity(ii);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
