package com.hitee.order24customerapp.RegisterUtils;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.hitee.order24customerapp.LoginUtils.LoginActivity;
import com.hitee.order24customerapp.R;
import com.hitee.order24customerapp.Utils.AlertDialogManager;
import com.hitee.order24customerapp.Utils.Constant;
import com.hitee.order24customerapp.app.AppController;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {
    AlertDialogManager alertDialogManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        final ActionBar abar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.custom_actionbar, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        TextView txttitle = (TextView) viewActionBar.findViewById(R.id.txtTitle);
        txttitle.setText("R E G  I S T E R");
        txttitle.setTextSize(14);
        if (abar != null) {
            abar.setCustomView(viewActionBar, params);

            abar.setDisplayShowCustomEnabled(true);
            abar.setDisplayShowTitleEnabled(false);
            abar.setHomeButtonEnabled(false);
            abar.setDisplayHomeAsUpEnabled(true);

        }
        alertDialogManager = new AlertDialogManager();

        TextView txtLogin = (TextView) findViewById(R.id.login);
        txtLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                finish();
            }
        });
        final EditText txtFirstName = (EditText) findViewById(R.id.input_name);
        final EditText input_lastname = (EditText) findViewById(R.id.input_lastname);
        final EditText input_email = (EditText) findViewById(R.id.input_email);
        final EditText input_phone = (EditText) findViewById(R.id.input_phone);
        final EditText input_password = (EditText) findViewById(R.id.input_password);
        final EditText input_referral = (EditText) findViewById(R.id.input_referral);

        final ProgressDialog waitingDialog = new ProgressDialog(this);
        waitingDialog.setTitle("Registration");
        waitingDialog.setMessage("Please wait while we register your account");

        Button btnRegister = (Button) findViewById(R.id.btn_signup);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                waitingDialog.show();

                if (txtFirstName.getText().toString().equals("") && input_lastname.getText().toString().equals("") && input_email.getText().toString().equals("")
                        && input_phone.getText().toString().equals("") && input_password.getText().toString().equals("")) {

                    alertDialogManager.showAlertDialog(RegisterActivity.this, "Error", "Please fill up the empty field (s)", false);

                } else {


                    StringRequest strReq = new StringRequest(Request.Method.POST, Constant.REGISTER,
                            new com.android.volley.Response.Listener<String>() {

                                @Override
                                public void onResponse(String response) {
                                    Log.d("TAGGG", String.valueOf(response));
                                    waitingDialog.dismiss();
                                    try {
                                        if (response.equals("200")) {
                                            alertDialogManager.showAlertDialog(RegisterActivity.this, "Success", "Your account has been created", true);
                                            txtFirstName.setText("");
                                            input_lastname.setText("");
                                            input_email.setText("");
                                            input_phone.setText("");
                                            input_password.setText("");
                                            startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                                            finish();

                                        } else if (response.equals("204")) {
                                            alertDialogManager.showAlertDialog(RegisterActivity.this, "Failed", "Opps!!!, The user already exist", false);
                                        } else if (response.equals("404")) {
                                            alertDialogManager.showAlertDialog(RegisterActivity.this, "Failed", "Opps!!!, Please check the information supplied and try again.", false);
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                }
                            }, new com.android.volley.Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            NetworkResponse response = error.networkResponse;
                            if (error instanceof ServerError && response != null) {

                            }
                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("Email", input_email.getText().toString().trim());
                            params.put("FirstName", txtFirstName.getText().toString().trim());
                            params.put("LastName", input_lastname.getText().toString().trim());
                            params.put("PhoneNumber", input_phone.getText().toString().trim());
                            params.put("Password", input_password.getText().toString().trim());
                            params.put("Referral", input_referral.getText().toString().trim());
                            Log.d("POST_TAG", params.toString());
                            return params;
                        }
                    };
                    DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                    strReq.setRetryPolicy(retryPolicy);
                    AppController.getInstance().addToRequestQueue(strReq);

                }
            }
        });
    }

}
