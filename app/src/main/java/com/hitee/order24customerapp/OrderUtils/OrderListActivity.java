package com.hitee.order24customerapp.OrderUtils;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.hitee.order24customerapp.Adapter.CheckoutAddOnAdapter;
import com.hitee.order24customerapp.Adapter.OrderListAdapter;
import com.hitee.order24customerapp.Adapter.ShoppingAdapter;
import com.hitee.order24customerapp.AddressUtils.AddressInfoActivity;
import com.hitee.order24customerapp.Bottom.CustomerActivity;
import com.hitee.order24customerapp.Models.AddOnList;
import com.hitee.order24customerapp.Models.CartModel;
import com.hitee.order24customerapp.Models.OrderDetailModel;
import com.hitee.order24customerapp.Models.OrderHistoryModel;
import com.hitee.order24customerapp.Models.UserModel;
import com.hitee.order24customerapp.PaymentUtils.BankOtpActivity;
import com.hitee.order24customerapp.R;
import com.hitee.order24customerapp.RegisterUtils.RegisterActivity;
import com.hitee.order24customerapp.ShoppingUtils.ShoppingCartActivity;
import com.hitee.order24customerapp.TrackingUtils.TrackingListActivity;
import com.hitee.order24customerapp.Utils.AlertDialogManager;
import com.hitee.order24customerapp.Utils.Constant;
import com.hitee.order24customerapp.Utils.PrefUtils;
import com.hitee.order24customerapp.app.AppController;
import com.tubb.smrv.SwipeMenuRecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class OrderListActivity extends AppCompatActivity {
    private ArrayList<OrderDetailModel> rowListItem;
    private RecyclerView rView;

    OrderListAdapter rcAdapter;
    TextView txtPrice, subtotal, txtCounter;
    AlertDialogManager alertDialogManager;
    private ArrayList<AddOnList> addOnLists;
    private RecyclerView AddOnrView;
    CheckoutAddOnAdapter checkoutAddOnAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list);

        final ActionBar abar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.custom_actionbar, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        TextView txttitle = (TextView) viewActionBar.findViewById(R.id.txtTitle);
        txttitle.setText("O R D E R  L I S T");
        txttitle.setTextSize(12);
        if (abar != null) {
            abar.setCustomView(viewActionBar, params);

            abar.setDisplayShowCustomEnabled(true);
            abar.setDisplayShowTitleEnabled(false);
            abar.setHomeButtonEnabled(false);
            abar.setDisplayHomeAsUpEnabled(true);
        }

        String OrderId = getIntent().getStringExtra("getOrderId");
        String getBookingID = getIntent().getStringExtra("getBookingID");
        String getOrderValue = getIntent().getStringExtra("getOrderValue");
        String getBusinessAddress = getIntent().getStringExtra("getBusinessAddress");
        String getBusinessID = getIntent().getStringExtra("getBusinessID");
        String getBusinessPhone = getIntent().getStringExtra("getBusinessPhone");
        String getCustomerId = getIntent().getStringExtra("getCustomerId");
        String getCustomerPhone = getIntent().getStringExtra("getCustomerPhone");
        String getCustomerName = getIntent().getStringExtra("getCustomerName");
        String getCreatedDate = getIntent().getStringExtra("getCreatedDate");
        String getDropAddress = getIntent().getStringExtra("getDropAddress");
        String getTxId = getIntent().getStringExtra("getTxId");
        String getStatus = getIntent().getStringExtra("tranStatus");
        Log.d("STATUS", getStatus);
        UserModel userModel = PrefUtils.getCurrentUserModel(OrderListActivity.this);

        alertDialogManager = new AlertDialogManager();
        final TextView name = (TextView) findViewById(R.id.name);
        final TextView history = (TextView) findViewById(R.id.history);
        final TextView order_at_time = (TextView) findViewById(R.id.order_at_time);
        final TextView delivery_type = (TextView) findViewById(R.id.delivery_type);
        final TextView deliver_address = (TextView) findViewById(R.id.deliver_address);

        name.setText(getCustomerName);
        history.setText(getBusinessPhone);
        order_at_time.setText(getCreatedDate);
        deliver_address.setText(getDropAddress);
        delivery_type.setText(getBookingID);

        String URL = Constant.GET_ORDER_LIST + "/" + OrderId; //userModel.getId();

        rowListItem = new ArrayList<>();
        getAllItemList(URL);


        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        rView = (RecyclerView) findViewById(R.id.recycle_view);
        rView.setHasFixedSize(false);
        rView.setLayoutManager(layoutManager);
        rView.setNestedScrollingEnabled(false);
        rcAdapter = new OrderListAdapter(this, rowListItem);
        rView.setAdapter(rcAdapter);


        String URL_ADDON = Constant.GET_ORDER_LIST_ADD_ONS + "/" + OrderId;
        addOnLists = new ArrayList<>();
        getAllAddOnItemList(URL_ADDON);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);


        AddOnrView = (RecyclerView) findViewById(R.id.recycle_view_addon);
        AddOnrView.setHasFixedSize(false);


        AddOnrView.setLayoutManager(layoutManager1);
        AddOnrView.setNestedScrollingEnabled(false);
        checkoutAddOnAdapter = new CheckoutAddOnAdapter(this, addOnLists);
        AddOnrView.setAdapter(checkoutAddOnAdapter);

        final ProgressDialog waitingDialog = new ProgressDialog(this);
        waitingDialog.setTitle("Confirming Order");
        waitingDialog.setMessage("Please wait while we submit your request");
        // final String url = Constant.UPDATE_ORDER_STATUS + "/" + OrderId;

        Button btnReceived = (Button) findViewById(R.id.order_received);
        Button btnorder_completed = (Button) findViewById(R.id.order_completed);
        if (getStatus.equals("SETTLED")) {
            btnReceived.setVisibility(View.GONE);
            btnorder_completed.setVisibility(View.VISIBLE);
        } else {
            btnReceived.setVisibility(View.VISIBLE);
            btnorder_completed.setVisibility(View.GONE);
            btnReceived.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    waitingDialog.show();

                    StringRequest strReq = new StringRequest(Request.Method.POST, Constant.UPDATE_ORDER_STATUS,
                            new com.android.volley.Response.Listener<String>() {

                                @Override
                                public void onResponse(String response) {
                                    Log.d("TAGGG", String.valueOf(response));
                                    waitingDialog.dismiss();
                                    try {
                                        if (response.equals("200")) {
                                            alertDialogManager.showAlertDialog(OrderListActivity.this, "Success", "Thank you for confirming your order", true);


                                        } else if (response.equals("204")) {
                                            alertDialogManager.showAlertDialog(OrderListActivity.this, "Failed", "Opps! Please try again.", false);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                }
                            }, new com.android.volley.Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            NetworkResponse response = error.networkResponse;
                            if (error instanceof ServerError && response != null) {

                            }
                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("id", getTxId);
                            params.put("customer_id", userModel.getId());
                            params.put("booking_id", getBookingID);
                            Log.d("POST_TAG", params.toString());
                            return params;
                        }
                    };
                    DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                    strReq.setRetryPolicy(retryPolicy);
                    AppController.getInstance().addToRequestQueue(strReq);

                }
            });
        }
    }

    private void getAllItemList(String url) {
        final ProgressDialog progress = new ProgressDialog(OrderListActivity.this);
        progress.setTitle("O R D E R  L I S T");
        progress.setMessage("Loading...");
        progress.show();


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null,
                new Response.Listener<JSONObject>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(JSONObject response) {
                        progress.dismiss();
                        try {
                            JSONArray genreArry = response.getJSONArray("OrderDetails");
                            for (int i = 0; i < genreArry.length(); i++) {

                                try {
                                    final JSONObject obj = genreArry.getJSONObject(i);
                                    OrderDetailModel mydic = new OrderDetailModel();
                                    mydic.setProductId(obj.getString("ProductId"));
                                    mydic.setProductImage(obj.getString("ProductImage"));
                                    mydic.setPrice(obj.getString("Price"));
                                    mydic.setQuantity(obj.getString("Quantity"));
                                    mydic.setStatus(obj.getString("Status"));
                                    mydic.setProductName(obj.getString("ProductName"));
                                    rowListItem.add(mydic);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                            rcAdapter.notifyDataSetChanged();


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error in getting json
                // Log.e("TAG", "Error: " + error.getMessage());
                progress.dismiss();
                Toast.makeText(getApplicationContext(), "Opps! Something went wrong, please try again later ", Toast.LENGTH_SHORT).show();
            }
        });


        AppController.getInstance().addToRequestQueue(jsonObjReq);

    }


    private void getAllAddOnItemList(String url) {

        JsonArrayRequest request = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        //progress.dismiss();
                        Log.d("LL", response.toString());
                        try {
                            if (response.toString().equals("[]")) {
                                //Toast.makeText(OrderListActivity.this, "There is no addon found.", Toast.LENGTH_LONG).show();

                                return;
                            }

                            for (int i = 0; i < response.length(); i++) {

                                try {
                                    final JSONObject obj = response.getJSONObject(i);

                                    AddOnList mydic = new AddOnList();
                                    mydic.setId(obj.getString("Id"));
                                    mydic.setName(obj.getString("Name"));
                                    mydic.setQuantity(obj.getString("Quantity"));
                                    mydic.setTotal(obj.getString("Total"));

                                    addOnLists.add(mydic);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            checkoutAddOnAdapter.notifyDataSetChanged();
                            // stop animating Shimmer and hide the layout

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        checkoutAddOnAdapter.notifyDataSetChanged();

                        // stop animating Shimmer and hide the layout

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error in getting json

                //  progress.dismiss();
                Toast.makeText(getApplicationContext(), "Opps! Something went wrong, please try again later ", Toast.LENGTH_SHORT).show();
            }
        });

        AppController.getInstance().addToRequestQueue(request);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                Intent ii = new Intent(OrderListActivity.this, CustomerActivity.class);
                startActivity(ii);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}
