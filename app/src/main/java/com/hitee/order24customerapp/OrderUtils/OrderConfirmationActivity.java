package com.hitee.order24customerapp.OrderUtils;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.icu.text.UnicodeSetSpanner;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andrognito.pinlockview.IndicatorDots;
import com.andrognito.pinlockview.PinLockListener;
import com.andrognito.pinlockview.PinLockView;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.flutterwave.raveandroid.Meta;
import com.flutterwave.raveandroid.RaveConstants;
import com.flutterwave.raveandroid.RavePayActivity;
import com.flutterwave.raveandroid.RavePayManager;
import com.hitee.order24customerapp.Adapter.CartViewAddOnAdapter;
import com.hitee.order24customerapp.Adapter.CheckoutAddOnAdapter;
import com.hitee.order24customerapp.Adapter.OrderConfirmationAdapter;
import com.hitee.order24customerapp.Adapter.ShoppingAdapter;
import com.hitee.order24customerapp.Bottom.CustomerActivity;
import com.hitee.order24customerapp.LoginUtils.LoginActivity;
import com.hitee.order24customerapp.Models.AddOnList;
import com.hitee.order24customerapp.Models.AddressInfoModel;
import com.hitee.order24customerapp.Models.CartModel;
import com.hitee.order24customerapp.Models.CartModel2;
import com.hitee.order24customerapp.Models.UserModel;
import com.hitee.order24customerapp.ParcelUtils.ParcelListActivity;
import com.hitee.order24customerapp.ParcelUtils.PaymentActivity;
import com.hitee.order24customerapp.PaymentUtils.RavePaymentActivity;
import com.hitee.order24customerapp.R;
import com.hitee.order24customerapp.ShoppingUtils.ShoppingCartActivity;
import com.hitee.order24customerapp.ShoppingUtils.ShoppingInformationActivity;
import com.hitee.order24customerapp.Utils.AlertDialogManager;
import com.hitee.order24customerapp.Utils.Constant;
import com.hitee.order24customerapp.Utils.PrefUtils;
import com.hitee.order24customerapp.WalletUtils.CustomerWalletActivity;
import com.hitee.order24customerapp.app.AppController;
import com.tubb.smrv.SwipeMenuRecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;

public class OrderConfirmationActivity extends AppCompatActivity {

    UserModel userModel;
    AddressInfoModel addressInfoModel;
    AlertDialogManager alertDialogManager;

    JSONObject jsonObject = null;
    private ArrayList<AddOnList> addOnLists;

    private RecyclerView AddOnrView;
    CheckoutAddOnAdapter rcAdapter;
    String PricePerKilometer, PriceperMins, DistanceCalc, KMCalc, Distance, BaseFare, Duration, Total;
    ArrayList<CartModel2> cartss = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_confirmation);
        // setupToolbar();

        final ActionBar abar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.custom_actionbar, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        TextView txttitle = (TextView) viewActionBar.findViewById(R.id.txtTitle);
        txttitle.setText("O R D E R  C O N F I R M A T I O N");
        txttitle.setTextSize(12);
        if (abar != null) {
            abar.setCustomView(viewActionBar, params);

            abar.setDisplayShowCustomEnabled(true);
            abar.setDisplayShowTitleEnabled(false);
            abar.setHomeButtonEnabled(false);
        }


        userModel = PrefUtils.getCurrentUserModel(OrderConfirmationActivity.this);
        addressInfoModel = PrefUtils.getAddress(OrderConfirmationActivity.this);
        if (userModel == null) {
            startActivity(new Intent(OrderConfirmationActivity.this, LoginActivity.class));
        } else {

            String URL_ADDON = Constant.GET_LIST_PRODUCTS_ADDON + "/" + userModel.getId();
            addOnLists = new ArrayList<>();
            getAllAddOnItemList(URL_ADDON);
            LinearLayoutManager layoutManager1 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);


            AddOnrView = (RecyclerView) findViewById(R.id.recyclerView1);
            AddOnrView.setHasFixedSize(false);


            AddOnrView.setLayoutManager(layoutManager1);
            AddOnrView.setNestedScrollingEnabled(false);
            rcAdapter = new CheckoutAddOnAdapter(this, addOnLists);
            AddOnrView.setAdapter(rcAdapter);

            if (getIntent().getExtras().getParcelableArrayList("carts") != null) {

                cartss = Objects.requireNonNull(this.getIntent().getExtras()).getParcelableArrayList("carts");// (ArrayList<CartModel2>) getIntent().getParcelableExtra("carts");
            }

            String BaseFareEstimate = getIntent().getStringExtra("BaseFareEstimate");
            final String TOTAL_PRICE = getIntent().getStringExtra("TOTAL_PRICE");
            String initialprice = getIntent().getStringExtra("initialprice");
            String Fare = getIntent().getStringExtra("FareRate");

            String Address = getIntent().getStringExtra("Address");
            String landmark = getIntent().getStringExtra("landmark");
            String door = getIntent().getStringExtra("door");

            PricePerKilometer = getIntent().getStringExtra("PricePerKilometer");
            PriceperMins = getIntent().getStringExtra("PriceperMins");
            DistanceCalc = getIntent().getStringExtra("DistanceCalc");
            KMCalc = getIntent().getStringExtra("KMCalc");
            Distance = getIntent().getStringExtra("Distance");
            BaseFare = getIntent().getStringExtra("BaseFare");
            Duration = getIntent().getStringExtra("Duration");
            Total = String.valueOf(Double.parseDouble(BaseFare) + Double.parseDouble(DistanceCalc) + Double.parseDouble(KMCalc));


            TextView txtaddress = (TextView) findViewById(R.id.address);
            TextView txtlandmark = (TextView) findViewById(R.id.landmark);
            TextView txtdoor = (TextView) findViewById(R.id.door);
            TextView txtPrice = (TextView) findViewById(R.id.price);
            TextView txtaddress_price = (TextView) findViewById(R.id.address_price);
            alertDialogManager = new AlertDialogManager();


            txtaddress.setText(Address);
            txtlandmark.setText(landmark);
            txtdoor.setText(door);
            txtPrice.setText(initialprice);
            txtaddress_price.setText("₦ " + Fare);


            LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

            RecyclerView rView = (RecyclerView) findViewById(R.id.recyclerView);
            rView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
            rView.setLayoutManager(layoutManager);
            rView.setNestedScrollingEnabled(true);

            OrderConfirmationAdapter rcAdapter = new OrderConfirmationAdapter(this, cartss);
            rView.setAdapter(rcAdapter);


            RelativeLayout relAnalysis = (RelativeLayout) findViewById(R.id.analysis);
            relAnalysis.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showCustomDialog();
                }
            });

            Button btnFinishOrder = (Button) findViewById(R.id.btnFinishOrder);
            btnFinishOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Meta meta = new Meta("rave_escrow_tx", "1"); // for escrow
                    List<Meta> users = new ArrayList<>();
                    users.add(meta);

                    String Fullname = userModel.getFirstName() + " " + userModel.getLastName();
                    Intent ii = new Intent(OrderConfirmationActivity.this, RavePaymentActivity.class);
                    ii.putExtra("Amount", TOTAL_PRICE);
                    ii.putExtra("AddressId", addressInfoModel.getId());
                    startActivity(ii);

                }
            });


            Button btnWallet = (Button) findViewById(R.id.btnWallet);
            btnWallet.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ViewGroup viewGroup = findViewById(android.R.id.content);

                    //then we will inflate the custom alert dialog xml that we created
                    View dialogView = LayoutInflater.from(v.getContext()).inflate(R.layout.lock_screen, viewGroup, false);
                    PinLockView mPinLockView = (PinLockView) dialogView.findViewById(R.id.pin_lock_view);
                    IndicatorDots mIndicatorDots = (IndicatorDots) dialogView.findViewById(R.id.indicator_dots);
                    AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());

                    //setting the view of the builder to our custom view that we already inflated
                    builder.setView(dialogView);

                    //finally creating the alert dialog and displaying it
                    AlertDialog alertDialog = builder.create();
                    mPinLockView.attachIndicatorDots(mIndicatorDots);
                    mPinLockView.setPinLockListener(new PinLockListener() {
                        @Override
                        public void onComplete(String pin) {
                            final ProgressDialog progress = new ProgressDialog(v.getContext());
                            progress.setTitle("Pin Confirmation");
                            progress.setMessage("Please wait till we verify the pin");
                            progress.show();


                            StringRequest strReq = new StringRequest(Request.Method.POST, Constant.PIN_WALLET,
                                    new com.android.volley.Response.Listener<String>() {

                                        @Override
                                        public void onResponse(String response) {
                                            Log.d("PIN_RESPONSE", response);
                                            progress.dismiss();
                                            try {

                                                try {
                                                    jsonObject = new JSONObject(response);

                                                    if (String.valueOf(jsonObject.getString("HttpStatusCode")).equals("200")) {
                                                        double amount = Double.parseDouble(jsonObject.getString("Amount"));
                                                        double amountDue = Double.parseDouble(TOTAL_PRICE);
                                                        if (amount < amountDue) {
                                                            alertDialogManager.showAlertDialog(v.getContext(), "WALLET", "The wallet amount is less than the order amount. Kindly fund your wallet", false);
                                                            alertDialog.dismiss();
                                                        } else
                                                            {


                                                            StringRequest strReq = new StringRequest(Request.Method.POST, Constant.PAYWITHWALLET,
                                                                    new com.android.volley.Response.Listener<String>() {

                                                                        @Override
                                                                        public void onResponse(String response) {
                                                                            Log.d("TAGGGGGGGG", String.valueOf(response));
                                                                            progress.dismiss();
                                                                            try {
                                                                                if (response.equals("200")) {
                                                                                    startActivity(new Intent(OrderConfirmationActivity.this, OrderSubmittedActivity.class));
                                                                                } else {
                                                                                    alertDialogManager.showAlertDialog(OrderConfirmationActivity.this, "Submission failed", "Your order is not submitted", false);
                                                                                }

                                                                            } catch (Exception e) {
                                                                                e.printStackTrace();
                                                                            }

                                                                        }
                                                                    }, new com.android.volley.Response.ErrorListener() {
                                                                @Override
                                                                public void onErrorResponse(VolleyError error) {

                                                                    NetworkResponse response = error.networkResponse;
                                                                    if (error instanceof ServerError && response != null) {

                                                                    }
                                                                }
                                                            }) {

                                                                @Override
                                                                protected Map<String, String> getParams() {

                                                                    Map<String, String> params = new HashMap<String, String>();
                                                                    params.put("CustomerId", userModel.getId());
                                                                    params.put("Amount", TOTAL_PRICE);
                                                                    params.put("IP", "0");
                                                                    params.put("appfee", "0");
                                                                    params.put("charged_amount", TOTAL_PRICE);
                                                                    params.put("currency", "NGN");
                                                                    params.put("flwRef", "WALLET");
                                                                    params.put("narration", "WALLET");
                                                                    params.put("orderRef", "WALLET");
                                                                    params.put("raveRef", "WALLET");
                                                                    params.put("vbRespcode", "00");
                                                                    params.put("vbrespmessage", "00");
                                                                    params.put("AddressId", addressInfoModel.getId());
                                                                    params.put("FirstName", userModel.getFirstName());
                                                                    params.put("LastName", userModel.getLastName());
                                                                    params.put("PhoneNumber", userModel.getPhoneNumber());
                                                                    params.put("Email", userModel.getEmail());
                                                                    params.put("transaction_type", "DEBIT");
                                                                    Log.d("POST_TAGG", params.toString());
                                                                    return params;
                                                                }
                                                            };
                                                            DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                                                            strReq.setRetryPolicy(retryPolicy);
                                                            AppController.getInstance().addToRequestQueue(strReq);

                                                        }

                                                    } else {

                                                        Toast.makeText(v.getContext(), "Wrong pin supplied", Toast.LENGTH_SHORT).show();
                                                    }

                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }

                                        }
                                    }, new com.android.volley.Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {

                                    NetworkResponse response = error.networkResponse;
                                    if (error instanceof ServerError && response != null) {

                                    }
                                }
                            }) {

                                @Override
                                protected Map<String, String> getParams() {

                                    Map<String, String> params = new HashMap<String, String>();
                                    params.put("code", pin);
                                    params.put("CustomerId", userModel.getId());
                                    return params;
                                }
                            };
                            DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                            strReq.setRetryPolicy(retryPolicy);
                            AppController.getInstance().addToRequestQueue(strReq);

                        }

                        @Override
                        public void onEmpty() {

                        }

                        @Override
                        public void onPinChange(int pinLength, String intermediatePin) {

                        }
                    });

                    mPinLockView.enableLayoutShuffling();

                    mPinLockView.setPinLength(4);
                    mPinLockView.setTextColor(ContextCompat.getColor(v.getContext(), R.color.white));

                    mIndicatorDots.setIndicatorType(IndicatorDots.IndicatorType.FILL_WITH_ANIMATION);

                    //Now we need an AlertDialog.Builder object

                    alertDialog.show();


                }
            });
        }
    }


    private void showCustomDialog() {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(this).inflate(R.layout.distance_calc, viewGroup, false);


        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);

        //finally creating the alert dialog and displaying it
        AlertDialog alertDialog = builder.create();
        TextView txtBaseFare = (TextView) dialogView.findViewById(R.id.basefare);
        TextView txtpriceperkilometer = (TextView) dialogView.findViewById(R.id.priceperkilometer);
        TextView txtpricsepermin = (TextView) dialogView.findViewById(R.id.pricsepermin);
        TextView txtdistance = (TextView) dialogView.findViewById(R.id.distance);
        TextView txtduration = (TextView) dialogView.findViewById(R.id.duration);
        TextView txdurationCalc = (TextView) dialogView.findViewById(R.id.durationCalcValue);
        TextView txtdistanceCalc = (TextView) dialogView.findViewById(R.id.distanceCalcValue);
        TextView totalCalcValue = (TextView) dialogView.findViewById(R.id.totalCalcValue);

        txtBaseFare.setText("₦ " + BaseFare);
        txtpriceperkilometer.setText("₦ " + PricePerKilometer + "  /  1 KM");
        txtpricsepermin.setText("₦ " + PriceperMins + " /  1 Min");
        txtdistance.setText(Distance);
        txtduration.setText(Duration);

        txtdistanceCalc.setText("₦ " + DistanceCalc);
        txdurationCalc.setText("₦ " + KMCalc);
        totalCalcValue.setText("₦ " + Total);


        Button btnOk = (Button) dialogView.findViewById(R.id.btnOkay);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();


    }


    private void getAllAddOnItemList(String url) {
//        final ProgressDialog progress = new ProgressDialog(ShoppingCartActivity.this);
//        progress.setTitle("A D D O N");
//        progress.setMessage("Loading...");
//        progress.show();


        JsonArrayRequest request = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        //progress.dismiss();
                        Log.d("LL", response.toString());
                        try {
                            if (response.toString().equals("[]")) {
                                // Toast.makeText(OrderConfirmationActivity.this, "There is no addon found.", Toast.LENGTH_LONG).show();

                                return;
                            }

                            for (int i = 0; i < response.length(); i++) {

                                try {
                                    final JSONObject obj = response.getJSONObject(i);

                                    AddOnList mydic = new AddOnList();
                                    mydic.setId(obj.getString("Id"));
                                    mydic.setName(obj.getString("Name"));
                                    mydic.setQuantity(obj.getString("Quantity"));
                                    mydic.setTotal(obj.getString("Total"));

                                    addOnLists.add(mydic);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            rcAdapter.notifyDataSetChanged();
                            // stop animating Shimmer and hide the layout

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        rcAdapter.notifyDataSetChanged();

                        // stop animating Shimmer and hide the layout

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error in getting json

                //  progress.dismiss();
                Toast.makeText(getApplicationContext(), "Opps! Something went wrong, please try again later ", Toast.LENGTH_SHORT).show();
            }
        });

        AppController.getInstance().addToRequestQueue(request);

    }


    public String GetREF() {
        Long max = 0L;
        Long min = 100000000000L;
        //Use the date format that best suites you
        @SuppressLint("SimpleDateFormat") SimpleDateFormat spf = new SimpleDateFormat("dd/MM/yyyy");
        Long randomLong = 0L;
        for (int i = 0; i <= 10; i++) {
            Random r = new Random();
            randomLong = (r.nextLong() % (max - min)) + min;
            Date dt = new Date(randomLong);
        }
        return randomLong.toString();
    }

    /**/
    private void setupToolbar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setTitle("Order Confirmation");
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RaveConstants.RAVE_REQUEST_CODE && data != null) {
            String message = data.getStringExtra("response");
            if (resultCode == RavePayActivity.RESULT_SUCCESS) {
                Log.d("FLUTTER", message);

                try {
                    JSONObject object = new JSONObject(message);
                    final JSONObject arr = object.getJSONObject("data");
                    Log.d("ARRAY", arr.toString());
                    // Toast.makeText(OrderConfirmationActivity.this, arr.getString("amount"), Toast.LENGTH_LONG).show();

                    final String amount = arr.getString("amount");
                    final String IP = arr.getString("IP");
                    final String appfee = arr.getString("appfee");
                    final String charged_amount = arr.getString("charged_amount");
                    final String currency = arr.getString("currency");
                    final String flwRef = arr.getString("flwRef");
                    final String narration = arr.getString("narration");
                    final String orderRef = arr.getString("orderRef");
                    final String raveRef = arr.getString("raveRef");
                    final String txRef = arr.getString("txRef");
                    final String vbRespcode = arr.getString("vbvrespcode");
                    final String vbrespmessage = arr.getString("vbvrespmessage");

                    // Toast.makeText(this, "SUCCESS " + message, Toast.LENGTH_SHORT).show();
                    final ProgressDialog progress = new ProgressDialog(OrderConfirmationActivity.this);
                    progress.setTitle("O R D E R  CONFIRMATION");
                    progress.setMessage("Submitting Order to restaurant...");
                    progress.show();


                    StringRequest strReq = new StringRequest(Request.Method.POST, Constant.POST_CHECKOUT,
                            new com.android.volley.Response.Listener<String>() {

                                @Override
                                public void onResponse(String response) {
                                    Log.d("TAGGGGGGGG", String.valueOf(response));
                                    progress.dismiss();
                                    try {
                                        if (response.equals("200")) {
                                            startActivity(new Intent(OrderConfirmationActivity.this, OrderSubmittedActivity.class));
                                        } else {
                                            alertDialogManager.showAlertDialog(OrderConfirmationActivity.this, "Submission failed", "Your order is not submitted", false);
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                }
                            }, new com.android.volley.Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            NetworkResponse response = error.networkResponse;
                            if (error instanceof ServerError && response != null) {

                            }
                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("CustomerId", userModel.getId());
                            params.put("amount", amount);
                            params.put("IP", IP);
                            params.put("appfee", appfee);
                            params.put("charged_amount", charged_amount);
                            params.put("currency", currency);
                            params.put("flwRef", flwRef);
                            params.put("narration", narration);
                            params.put("orderRef", orderRef);
                            params.put("raveRef", raveRef);
                            params.put("txRef", txRef);
                            params.put("vbRespcode", vbRespcode);
                            params.put("vbrespmessage", vbrespmessage);
                            params.put("AddressId", addressInfoModel.getId());
                            params.put("FirstName", userModel.getFirstName());
                            params.put("LastName", userModel.getLastName());
                            params.put("PhoneNumber", userModel.getPhoneNumber());
                            params.put("Email", userModel.getEmail());

                            Log.d("POST_TAGG", params.toString());
                            return params;
                        }
                    };
                    DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                    strReq.setRetryPolicy(retryPolicy);
                    AppController.getInstance().addToRequestQueue(strReq);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                Intent ii = new Intent(OrderConfirmationActivity.this, CustomerActivity.class);
                startActivity(ii);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}
