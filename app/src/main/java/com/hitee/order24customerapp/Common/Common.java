package com.hitee.order24customerapp.Common;

import android.location.Location;

import com.hitee.order24customerapp.Models.User;
import com.hitee.order24customerapp.Remote.GoogleMapAPI;
import com.hitee.order24customerapp.Remote.IGoogleAPI;
import com.hitee.order24customerapp.Remote.RetrofitClient;

public class Common {


    public static String currentToken = "";
    public static final String driver_tbl = "Drivers";
    public static final String user_driver_tbl = "DriversInformation";
    public static final String user_rider_tbl = "RidersInformation";
    public static final String pickup_request_tbl = "PickupRequest";
    public static final String token_tbl = "Token";

    public static final String baseURL = "https://maps.googleapis.com";
    public static final String fcmURL = "https://fcm.googleapis.com";

    public static final String googleAPIUrl = "https://maps.googleapis.com";

    public static User currentUser;

    public static Location mLastLocation = null;

    public static IGoogleAPI getGoogleAPI() {
        return RetrofitClient.getClient(baseURL).create(IGoogleAPI.class);

    }

    private static double base_fare = 400;
    private static double time_rate = 0.35;
    private static double price_per_km = 60;
    private static double distance_rate = 1.75;


    public static double getPrice(double km, int min) {
        return (base_fare + (price_per_km * km) + (distance_rate * min));
    }

    public static IGoogleAPI getGoogleService() {
        return GoogleMapAPI.getClient(googleAPIUrl).create(IGoogleAPI.class);

    }


}
