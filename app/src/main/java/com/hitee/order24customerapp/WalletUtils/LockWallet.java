package com.hitee.order24customerapp.WalletUtils;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andrognito.pinlockview.IndicatorDots;
import com.andrognito.pinlockview.PinLockListener;
import com.andrognito.pinlockview.PinLockView;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.hitee.order24customerapp.Bottom.CustomerActivity;
import com.hitee.order24customerapp.LoginUtils.LoginActivity;
import com.hitee.order24customerapp.Models.UserModel;
import com.hitee.order24customerapp.OrderUtils.OrderConfirmationActivity;
import com.hitee.order24customerapp.ParcelUtils.DeliveryActivity;
import com.hitee.order24customerapp.ParcelUtils.PaymentActivity;
import com.hitee.order24customerapp.R;
import com.hitee.order24customerapp.Utils.AlertDialogManager;
import com.hitee.order24customerapp.Utils.Constant;
import com.hitee.order24customerapp.Utils.PrefUtils;
import com.hitee.order24customerapp.app.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class LockWallet extends AppCompatActivity {


    public static final String TAG = "PinLockView";
    AlertDialogManager alertDialogManager;
    private PinLockView mPinLockView;
    private IndicatorDots mIndicatorDots;
    UserModel userModel;
    private PinLockListener mPinLockListener = new PinLockListener() {
        @Override
        public void onComplete(String pin) {
           // Timber.tag(TAG).d("Pin complete: " + pin);


            final ProgressDialog progress = new ProgressDialog(LockWallet.this);
            progress.setTitle("Pin Confirmation");
            progress.setMessage("Please wait till we verify the pin");
            progress.show();


            StringRequest strReq = new StringRequest(Request.Method.POST, Constant.PIN_WALLET,
                    new com.android.volley.Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            progress.dismiss();
                            try {
                                JSONObject jsonObject = null;
                                try {
                                    jsonObject = new JSONObject(response);
                                    if (String.valueOf(jsonObject.getString("HttpStatusCode")).equals("200")) {
                                        // String amount = jsonObject.getString("Amount");
                                        Intent ii = new Intent(LockWallet.this, PaymentActivity.class);
                                        ii.putExtra("Amount", jsonObject.getString("Amount"));
                                        startActivity(ii);

                                    } else {
                                        alertDialogManager.showAlertDialog(LockWallet.this, "Failed", "Wrong Pin Supplied", false);
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    NetworkResponse response = error.networkResponse;
                    if (error instanceof ServerError && response != null) {

                    }
                }
            }) {

                @Override
                protected Map<String, String> getParams() {

                    Map<String, String> params = new HashMap<String, String>();
                    params.put("code", pin);
                    params.put("CustomerId", userModel.getId());
                    Log.d("PRAMA", String.valueOf(params));
                    return params;
                }
            };
            DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            strReq.setRetryPolicy(retryPolicy);
            AppController.getInstance().addToRequestQueue(strReq);

            Toast.makeText(LockWallet.this, pin, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onEmpty() {
            Log.d("Error","Pin empty");
        }

        @Override
        public void onPinChange(int pinLength, String intermediatePin) {
            Log.d(TAG, "Pin changed, new length " + pinLength + " with intermediate pin " + intermediatePin);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.lock_screen);

        userModel = PrefUtils.getCurrentUserModel(LockWallet.this);
        if (userModel == null) {
            startActivity(new Intent(LockWallet.this, LoginActivity.class));
        } else {

            mPinLockView = (PinLockView) findViewById(R.id.pin_lock_view);
            mIndicatorDots = (IndicatorDots) findViewById(R.id.indicator_dots);
            alertDialogManager = new AlertDialogManager();
            mPinLockView.attachIndicatorDots(mIndicatorDots);
            mPinLockView.setPinLockListener(mPinLockListener);
            //mPinLockView.setCustomKeySet(new int[]{2, 3, 1, 5, 9, 6, 7, 0, 8, 4});
            mPinLockView.enableLayoutShuffling();

            mPinLockView.setPinLength(4);
            mPinLockView.setTextColor(ContextCompat.getColor(this, R.color.white));

            mIndicatorDots.setIndicatorType(IndicatorDots.IndicatorType.FILL_WITH_ANIMATION);

            TextView profile_name = (TextView) findViewById(R.id.profile_name);

            profile_name.setText("Welcome " + userModel.getFirstName());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                Intent ii = new Intent(LockWallet.this, CustomerActivity.class);
                startActivity(ii);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}