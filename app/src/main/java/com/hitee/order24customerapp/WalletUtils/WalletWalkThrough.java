package com.hitee.order24customerapp.WalletUtils;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.andrognito.pinlockview.IndicatorDots;
import com.andrognito.pinlockview.PinLockListener;
import com.andrognito.pinlockview.PinLockView;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.hitee.order24customerapp.AddressUtils.AddNewAddressActivity;
import com.hitee.order24customerapp.AddressUtils.AddressActivity;
import com.hitee.order24customerapp.Bottom.CustomerActivity;
import com.hitee.order24customerapp.LoginUtils.LoginActivity;
import com.hitee.order24customerapp.Models.UserModel;
import com.hitee.order24customerapp.Models.WalletCodeInfoModel;
import com.hitee.order24customerapp.OrderUtils.OrderConfirmationActivity;
import com.hitee.order24customerapp.ParcelUtils.DeliveryActivity;
import com.hitee.order24customerapp.R;
import com.hitee.order24customerapp.Utils.AlertDialogManager;
import com.hitee.order24customerapp.Utils.Constant;
import com.hitee.order24customerapp.Utils.PrefUtils;
import com.hitee.order24customerapp.Utils.ViewDialog;
import com.hitee.order24customerapp.app.AppController;

import java.util.HashMap;
import java.util.Map;


public class WalletWalkThrough extends AppCompatActivity {

    ViewDialog viewDialog;

    public static final String TAG = "PinLockView";

    private PinLockView mPinLockView;
    private IndicatorDots mIndicatorDots;
    AlertDialogManager alertDialogManager;
    UserModel userModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_walk_through);


        final ActionBar abar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.custom_actionbar, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        TextView txttitle = (TextView) viewActionBar.findViewById(R.id.txtTitle);
        txttitle.setText("S E T  P I N  C O D E");
        txttitle.setTextSize(14);
        if (abar != null) {
            abar.setCustomView(viewActionBar, params);

            abar.setDisplayShowCustomEnabled(true);
            abar.setDisplayShowTitleEnabled(false);
            abar.setHomeButtonEnabled(false);
            abar.setDisplayHomeAsUpEnabled(true);
        }
        alertDialogManager = new AlertDialogManager();

        viewDialog = new ViewDialog(this);

        userModel = PrefUtils.getCurrentUserModel(WalletWalkThrough.this);
        if (userModel == null) {
            startActivity(new Intent(WalletWalkThrough.this, LoginActivity.class));
        } else {


            mPinLockView = (PinLockView) findViewById(R.id.pin_lock_view);
            mIndicatorDots = (IndicatorDots) findViewById(R.id.indicator_dots);

            mPinLockView.attachIndicatorDots(mIndicatorDots);
            mPinLockView.setPinLockListener(mPinLockListener);
            //mPinLockView.setCustomKeySet(new int[]{2, 3, 1, 5, 9, 6, 7, 0, 8, 4});
            //mPinLockView.enableLayoutShuffling();
            mPinLockView.isShowDeleteButton();
            mPinLockView.setShowDeleteButton(true);
            mPinLockView.setPinLength(4);
            mPinLockView.setTextColor(ContextCompat.getColor(this, R.color.white));

            mIndicatorDots.setIndicatorType(IndicatorDots.IndicatorType.FILL_WITH_ANIMATION);
        }

    }


    private PinLockListener mPinLockListener = new PinLockListener() {
        @Override
        public void onComplete(String pin) {
            // Timber.tag(TAG).d("Pin complete: " + pin);
            // Toast.makeText(WalletWalkThrough.this, pin, Toast.LENGTH_SHORT).show();


            final UserModel userModel = PrefUtils.getCurrentUserModel(WalletWalkThrough.this);
            if (userModel == null || userModel.equals("null")) {

                startActivity(new Intent(WalletWalkThrough.this, LoginActivity.class));
            } else {

                viewDialog.showDialog();
                StringRequest strReq = new StringRequest(Request.Method.POST, Constant.SEND_WALLET_CODE,
                        new com.android.volley.Response.Listener<String>() {

                            @Override
                            public void onResponse(String response) {
                                Log.d("TAGGG", String.valueOf(response));
                                viewDialog.hideDialog();
                                try {
                                    if (response.equals("200")) {
                                        alertDialogManager.showAlertDialog(WalletWalkThrough.this, "Success", "Wallet Code Verified", true);
                                        WalletCodeInfoModel model = new WalletCodeInfoModel();
                                        model.setCode(pin);
                                        model.setCustomerId(userModel.getId());
                                        PrefUtils.setWalletCode(model, WalletWalkThrough.this);
                                        startActivity(new Intent(WalletWalkThrough.this, CustomerActivity.class));

                                    } else if (response.equals("406")) {
                                        alertDialogManager.showAlertDialog(WalletWalkThrough.this, "Failed", "Opps!, You have set wallet code already ", false);
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        NetworkResponse response = error.networkResponse;
                        if (error instanceof ServerError && response != null) {

                        }
                    }
                }) {

                    @Override
                    protected Map<String, String> getParams() {

                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Code", pin);
                        params.put("CustomerId", userModel.getId());
                        Log.d("POST_TAG", params.toString());
                        return params;
                    }
                };
                DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                strReq.setRetryPolicy(retryPolicy);
                AppController.getInstance().addToRequestQueue(strReq);
            }
        }

        @Override
        public void onEmpty() {
            Log.d("Error","Pin empty");
        }

        @Override
        public void onPinChange(int pinLength, String intermediatePin) {
            Log.d(TAG, "Pin changed, new length " + pinLength + " with intermediate pin " + intermediatePin);
        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                Intent ii = new Intent(WalletWalkThrough.this, CustomerActivity.class);
                startActivity(ii);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}
