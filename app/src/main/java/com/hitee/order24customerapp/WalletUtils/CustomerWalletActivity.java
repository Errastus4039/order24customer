package com.hitee.order24customerapp.WalletUtils;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.flutterwave.raveandroid.RaveConstants;
import com.flutterwave.raveandroid.RavePayActivity;
import com.flutterwave.raveandroid.RavePayManager;
import com.hitee.order24customerapp.Adapter.ShoppingAdapter;
import com.hitee.order24customerapp.Adapter.WalletAdapter;
import com.hitee.order24customerapp.Bottom.CustomerActivity;
import com.hitee.order24customerapp.LoginUtils.LoginActivity;
import com.hitee.order24customerapp.Models.CartModel;
import com.hitee.order24customerapp.Models.UserModel;
import com.hitee.order24customerapp.Models.WalletModel;
import com.hitee.order24customerapp.OrderUtils.OrderConfirmationActivity;
import com.hitee.order24customerapp.OrderUtils.OrderSubmittedActivity;
import com.hitee.order24customerapp.R;
import com.hitee.order24customerapp.ShoppingUtils.ShoppingCartActivity;
import com.hitee.order24customerapp.Utils.AlertDialogManager;
import com.hitee.order24customerapp.Utils.Constant;
import com.hitee.order24customerapp.Utils.Formmatting;
import com.hitee.order24customerapp.Utils.PrefUtils;
import com.hitee.order24customerapp.Utils.ViewDialog;
import com.hitee.order24customerapp.app.AppController;
import com.tubb.smrv.SwipeMenuRecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class CustomerWalletActivity extends AppCompatActivity {
    UserModel userModel;
    AlertDialogManager alertDialogManager;
    private ArrayList<WalletModel> rowListItem;
    ViewDialog viewDialog;
    WalletAdapter rcAdapter;
    private RecyclerView rView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_wallet);


        final ActionBar abar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.custom_actionbar, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        TextView txttitle = (TextView) viewActionBar.findViewById(R.id.txtTitle);
        txttitle.setText("C U S T O M E R  W A L L E T");
        txttitle.setTextSize(14);
        if (abar != null) {
            abar.setCustomView(viewActionBar, params);

            abar.setDisplayShowCustomEnabled(true);
            abar.setDisplayShowTitleEnabled(false);
            abar.setHomeButtonEnabled(false);
            abar.setDisplayHomeAsUpEnabled(true);
        }
        viewDialog = new ViewDialog(this);
        alertDialogManager = new AlertDialogManager();

        rowListItem = new ArrayList<>();


        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        rView = (RecyclerView) findViewById(R.id.recycle_view);
        rView.setHasFixedSize(false);
        rView.setLayoutManager(layoutManager);
        rView.setNestedScrollingEnabled(false);
        rcAdapter = new WalletAdapter(CustomerWalletActivity.this, rowListItem);
        rView.addItemDecoration(new DividerItemDecoration(CustomerWalletActivity.this, LinearLayoutManager.VERTICAL));

        TextView balance = (TextView) findViewById(R.id.balance);
        userModel = PrefUtils.getCurrentUserModel(CustomerWalletActivity.this);
        if (userModel == null) {
            startActivity(new Intent(CustomerWalletActivity.this, LoginActivity.class));
        } else {

            Button btnWallet = (Button) findViewById(R.id.fund_wallet);
            btnWallet.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    showCustomDialog();
                }
            });


            viewDialog.showDialog();
            String url = Constant.GET_WALLET_BALANCE + "/" + userModel.getId();
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    url, null,
                    new Response.Listener<JSONObject>() {
                        @SuppressLint("SetTextI18n")
                        @Override
                        public void onResponse(JSONObject response) {
                            viewDialog.hideDialog();
                            try {
                                JSONArray genreArry = response.getJSONArray("GetWalletViewModels");
                                // FullPriceModel full = new FullPriceModel();
                                String pp = Formmatting.priceToString(Double.valueOf(response.getString("Balance")));
                                balance.setText("₦ " + pp);
                                for (int i = 0; i < genreArry.length(); i++) {

                                    try {
                                        final JSONObject obj = genreArry.getJSONObject(i);
                                        WalletModel mydic = new WalletModel();
                                        mydic.setId(obj.getString("Id"));
                                        mydic.setAmount(obj.getString("Amount"));
                                        mydic.setDate(obj.getString("fulldate"));
                                        mydic.setDescription(obj.getString("transaction_type"));
                                        mydic.setTxRef(obj.getString("txRef"));

                                        rowListItem.add(mydic);

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                                rcAdapter.notifyDataSetChanged();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // error in getting json
                    Log.d("TAG", "Error: " + error.getMessage());
                    viewDialog.hideDialog();
                    Toast.makeText(getApplicationContext(), "Opps! Something went wrong, please try again later ", Toast.LENGTH_SHORT).show();
                }
            });


            AppController.getInstance().addToRequestQueue(jsonObjReq);
        }

        rView.setAdapter(rcAdapter);

    }


    private void showCustomDialog() {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(this).inflate(R.layout.wallet_balance, viewGroup, false);


        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);

        //finally creating the alert dialog and displaying it
        AlertDialog alertDialog = builder.create();
        alertDialog.show();


        Button btnCard = (Button) dialogView.findViewById(R.id.buttonCard);
        final EditText reason = (EditText) dialogView.findViewById(R.id.cancellation_reason);

        btnCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String amount = reason.getText().toString();
                String Fullname = userModel.getFirstName() + " " + userModel.getLastName();
                new RavePayManager(CustomerWalletActivity.this).setAmount(Double.parseDouble(amount))
                        .setCountry("NG")
                        .setCurrency("NGN")
                        .setEmail(userModel.getEmail())
                        .setfName(Fullname)
                        .setNarration("Wallet payment")
                        .setPublicKey("FLWPUBK-385891242a132f2c90f05da57b16868b-X")
                        .setEncryptionKey("7e405d478e4496acd1daa4eb")
                        .setTxRef(GetREF())
                        .acceptAccountPayments(true)
                        .acceptCardPayments(true)
                        .acceptMpesaPayments(false)
                        .acceptGHMobileMoneyPayments(false)
                        .onStagingEnv(false)
                        .allowSaveCardFeature(true)
                        .setMeta(null)
                        .initialize();

            }
        });


    }

    public String GetREF() {
        Long max = 0L;
        Long min = 100000000000L;
        //Use the date format that best suites you
        @SuppressLint("SimpleDateFormat") SimpleDateFormat spf = new SimpleDateFormat("dd/MM/yyyy");
        Long randomLong = 0L;
        for (int i = 0; i <= 10; i++) {
            Random r = new Random();
            randomLong = (r.nextLong() % (max - min)) + min;
            Date dt = new Date(randomLong);
        }
        return randomLong.toString();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RaveConstants.RAVE_REQUEST_CODE && data != null) {
            String message = data.getStringExtra("response");
            if (resultCode == RavePayActivity.RESULT_SUCCESS) {
                Log.d("FLUTTER", message);

                try {
                    JSONObject object = new JSONObject(message);
                    final JSONObject arr = object.getJSONObject("data");
                    Log.d("ARRAY", arr.toString());
                    // Toast.makeText(OrderConfirmationActivity.this, arr.getString("amount"), Toast.LENGTH_LONG).show();

                    final String amount = arr.getString("amount");
                    final String IP = arr.getString("IP");
                    final String appfee = arr.getString("appfee");
                    final String charged_amount = arr.getString("charged_amount");
                    final String currency = arr.getString("currency");
                    final String flwRef = arr.getString("flwRef");
                    final String narration = arr.getString("narration");
                    final String orderRef = arr.getString("orderRef");
                    final String raveRef = arr.getString("raveRef");
                    final String txRef = arr.getString("txRef");
                    final String vbRespcode = arr.getString("vbvrespcode");
                    final String vbrespmessage = arr.getString("vbvrespmessage");

                    // Toast.makeText(this, "SUCCESS " + message, Toast.LENGTH_SHORT).show();
                    final ProgressDialog progress = new ProgressDialog(CustomerWalletActivity.this);
                    progress.setTitle("WALLET CONFIRMATION");
                    progress.setMessage("Submitting wallet...");
                    progress.show();


                    StringRequest strReq = new StringRequest(Request.Method.POST, Constant.POST_WALLET,
                            new com.android.volley.Response.Listener<String>() {

                                @Override
                                public void onResponse(String response) {
                                    progress.dismiss();
                                    try {
                                        if (response.equals("200")) {
                                            startActivity(new Intent(CustomerWalletActivity.this, WalletSubmittedActivity.class));
                                        } else {
                                            alertDialogManager.showAlertDialog(CustomerWalletActivity.this, "Submission failed", "Your wallet is not submitted", false);
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                }
                            }, new com.android.volley.Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            NetworkResponse response = error.networkResponse;
                            if (error instanceof ServerError && response != null) {

                            }
                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("CustomerId", userModel.getId());
                            params.put("Amount", amount);
                            params.put("IP", IP);
                            params.put("appfee", appfee);
                            params.put("charged_amount", charged_amount);
                            params.put("currency", currency);
                            params.put("flwRef", flwRef);
                            params.put("narration", narration);
                            params.put("orderRef", orderRef);
                            params.put("txRef", txRef);
                            params.put("transaction_type", "CREDIT");
                            params.put("FirstName", userModel.getFirstName());
                            params.put("LastName", userModel.getLastName());
                            params.put("PhoneNumber", userModel.getPhoneNumber());
                            params.put("Email", userModel.getEmail());

                            Log.d("POST_TAGG", params.toString());
                            return params;
                        }
                    };
                    DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                    strReq.setRetryPolicy(retryPolicy);
                    AppController.getInstance().addToRequestQueue(strReq);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                Intent ii = new Intent(CustomerWalletActivity.this, CustomerActivity.class);
                startActivity(ii);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}
