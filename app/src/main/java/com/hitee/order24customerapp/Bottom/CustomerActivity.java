package com.hitee.order24customerapp.Bottom;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.hitee.order24customerapp.Adapter.CategoryAdapter;
import com.hitee.order24customerapp.AddressUtils.AddressActivity;
import com.hitee.order24customerapp.BuildConfig;
import com.hitee.order24customerapp.DashboardUtils.BusinessActivity;
import com.hitee.order24customerapp.LoginUtils.LoginActivity;
import com.hitee.order24customerapp.Models.Category;
import com.hitee.order24customerapp.Models.LocationInfoModel;
import com.hitee.order24customerapp.Models.UserModel;
import com.hitee.order24customerapp.Models.WalletCodeInfoModel;
import com.hitee.order24customerapp.OrderUtils.OrderHistoryActivity;
import com.hitee.order24customerapp.OtherUtils.FAQActivity;
import com.hitee.order24customerapp.OtherUtils.HelpActivity;
import com.hitee.order24customerapp.OtherUtils.ReferralActivity;
import com.hitee.order24customerapp.OtherUtils.WishListActivity;
import com.hitee.order24customerapp.ParcelUtils.ParcelInfoActivity;
import com.hitee.order24customerapp.ParcelUtils.ParcelListActivity;
import com.hitee.order24customerapp.ParcelUtils.SendParcelActivity;
import com.hitee.order24customerapp.R;
import com.hitee.order24customerapp.ShoppingUtils.ShoppingCartActivity;
import com.hitee.order24customerapp.TrackingParcelUtils.TrackingParcelListActivity;
import com.hitee.order24customerapp.TrackingUtils.OrderTracking;
import com.hitee.order24customerapp.Utils.Constant;
import com.hitee.order24customerapp.Utils.PrefUtils;
import com.hitee.order24customerapp.Utils.RecyclerTouchListener;
import com.hitee.order24customerapp.WalletUtils.CustomerWalletActivity;
import com.hitee.order24customerapp.WalletUtils.WalletWalkThrough;
import com.hitee.order24customerapp.app.AppController;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.ButterKnife;

public class CustomerActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = CustomerActivity.class.getSimpleName();
    private RecyclerView recyclerView;
    private List<Category> cartList;
    private CategoryAdapter mAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ShimmerFrameLayout mShimmerViewContainer;
    //String URL;
    private SearchView search;
    DrawerLayout drawer;
    ProgressDialog progressDialog;

    private String mLastUpdateTime;


    private static final int REQUEST_CHECK_SETTINGS = 100;

    private WalletCodeInfoModel walletCodeInfoModel;
    // bunch of location related apis
    private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;

    // boolean flag to toggle the ui
    private Boolean mRequestingLocationUpdates = true;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        Menu menu = navigationView.getMenu();

        MenuItem tools = menu.findItem(R.id.tools);
        MenuItem account = menu.findItem(R.id.account);
        MenuItem auth = menu.findItem(R.id.auth);
        SpannableString s = new SpannableString(tools.getTitle());
        s.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s.length(), 0);
        tools.setTitle(s);


        SpannableString saccount = new SpannableString(account.getTitle());
        saccount.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, saccount.length(), 0);
        account.setTitle(saccount);

        SpannableString sauth = new SpannableString(auth.getTitle());
        sauth.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, sauth.length(), 0);
        auth.setTitle(sauth);


        navigationView.setNavigationItemSelectedListener(this);

        final View headerView = navigationView.getHeaderView(0);

        Menu menuNav = navigationView.getMenu();
        MenuItem login = menuNav.findItem(R.id.nav_login);
        MenuItem logout = menuNav.findItem(R.id.nav_logout);
        UserModel userModel = PrefUtils.getCurrentUserModel(CustomerActivity.this);
        if (userModel == null) {
            login.setEnabled(true);
            login.setVisible(true);

        } else {
            ((TextView) headerView.findViewById(R.id.txtname)).setText(userModel.getFirstName() + " " + userModel.getLastName());
            ((TextView) headerView.findViewById(R.id.txtEmail)).setText(userModel.getEmail());
            ((TextView) headerView.findViewById(R.id.txtPhonenumber)).setText(userModel.getPhoneNumber());
            login.setEnabled(false);
            login.setVisible(false);
            logout.setVisible(true);
            login.setEnabled(true);

        }

        final ActionBar abar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.custom_actionbar, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);

        TextView txttitle = (TextView) viewActionBar.findViewById(R.id.txtTitle);
        txttitle.setText("C A T E G O R I E S");
        txttitle.setTextSize(14);
        if (abar != null) {
            abar.setCustomView(viewActionBar, params);
            abar.setDisplayShowCustomEnabled(true);
            abar.setDisplayShowTitleEnabled(false);
            abar.setHomeButtonEnabled(false);
        }

        Button btnsendparcel = (Button) findViewById(R.id.sendparcel);
        btnsendparcel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CustomerActivity.this, SendParcelActivity.class));
            }
        });

        walletCodeInfoModel = PrefUtils.getWalletCode(CustomerActivity.this);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout2);
        swipeRefreshLayout.setOnRefreshListener(this);

        swipeRefreshLayout.post(
                new Runnable() {
                    @Override
                    public void run() {
                        fetchRecipes(Constant.GET_CATEGORIES);
                    }
                }
        );

        progressDialog = new ProgressDialog(CustomerActivity.this);
        progressDialog.setTitle("C a t e g o r i e s");
        progressDialog.setMessage("Please wait while we load categories.");
        progressDialog.setCancelable(false);

        recyclerView = findViewById(R.id.recycle_view);
        cartList = new ArrayList<>();
        mAdapter = new CategoryAdapter(this, cartList);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        // recyclerView.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.VERTICAL, 16));
        recyclerView.setAdapter(mAdapter);

        // fetchRecipes(Constant.GET_CATEGORIES);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                if (cartList == null) {

                } else {

                    final Category doc = cartList.get(position);
                    Intent i = new Intent(CustomerActivity.this, BusinessActivity.class);
                    i.putExtra("CategoryId", doc.getId());
                    i.putExtra("getCategoryName", doc.getCategoryName());
                    startActivity(i);
                }

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));


        ButterKnife.bind(this);

        // initialize the necessary libraries
        init();
        startLocationButtonClick();
        // restore the values from saved instance state
        restoreValuesFromBundle(savedInstanceState);
    }

    private void fetchRecipes(String URL) {
        progressDialog.show();

        cartList.clear();
        swipeRefreshLayout.setRefreshing(true);
        JsonArrayRequest request = new JsonArrayRequest(URL,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        progressDialog.dismiss();
                        if (response == null) {
                            Toast.makeText(CustomerActivity.this, "Couldn't fetch the menu! Pleas try again.", Toast.LENGTH_LONG).show();
                            return;
                        }
                        swipeRefreshLayout.setRefreshing(false);
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                final JSONObject obj = response.getJSONObject(i);

                                Category mydic = new Category();
                                mydic.setId(obj.getString("Id"));
                                mydic.setCategoryName(obj.getString("CategoryName"));
                                mydic.setDescription(obj.getString("Description"));

                                cartList.add(mydic);


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        mAdapter.notifyDataSetChanged();
                        swipeRefreshLayout.setRefreshing(false);
                        // stop animating Shimmer and hide the layout
                        mShimmerViewContainer.stopShimmerAnimation();
                        mShimmerViewContainer.setVisibility(View.GONE);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error in getting json
                Log.e(TAG, "Error: " + error.getMessage());
                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
                progressDialog.dismiss();
                swipeRefreshLayout.setRefreshing(false);
                Toast.makeText(CustomerActivity.this, "Please swipe down to reload the page", Toast.LENGTH_SHORT).show();
            }
        });

        AppController.getInstance().addToRequestQueue(request);
    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
        // Resuming location updates depending on button state and
        // allowed permissions
        if (mRequestingLocationUpdates && checkPermissions()) {
            startLocationUpdates();
        }

        updateLocationUI();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        if (mRequestingLocationUpdates) {
            // pausing location updates
            // stopLocationUpdates();
        }
        super.onPause();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;

        switch (item.getItemId()) {
            case R.id.nav_address:
                Intent i = new Intent(CustomerActivity.this, AddressActivity.class);
                startActivity(i);
                break;
            case R.id.carts:
                Intent iii = new Intent(CustomerActivity.this, ShoppingCartActivity.class);
                startActivity(iii);
                break;


            case R.id.send_parcel:
                Intent iiiiii = new Intent(CustomerActivity.this, SendParcelActivity.class);
                startActivity(iiiiii);
                break;

            case R.id.myorder:
                Intent ii = new Intent(CustomerActivity.this, OrderHistoryActivity.class);
                startActivity(ii);
                break;

            case R.id.faq:
                Intent iiii = new Intent(CustomerActivity.this, FAQActivity.class);
                startActivity(iiii);
                break;

            case R.id.wallet:
                if (walletCodeInfoModel == null) {
                    Intent wallet = new Intent(CustomerActivity.this, WalletWalkThrough.class);
                    startActivity(wallet);
                    break;

                } else {
                    Intent wallet = new Intent(CustomerActivity.this, CustomerWalletActivity.class);
                    startActivity(wallet);
                    break;
                }

            case R.id.wishlsit:
                Intent wishlsit = new Intent(CustomerActivity.this, WishListActivity.class);
                startActivity(wishlsit);
                break;

            case R.id.list_send_parcel:
                Intent list_send_parcel = new Intent(CustomerActivity.this, ParcelInfoActivity.class);
                startActivity(list_send_parcel);
                break;


            case R.id.tracking_parcel:
                Intent tracking_parcel = new Intent(CustomerActivity.this, TrackingParcelListActivity.class);
                startActivity(tracking_parcel);
                break;

            case R.id.tracking:
                Intent tracking = new Intent(CustomerActivity.this, OrderTracking.class);
                startActivity(tracking);
                break;


            case R.id.referal:
                Intent referal = new Intent(CustomerActivity.this, ReferralActivity.class);
                startActivity(referal);
                break;
            case R.id.help:
                Intent help = new Intent(CustomerActivity.this, HelpActivity.class);
                startActivity(help);
                break;
            case R.id.nav_login:
                Intent login = new Intent(CustomerActivity.this, LoginActivity.class);
                startActivity(login);
                break;
            case R.id.nav_logout:

                PrefUtils.clearAddress(CustomerActivity.this);
                PrefUtils.clearCurrentUser(CustomerActivity.this);
                PrefUtils.clearCurrentUserModel(CustomerActivity.this);
                PrefUtils.clearLocation(CustomerActivity.this);
                PrefUtils.clearWalletCode(CustomerActivity.this);
                PrefUtils.clearReferralCodeModel(CustomerActivity.this);
                Intent nav_logout = new Intent(CustomerActivity.this, LoginActivity.class);
                startActivity(nav_logout);
                finish();
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private boolean loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public void onRefresh() {
        fetchRecipes(Constant.GET_CATEGORIES);
    }


    private void init() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(this);

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                // location is received
                mCurrentLocation = locationResult.getLastLocation();
                mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());

                updateLocationUI();
            }
        };

        mRequestingLocationUpdates = false;

        mLocationRequest = new LocationRequest();
        // mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        //mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    /**
     * Restoring values from saved instance state
     */
    private void restoreValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("is_requesting_updates")) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean("is_requesting_updates");
            }

            if (savedInstanceState.containsKey("last_known_location")) {
                mCurrentLocation = savedInstanceState.getParcelable("last_known_location");
            }

            if (savedInstanceState.containsKey("last_updated_on")) {
                mLastUpdateTime = savedInstanceState.getString("last_updated_on");
            }
        }

        updateLocationUI();
    }


    /**
     * Update the UI displaying the location data
     * and toggling the buttons
     */
    private void updateLocationUI() {
        if (mCurrentLocation != null) {

            LocationInfoModel model = new LocationInfoModel();
            model.setLatitude(mCurrentLocation.getLatitude());
            model.setLongitude(mCurrentLocation.getLongitude());
            PrefUtils.setLocation(model, CustomerActivity.this);
        }

        // toggleButtons();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("is_requesting_updates", mRequestingLocationUpdates);
        outState.putParcelable("last_known_location", mCurrentLocation);
        outState.putString("last_updated_on", mLastUpdateTime);

    }

    /**
     * Starting location updates
     * Check whether location settings are satisfied and then
     * location updates will be requested
     */
    private void startLocationUpdates() {
        mSettingsClient
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.i(TAG, "All location settings are satisfied.");

                        // Toast.makeText(getApplicationContext(), "Started location updates!", Toast.LENGTH_SHORT).show();

                        //noinspection MissingPermission
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());

                        updateLocationUI();
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " +
                                        "location settings ");
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(CustomerActivity.this, REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    Log.i(TAG, "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";
                                Log.e(TAG, errorMessage);

                                Toast.makeText(CustomerActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                        }

                        updateLocationUI();
                    }
                });
    }

    // @OnClick(R.id.btn_start_location_updates)
    public void startLocationButtonClick() {
        // Requesting ACCESS_FINE_LOCATION using Dexter library
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        mRequestingLocationUpdates = true;
                        startLocationUpdates();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        if (response.isPermanentlyDenied()) {
                            // open device settings when the permission is
                            // denied permanently
                            openSettings();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

//    @OnClick(R.id.btn_stop_location_updates)
//    public void stopLocationButtonClick() {
//        mRequestingLocationUpdates = false;
//        stopLocationUpdates();
//    }

    public void stopLocationUpdates() {
        // Removing location updates
        mFusedLocationClient
                .removeLocationUpdates(mLocationCallback)
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        //Toast.makeText(getApplicationContext(), "Location updates stopped!", Toast.LENGTH_SHORT).show();
                        //toggleButtons();
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.e(TAG, "User agreed to make required location settings changes.");
                        // Nothing to do. startLocationupdates() gets called in onResume again.
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.e(TAG, "User chose not to make required location settings changes.");
                        mRequestingLocationUpdates = false;
                        break;
                }
                break;
        }
    }

    private void openSettings() {
        Intent intent = new Intent();
        intent.setAction(
                Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package",
                BuildConfig.APPLICATION_ID, null);
        intent.setData(uri);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }


    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }


}
