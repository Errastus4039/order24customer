package com.hitee.order24customerapp.fragments;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.hitee.order24customerapp.Adapter.BusinessAdapter;
import com.hitee.order24customerapp.Adapter.CategoryAdapter;
import com.hitee.order24customerapp.Bottom.CustomerActivity;
import com.hitee.order24customerapp.BusinessUtils.BusinessMenuActivity;
import com.hitee.order24customerapp.DashboardUtils.BusinessActivity;
import com.hitee.order24customerapp.Models.BusinessModel;
import com.hitee.order24customerapp.Models.Category;
import com.hitee.order24customerapp.Models.LocationInfoModel;
import com.hitee.order24customerapp.R;
import com.hitee.order24customerapp.Utils.Constant;
import com.hitee.order24customerapp.Utils.MyDividerItemDecoration;
import com.hitee.order24customerapp.Utils.PrefUtils;
import com.hitee.order24customerapp.Utils.RecyclerTouchListener;
import com.hitee.order24customerapp.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class BusinessFragment extends Fragment {

    private static final String TAG = CustomerActivity.class.getSimpleName();
    private RecyclerView recyclerView;
    private ArrayList<BusinessModel> cartList;
    private BusinessAdapter mAdapter;
    private ShimmerFrameLayout mShimmerViewContainer;
    String CategoryId;
    ProgressDialog progress;
    double latitude, longitude = 0;
    protected LocationManager locationManager;

    public BusinessFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_business, container, false);

        recyclerView = rootView.findViewById(R.id.recycle_view);
        cartList = new ArrayList<>();
        mAdapter = new BusinessAdapter(getContext(), cartList);


        final ActionBar abar = ((BusinessActivity) Objects.requireNonNull(getActivity())).getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.custom_actionbar, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        TextView textviewTitle = (TextView) viewActionBar.findViewById(R.id.txtTitle);
        Intent intent = getActivity().getIntent();
        String CategoryName = intent.getStringExtra("getCategoryName");
        if (CategoryName == null) {
            startActivity(new Intent(getContext(), CustomerActivity.class));
        } else if (CategoryName.startsWith("Restaurant")) {

            textviewTitle.setText("L I S T  O F  R E S T A U R A N T S");
            textviewTitle.setTextSize(14);

        } else if (CategoryName.startsWith("Grocery")) {

            textviewTitle.setText("G R O C E R Y  S T O R ES");
            textviewTitle.setTextSize(14);
        } else if (CategoryName.startsWith("Bar")) {

            textviewTitle.setText("B A R  A N D  D R I N K S");
            textviewTitle.setTextSize(14);
        } else if (CategoryName.startsWith("bakery")) {

            textviewTitle.setText("B A K E R Y  A N D  C O N F E C T I O N E R Y");
            textviewTitle.setTextSize(12);
        }

        if (abar != null) {
            abar.setCustomView(viewActionBar, params);

            abar.setDisplayShowCustomEnabled(true);
            abar.setDisplayShowTitleEnabled(false);
            abar.setHomeButtonEnabled(false);
            abar.setDisplayHomeAsUpEnabled(true);
        }
        progress = new ProgressDialog(getActivity());
        progress.setMessage("Loading...");
        progress.show();


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new MyDividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL, 16));
        recyclerView.setHasFixedSize(false);
        recyclerView.setAdapter(mAdapter);


        CategoryId = intent.getStringExtra("CategoryId");
        LocationInfoModel locationInfoModel = PrefUtils.getLocation(getActivity());
        if (locationInfoModel == null) {
            statusCheck();

        } else {
            latitude = locationInfoModel.getLatitude();
            longitude = locationInfoModel.getLongitude();
            if (CategoryId.equals("")) {
                startActivity(new Intent(getContext(), CustomerActivity.class));
            } else {

                fetchRecipes(Constant.POST_LOCATION);
                mShimmerViewContainer = rootView.findViewById(R.id.shimmer_view_container);

                recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
                    @Override
                    public void onClick(View view, int position) {
                        final BusinessModel doc = cartList.get(position);
                        Intent i = new Intent(getActivity(), BusinessMenuActivity.class);
                        i.putExtra("BusinessId", doc.getId());
                        i.putExtra("getAddress", doc.getAddress());
                        i.putExtra("getBusinessName", doc.getBusinessName());
                        i.putExtra("getDescription", doc.getDescription());
                        i.putExtra("getDistance", doc.getDistance());
                        i.putExtra("getDuration", doc.getDuration());
                        i.putExtra("getEmail", doc.getEmail());
                        i.putExtra("getPhoneNumber", doc.getPhoneNumber());
                        i.putExtra("getLogo", doc.getLogo());
                        startActivity(i);
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));
            }
        }
        return rootView;

    }

    private void fetchRecipes(String URL) {


        StringRequest strReq = new StringRequest(Request.Method.POST, URL,
                new com.android.volley.Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.d("TAGGG", String.valueOf(response));


                        try {

                            if (response == null) {
                                Toast.makeText(getContext(), "Couldn't fetch the menu! Pleas try again.", Toast.LENGTH_LONG).show();
                                return;
                            }
                            JSONArray arr = new JSONArray(response);


                            for (int i = 0; i < arr.length(); i++) {
                                progress.dismiss();
                                try {
                                    final JSONObject obj = arr.getJSONObject(i);

                                    BusinessModel mydic = new BusinessModel();
                                    mydic.setId(obj.getString("Id"));
                                    mydic.setBusinessName(obj.getString("BusinessName"));
                                    mydic.setDescription(obj.getString("Description"));
                                    mydic.setEmail(obj.getString("Email"));
                                    mydic.setPhoneNumber(obj.getString("PhoneNumber"));
                                    mydic.setAddress(obj.getString("Address"));
                                    mydic.setDistance(obj.getString("Distance"));
                                    mydic.setDuration(obj.getString("Duration"));
                                    mydic.setLogo(obj.getString("logo"));

                                    cartList.add(mydic);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            mAdapter.notifyDataSetChanged();
                            // stop animating Shimmer and hide the layout
                            mShimmerViewContainer.stopShimmerAnimation();
                            mShimmerViewContainer.setVisibility(View.GONE);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {

                }
            }
        }) {

            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("CategoryId", CategoryId);
                params.put("Latitude", String.valueOf(latitude));
                params.put("Longitude", String.valueOf(longitude));

                Log.d("POST_TAG", params.toString());
                return params;
            }
        };
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        strReq.setRetryPolicy(retryPolicy);
        AppController.getInstance().addToRequestQueue(strReq);

    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        super.onPause();
    }

    public void statusCheck() {
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }
}
