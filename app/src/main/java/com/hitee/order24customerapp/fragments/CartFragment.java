package com.hitee.order24customerapp.fragments;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.hitee.order24customerapp.Adapter.ShoppingAdapter;
import com.hitee.order24customerapp.DashboardUtils.BusinessActivity;
import com.hitee.order24customerapp.LoginUtils.LoginActivity;
import com.hitee.order24customerapp.Models.CartModel;
import com.hitee.order24customerapp.Models.UserModel;
import com.hitee.order24customerapp.R;
import com.hitee.order24customerapp.ShoppingUtils.ShoppingCartActivity;
import com.hitee.order24customerapp.ShoppingUtils.ShoppingClickListener;
import com.hitee.order24customerapp.ShoppingUtils.ShoppingInformationActivity;
import com.hitee.order24customerapp.Utils.Constant;
import com.hitee.order24customerapp.Utils.PrefUtils;
import com.hitee.order24customerapp.app.AppController;
import com.tubb.smrv.SwipeMenuRecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class CartFragment extends Fragment implements View.OnClickListener, ShoppingClickListener {
    private ArrayList<CartModel> rowListItem;
    private SwipeMenuRecyclerView rView;

    ShoppingAdapter rcAdapter;
    TextView txtPrice, subtotal, txtCounter;


    public CartFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_cart, container, false);

        final ActionBar abar = ((BusinessActivity) Objects.requireNonNull(getActivity())).getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.custom_actionbar, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        TextView textviewTitle = (TextView) viewActionBar.findViewById(R.id.txtTitle);
        textviewTitle.setText("L I S T  O F  C A R T S");
        textviewTitle.setTextSize(14);
        if (abar != null) {
            abar.setCustomView(viewActionBar, params);

            abar.setDisplayShowCustomEnabled(true);
            abar.setDisplayShowTitleEnabled(false);
            abar.setHomeButtonEnabled(false);
            abar.setDisplayHomeAsUpEnabled(true);
        }

        UserModel userModel = PrefUtils.getCurrentUserModel(getActivity());
        if (userModel == null) {
            startActivity(new Intent(getContext(), LoginActivity.class));
        } else {

            String URL = Constant.GET_CARTS + "/" + userModel.getId();
            rowListItem = new ArrayList<>(); //getAllItemList(URL);
            getAllItemList(URL);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);

            txtPrice = (TextView) rootView.findViewById(R.id.price);

            subtotal = (TextView) rootView.findViewById(R.id.subtotal);
            rView = (SwipeMenuRecyclerView) rootView.findViewById(R.id.recyclerView);
            rView.setHasFixedSize(false);
            rView.setLayoutManager(layoutManager);
            rView.setNestedScrollingEnabled(false);
            rcAdapter = new ShoppingAdapter(getActivity(), rowListItem);
            rView.setAdapter(rcAdapter);
            rcAdapter.setClickListener(this);

            Button btnCheckout = (Button) rootView.findViewById(R.id.btnCheckout);
            btnCheckout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent  i = new Intent(getContext(), ShoppingInformationActivity.class);
                    i.putExtra("TotalPrice", txtPrice.getText().toString());
                    startActivity(i);
                }
            });


        }
        return rootView;
    }

    private void getAllItemList(String url) {
        final ProgressDialog progress = new ProgressDialog(getActivity());
        progress.setTitle("C A R T  L I S T");
        progress.setMessage("Loading...");
        progress.show();


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null,
                new Response.Listener<JSONObject>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(JSONObject response) {
                        progress.dismiss();
                        Log.d("LL", response.toString());
                        try {
                            JSONArray genreArry = response.getJSONArray("Carts");

                            // FullPriceModel full = new FullPriceModel();
                            txtPrice.setText("₦ " + response.getString("ProductPrice"));
                            subtotal.setText("₦ " + response.getString("ProductPrice"));
                            for (int i = 0; i < genreArry.length(); i++) {

                                try {
                                    final JSONObject obj = genreArry.getJSONObject(i);


                                    CartModel mydic = new CartModel();
                                    mydic.setProductId(obj.getString("product_id"));
                                    mydic.setImageProductUrl(obj.getString("ProductImage"));
                                    mydic.setPrice(obj.getString("total_price"));
                                    mydic.setQuantity(obj.getString("quantity"));
                                    mydic.setName(obj.getString("Name"));
                                    mydic.setProductionTime(obj.getString("ProductionTime"));
                                    //  mydic.setFullPrice(full);
                                    rowListItem.add(mydic);


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            rcAdapter.notifyDataSetChanged();
//                            // stop animating Shimmer and hide the layout
//                            mShimmerViewContainer.stopShimmerAnimation();
//                            mShimmerViewContainer.setVisibility(View.GONE);
                            Log.d("ro", String.valueOf(rowListItem.size()));

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
//                        rcAdapter1.notifyDataSetChanged();

                        // stop animating Shimmer and hide the layout
//                        mShimmerViewContainer.stopShimmerAnimation();
//                        mShimmerViewContainer.setVisibility(View.GONE);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error in getting json
                Log.e("TAG", "Error: " + error.getMessage());
//                mShimmerViewContainer.stopShimmerAnimation();
//                mShimmerViewContainer.setVisibility(View.GONE);
                progress.dismiss();
                Toast.makeText(getContext(), "Opps! Something went wrong, please try again later ", Toast.LENGTH_SHORT).show();
            }
        });

        AppController.getInstance().addToRequestQueue(jsonObjReq);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_shopping_cart:
                //Toast.makeText(getContext(), "action shopping cart clicked!", Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }
        return true;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonValidate:
                //Toast.makeText(getContext(), "Button promo code clicked!", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnCheckout:
              //  Toast.makeText(getContext(), "Button Checkout clicked!", Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }

    }

    @Override
    public void itemClicked(View view, int position) {
        int pos = position + 1;
       // Toast.makeText(getContext(), "Position " + pos + " clicked!", Toast.LENGTH_SHORT).show();
    }

}
