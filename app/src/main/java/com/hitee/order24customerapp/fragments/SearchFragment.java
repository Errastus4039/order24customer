package com.hitee.order24customerapp.fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.hitee.order24customerapp.Adapter.BusinessAdapter;
import com.hitee.order24customerapp.Adapter.SearchBusinessAdapter;
import com.hitee.order24customerapp.Bottom.CustomerActivity;
import com.hitee.order24customerapp.BusinessUtils.BusinessMenuActivity;
import com.hitee.order24customerapp.DashboardUtils.BusinessActivity;
import com.hitee.order24customerapp.Models.BusinessModel;
import com.hitee.order24customerapp.Models.LocationInfoModel;
import com.hitee.order24customerapp.Models.UserModel;
import com.hitee.order24customerapp.R;
import com.hitee.order24customerapp.Utils.Constant;
import com.hitee.order24customerapp.Utils.MyDividerItemDecoration;
import com.hitee.order24customerapp.Utils.PrefUtils;
import com.hitee.order24customerapp.Utils.RecyclerTouchListener;
import com.hitee.order24customerapp.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment {

    private static final String TAG = CustomerActivity.class.getSimpleName();
    private RecyclerView recyclerView;
    private ArrayList<BusinessModel> cartList;
    private SearchBusinessAdapter mAdapter;
    private ShimmerFrameLayout mShimmerViewContainer;
    String CategoryId;
    ProgressDialog progress;
    double latitude, longitude = 0;
    private SearchView search;

    public SearchFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_search, container, false);
        recyclerView = rootView.findViewById(R.id.recycle_view);
        cartList = new ArrayList<>();
        mAdapter = new SearchBusinessAdapter(getContext(), cartList);
//        search = (SearchView) rootView.findViewById(R.id.search);
//        search.setQueryHint("Search for store");

        final ActionBar abar = ((BusinessActivity) Objects.requireNonNull(getActivity())).getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.custom_actionbar, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        TextView textviewTitle = (TextView) viewActionBar.findViewById(R.id.txtTitle);
        Intent intent = getActivity().getIntent();

        textviewTitle.setText("S E A R C H  S T O R E S");
        textviewTitle.setTextSize(12);


        if (abar != null) {
            abar.setCustomView(viewActionBar, params);

            abar.setDisplayShowCustomEnabled(true);
            abar.setDisplayShowTitleEnabled(false);
            abar.setHomeButtonEnabled(false);
            abar.setDisplayHomeAsUpEnabled(true);
        }

        EditText txtSearch = (EditText) rootView.findViewById(R.id.search_text);
        Button btnSearch = (Button) rootView.findViewById(R.id.search_btn);

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FetchSearch(Constant.SEARCH_BUSINEES, txtSearch.getText().toString().trim());
            }
        });

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new MyDividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL, 16));
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                final BusinessModel doc = cartList.get(position);
                Intent i = new Intent(getActivity(), BusinessMenuActivity.class);
                i.putExtra("BusinessId", doc.getId());
                i.putExtra("getAddress", doc.getAddress());
                i.putExtra("getBusinessName", doc.getBusinessName());
                i.putExtra("getDescription", doc.getDescription());
                i.putExtra("getDistance", doc.getDistance());
                i.putExtra("getDuration", doc.getDuration());
                i.putExtra("getEmail", doc.getEmail());
                i.putExtra("getPhoneNumber", doc.getPhoneNumber());
                i.putExtra("getLogo", doc.getLogo());
                startActivity(i);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));


        mShimmerViewContainer = rootView.findViewById(R.id.shimmer_view_container);
        CategoryId = intent.getStringExtra("CategoryId");
        LocationInfoModel locationInfoModel = PrefUtils.getLocation(getActivity());

        latitude = locationInfoModel.getLatitude();
        longitude = locationInfoModel.getLongitude();

//        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String s) {
//                FetchSearch(Constant.SEARCH_BUSINEES, s.toString());
//                return false;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String s) {
//                return false;
//            }
//        });


        return rootView;
    }

    private void FetchSearch(String URL, String SearchParameter) {
        progress = new ProgressDialog(getActivity());
        progress.setMessage("Loading...");
        progress.show();
        cartList.clear();

        StringRequest strReq = new StringRequest(Request.Method.POST, URL,
                new com.android.volley.Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.d("TAGGG", String.valueOf(response));
progress.dismiss();

                        try {
                            if (response.toString().equals("[]")) {
                                Toast.makeText(getContext(), "There is no result found.", Toast.LENGTH_LONG).show();
                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);

                                return;
                            }

                            JSONArray arr = new JSONArray(response);


                            for (int i = 0; i < arr.length(); i++) {
                                progress.dismiss();
                                try {
                                    final JSONObject obj = arr.getJSONObject(i);

                                    BusinessModel mydic = new BusinessModel();
                                    mydic.setId(obj.getString("Id"));
                                    mydic.setBusinessName(obj.getString("BusinessName"));
                                    mydic.setDescription(obj.getString("Description"));
                                    mydic.setEmail(obj.getString("Email"));
                                    mydic.setPhoneNumber(obj.getString("PhoneNumber"));
                                    mydic.setAddress(obj.getString("Address"));
                                    mydic.setDistance(obj.getString("Distance"));
                                    mydic.setDuration(obj.getString("Duration"));
                                    mydic.setLogo(obj.getString("logo"));

                                    cartList.add(mydic);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            mAdapter.notifyDataSetChanged();
                            // stop animating Shimmer and hide the layout
                            mShimmerViewContainer.stopShimmerAnimation();
                            mShimmerViewContainer.setVisibility(View.GONE);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {

                }
            }
        }) {

            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("SearchQuery", SearchParameter);
                params.put("Latitude", String.valueOf(latitude));
                params.put("Longitude", String.valueOf(longitude));
                Log.d("POST_TAG", params.toString());
                return params;
            }
        };
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        strReq.setRetryPolicy(retryPolicy);
        AppController.getInstance().addToRequestQueue(strReq);

    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        super.onPause();
    }
}
