package com.hitee.order24customerapp.Models;

/**
 * Created by Babatunde on 22/07/2017.
 */

public class DashboardModel {
    private String TotalApproved;
    private String TotalApprovedAmount;
    private String TotalApprovedAverageAmount;

    private String TotalProcessing;
    private String TotalProcessingAmount;
    private String TotalProcessingAverageAmount;

    private String TotalRejected;
    private String TotalRejectedAmount;
    private String TotalRejectedAverageAmount;

    private String TotalApplication;
    private String TotalApplicationAmount;
    private String TotalApplicationAverageAmount;

    public String getTotalApproved() {
        return TotalApproved;
    }

    public String getTotalApprovedAmount() {
        return TotalApprovedAmount;
    }

    public String getTotalApprovedAverageAmount() {
        return TotalApprovedAverageAmount;
    }

    public String getTotalProcessing() {
        return TotalProcessing;
    }

    public void setTotalApproved(String totalApproved) {
        TotalApproved = totalApproved;
    }

    public String getTotalProcessingAmount() {
        return TotalProcessingAmount;
    }

    public void setTotalApprovedAmount(String totalApprovedAmount) {
        TotalApprovedAmount = totalApprovedAmount;
    }

    public void setTotalApprovedAverageAmount(String totalApprovedAverageAmount) {
        TotalApprovedAverageAmount = totalApprovedAverageAmount;
    }

    public void setTotalProcessing(String totalProcessing) {
        TotalProcessing = totalProcessing;
    }

    public void setTotalProcessingAmount(String totalProcessingAmount) {
        TotalProcessingAmount = totalProcessingAmount;
    }

    public String getTotalApplication() {
        return TotalApplication;
    }

    public String getTotalApplicationAmount() {
        return TotalApplicationAmount;
    }

    public String getTotalApplicationAverageAmount() {
        return TotalApplicationAverageAmount;
    }

    public String getTotalProcessingAverageAmount() {
        return TotalProcessingAverageAmount;
    }

    public String getTotalRejected() {
        return TotalRejected;
    }

    public String getTotalRejectedAmount() {
        return TotalRejectedAmount;
    }

    public String getTotalRejectedAverageAmount() {
        return TotalRejectedAverageAmount;
    }

    public void setTotalProcessingAverageAmount(String totalProcessingAverageAmount) {
        TotalProcessingAverageAmount = totalProcessingAverageAmount;
    }

    public void setTotalApplication(String totalApplication) {
        TotalApplication = totalApplication;
    }

    public void setTotalApplicationAmount(String totalApplicationAmount) {
        TotalApplicationAmount = totalApplicationAmount;
    }

    public void setTotalApplicationAverageAmount(String totalApplicationAverageAmount) {
        TotalApplicationAverageAmount = totalApplicationAverageAmount;
    }

    public void setTotalRejected(String totalRejected) {
        TotalRejected = totalRejected;
    }

    public void setTotalRejectedAmount(String totalRejectedAmount) {
        TotalRejectedAmount = totalRejectedAmount;
    }

    public void setTotalRejectedAverageAmount(String totalRejectedAverageAmount) {
        TotalRejectedAverageAmount = totalRejectedAverageAmount;
    }
    
}
