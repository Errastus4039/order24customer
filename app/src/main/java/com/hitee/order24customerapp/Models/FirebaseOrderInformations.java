package com.hitee.order24customerapp.Models;

public class FirebaseOrderInformations {

    /* public string OrderId { get; set; }
        public string CustomerName { get; set; }
        public string Price { get; set; }
        public string StoreName { get; set; }
        public string deliveryaddress { get; set; }
        public string date { get; set; }
        public string CustomerPhone { get; set; }
        public string Distance { get; set; }
        public string StoreId { get; set; } //business id
        public string status { get; set; }*/

    private String OrderId;
    private String CustomerName;
    private String Price;
    private String StoreName;
    private String deliveryaddress;
    private String date;
    private String CustomerPhone;
    private String Distance;
    private String StoreId;
    private String status;

    public FirebaseOrderInformations(){

    }

    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getStoreName() {
        return StoreName;
    }

    public void setStoreName(String storeName) {
        StoreName = storeName;
    }

    public String getDeliveryaddress() {
        return deliveryaddress;
    }

    public void setDeliveryaddress(String deliveryaddress) {
        this.deliveryaddress = deliveryaddress;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCustomerPhone() {
        return CustomerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        CustomerPhone = customerPhone;
    }

    public String getDistance() {
        return Distance;
    }

    public void setDistance(String distance) {
        Distance = distance;
    }

    public String getStoreId() {
        return StoreId;
    }

    public void setStoreId(String storeId) {
        StoreId = storeId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
