package com.hitee.order24customerapp.Models;

import android.os.Parcel;
import android.os.Parcelable;

public class CartModel2 implements Parcelable {


    private String id;
    private String imageProductUrl;
    private String name;
    private String price;
    private String ProductId;
    private String Quantity;
    private String ProductionTime;
    private FullPriceModel FullPrice;

    public CartModel2() {

    }

    public CartModel2(Parcel in) {
        id = in.readString();
        imageProductUrl = in.readString();
        name = in.readString();
        price = in.readString();
        ProductId = in.readString();
        Quantity = in.readString();
        ProductionTime = in.readString();
    }

    public static final Creator<CartModel2> CREATOR = new Creator<CartModel2>() {
        @Override
        public CartModel2 createFromParcel(Parcel in) {
            return new CartModel2(in);
        }

        @Override
        public CartModel2[] newArray(int size) {
            return new CartModel2[size];
        }
    };

    public FullPriceModel getFullPrice() {
        return FullPrice;
    }

    public void setFullPrice(FullPriceModel fullPrice) {
        FullPrice = fullPrice;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

    public String getProductionTime() {
        return ProductionTime;
    }

    public void setProductionTime(String productionTime) {
        ProductionTime = productionTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImageProductUrl() {
        return imageProductUrl;
    }

    public void setImageProductUrl(String imageProductUrl) {
        this.imageProductUrl = imageProductUrl;
    }

    public String getProductId() {
        return ProductId;
    }

    public void setProductId(String productId) {
        ProductId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(imageProductUrl);
        dest.writeString(name);
        dest.writeString(price);
        dest.writeString(ProductId);
        dest.writeString(Quantity);
        dest.writeString(ProductionTime);
    }
}



