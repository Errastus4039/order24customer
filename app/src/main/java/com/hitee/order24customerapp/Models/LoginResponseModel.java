package com.hitee.order24customerapp.Models;

public class LoginResponseModel {
    private String Id;
    private String email;


    public String getId() {
        return Id;
    }


    public void setId(String id) {
        Id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
