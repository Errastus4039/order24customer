package com.hitee.order24customerapp.Models;


public class Meta {
    String metavalue;
    String metaname;

    public Meta(String metaname, String metavalue) {
        this.metaname = metaname;
        this.metavalue = metavalue;
    }

    public String getMetavalue() {
        return this.metavalue;
    }

    public void setMetavalue(String metavalue) {
        this.metavalue = metavalue;
    }

    public String getMetaname() {
        return this.metaname;
    }

    public void setMetaname(String metaname) {
        this.metaname = metaname;
    }
}