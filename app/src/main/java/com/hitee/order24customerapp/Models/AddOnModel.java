package com.hitee.order24customerapp.Models;

public class AddOnModel {
    private String Id;
    private String Name;
    private String Price;
    private String Quantity;
    private String CustomerId;
    private String BusinessAddOnId;


    public String getBusinessAddOnId() {
        return BusinessAddOnId;
    }

    public void setBusinessAddOnId(String businessAddOnId) {
        BusinessAddOnId = businessAddOnId;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

    public String getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(String customerId) {
        CustomerId = customerId;
    }
}
