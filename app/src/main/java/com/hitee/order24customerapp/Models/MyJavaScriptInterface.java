package com.hitee.order24customerapp.Models;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

public class MyJavaScriptInterface {

    Handler handlerForJavascriptInterface = new Handler();

    private Context ctx;
    String res;
    public MyJavaScriptInterface(Context ctx) {
        this.ctx = ctx;
    }

    @JavascriptInterface
    public void showHTML(String html)
    {
        final String html_ = html;
        //code to use html content here
        handlerForJavascriptInterface.post(new Runnable() {
            @Override
            public void run()
            {
                Toast toast = Toast.makeText(ctx, "Page has been loaded in webview. html content :"+html_, Toast.LENGTH_LONG);
                toast.show();
            }});
    }

}
