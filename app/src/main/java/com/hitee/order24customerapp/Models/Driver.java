package com.hitee.order24customerapp.Models;

public class Driver {

    private String Id;
    private String DriverName;
    private String CreatedDate;
    private String FirstName;
    private String LastName;
    private String Email;
    private String vehicleType;
    private String Contact_Phone_1;
    private String Contact_Phone_2;
    private String Address;
    private String DriverImage;

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getDriverName() {
        return DriverName;
    }

    public void setDriverName(String driverName) {
        DriverName = driverName;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getContact_Phone_1() {
        return Contact_Phone_1;
    }

    public void setContact_Phone_1(String contact_Phone_1) {
        Contact_Phone_1 = contact_Phone_1;
    }

    public String getContact_Phone_2() {
        return Contact_Phone_2;
    }

    public void setContact_Phone_2(String contact_Phone_2) {
        Contact_Phone_2 = contact_Phone_2;
    }

    public String getDriverImage() {
        return DriverImage;
    }

    public void setDriverImage(String driverImage) {
        DriverImage = driverImage;
    }
}
