package com.hitee.order24customerapp.Models;

public class OthersModel {
    private String BaseFare;
    private String BaseFareEstimate;
    private String Distance;
    private String DistanceCalc;
    private String Duration;
    private String Email;
    private String KMCalc;
    private String PricePerKilometer;
    private String PriceperMins;
    private String ProductPrice;
    private String TOTAL_PRICE;
    private String vatt;


    public String getBaseFare() {
        return BaseFare;
    }

    public void setBaseFare(String baseFare) {
        BaseFare = baseFare;
    }

    public String getBaseFareEstimate() {
        return BaseFareEstimate;
    }

    public void setBaseFareEstimate(String baseFareEstimate) {
        BaseFareEstimate = baseFareEstimate;
    }

    public String getDistance() {
        return Distance;
    }

    public void setDistance(String distance) {
        Distance = distance;
    }

    public String getDistanceCalc() {
        return DistanceCalc;
    }

    public void setDistanceCalc(String distanceCalc) {
        DistanceCalc = distanceCalc;
    }

    public String getDuration() {
        return Duration;
    }

    public void setDuration(String duration) {
        Duration = duration;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getKMCalc() {
        return KMCalc;
    }

    public void setKMCalc(String KMCalc) {
        this.KMCalc = KMCalc;
    }

    public String getPricePerKilometer() {
        return PricePerKilometer;
    }

    public void setPricePerKilometer(String pricePerKilometer) {
        PricePerKilometer = pricePerKilometer;
    }

    public String getPriceperMins() {
        return PriceperMins;
    }

    public void setPriceperMins(String priceperMins) {
        PriceperMins = priceperMins;
    }

    public String getProductPrice() {
        return ProductPrice;
    }

    public void setProductPrice(String productPrice) {
        ProductPrice = productPrice;
    }

    public String getTOTAL_PRICE() {
        return TOTAL_PRICE;
    }

    public void setTOTAL_PRICE(String TOTAL_PRICE) {
        this.TOTAL_PRICE = TOTAL_PRICE;
    }

    public String getVatt() {
        return vatt;
    }

    public void setVatt(String vatt) {
        this.vatt = vatt;
    }
}
