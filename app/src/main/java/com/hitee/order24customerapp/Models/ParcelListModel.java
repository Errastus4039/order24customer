package com.hitee.order24customerapp.Models;

public class ParcelListModel {


    private String Id;
    private String ItemName;
    private String ItemDesc;
    private String ContactNo;
    private String Image;
    private String Status;
    private String Pickup;
    private String PickUpLat;
    private String PickLng;
    private String DeliveryLat;
    private String DeliveryLng;
    private String DeliveryAddress;
    private String customerId;
    private String DriverPhone;
    private String DriverEmail;
    private String DriverName;

    public String getDriverPhone() {
        return DriverPhone;
    }

    public void setDriverPhone(String driverPhone) {
        DriverPhone = driverPhone;
    }

    public String getDriverEmail() {
        return DriverEmail;
    }

    public void setDriverEmail(String driverEmail) {
        DriverEmail = driverEmail;
    }

    public String getDriverName() {
        return DriverName;
    }

    public void setDriverName(String driverName) {
        DriverName = driverName;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getItemName() {
        return ItemName;
    }

    public void setItemName(String itemName) {
        ItemName = itemName;
    }

    public String getItemDesc() {
        return ItemDesc;
    }

    public void setItemDesc(String itemDesc) {
        ItemDesc = itemDesc;
    }

    public String getContactNo() {
        return ContactNo;
    }

    public void setContactNo(String contactNo) {
        ContactNo = contactNo;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getPickup() {
        return Pickup;
    }

    public void setPickup(String pickup) {
        Pickup = pickup;
    }

    public String getPickUpLat() {
        return PickUpLat;
    }

    public void setPickUpLat(String pickUpLat) {
        PickUpLat = pickUpLat;
    }

    public String getPickLng() {
        return PickLng;
    }

    public void setPickLng(String pickLng) {
        PickLng = pickLng;
    }

    public String getDeliveryLat() {
        return DeliveryLat;
    }

    public void setDeliveryLat(String deliveryLat) {
        DeliveryLat = deliveryLat;
    }

    public String getDeliveryLng() {
        return DeliveryLng;
    }

    public void setDeliveryLng(String deliveryLng) {
        DeliveryLng = deliveryLng;
    }

    public String getDeliveryAddress() {
        return DeliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        DeliveryAddress = deliveryAddress;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
}
