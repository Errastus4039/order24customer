package com.hitee.order24customerapp.Models;

public class FullPriceModel {
    private String total;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}
