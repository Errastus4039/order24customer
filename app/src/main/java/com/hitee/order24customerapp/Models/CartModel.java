package com.hitee.order24customerapp.Models;

/**
 * Created by Wahyu on 06/08/2015.
 */
public class CartModel {


    private String id;
    private String imageProductUrl;
    private String name;
    private String price;
    private String ProductId;
    private String Quantity;
    private String ProductionTime;
    private FullPriceModel FullPrice;

    public FullPriceModel getFullPrice() {
        return FullPrice;
    }

    public void setFullPrice(FullPriceModel fullPrice) {
        FullPrice = fullPrice;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

    public String getProductionTime() {
        return ProductionTime;
    }

    public void setProductionTime(String productionTime) {
        ProductionTime = productionTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImageProductUrl() {
        return imageProductUrl;
    }

    public void setImageProductUrl(String imageProductUrl) {
        this.imageProductUrl = imageProductUrl;
    }

    public String getProductId() {
        return ProductId;
    }

    public void setProductId(String productId) {
        ProductId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
