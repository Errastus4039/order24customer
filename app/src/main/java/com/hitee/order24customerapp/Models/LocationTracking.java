package com.hitee.order24customerapp.Models;

public class LocationTracking {

    public String lat;
    public String lng;
    public String BookingID;

    // Default constructor required for calls to
    // DataSnapshot.getValue(User.class)
    public LocationTracking() {
    }

    public LocationTracking(String lat, String lng, String BookingID) {
        this.BookingID = BookingID;
        this.lat = lat;
        this.lng = lng;

    }
//    public String getLat() {
//        return lat;
//    }
//
//    public void setLat(String lat) {
//        this.lat = lat;
//    }
//
//    public String getLng() {
//        return lng;
//    }
//
//    public void setLng(String lng) {
//        this.lng = lng;
//    }
//
//    public String getBookingID() {
//        return BookingID;
//    }
//
//    public void setBookingID(String bookingID) {
//        BookingID = bookingID;
//    }
}
