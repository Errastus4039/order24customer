package com.hitee.order24customerapp.Models;

public class OrderListModel {
    private String Quantity;
    private String ProductName;

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }
}
