package com.hitee.order24customerapp.Models;

/**
 * Created by BABATUNDE on 2/6/2017.
 */
public class User {


    public String id;
    public String CategoryId;
    public String DateCreated;
    public String EmailAddress;
    public String CreatingUserId;
    public String IsActivated;
    public String PhoneNo;
    public String CategoryName;
    public String FullName;




    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setCategoryName(String categoryName) {
        CategoryName = categoryName;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public String getCategoryId() {
        return CategoryId;
    }

    public String getCreatingUserId() {
        return CreatingUserId;
    }

    public String getDateCreated() {
        return DateCreated;
    }

    public String getIsActivated() {
        return IsActivated;
    }

    public String getEmailAddress() {
        return EmailAddress;
    }

    public String getPhoneNo() {
        return PhoneNo;
    }

    public void setCategoryId(String categoryId) {
        CategoryId = categoryId;
    }

    public void setCreatingUserId(String creatingUserId) {
        CreatingUserId = creatingUserId;
    }

    public void setDateCreated(String dateCreated) {
        DateCreated = dateCreated;
    }

    public void setEmailAddress(String emailAddress) {
        EmailAddress = emailAddress;
    }

    public void setIsActivated(String isActivated) {
        IsActivated = isActivated;
    }

    public void setPhoneNo(String phoneNo) {
        PhoneNo = phoneNo;
    }

}