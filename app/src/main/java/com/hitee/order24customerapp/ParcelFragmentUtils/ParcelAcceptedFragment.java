package com.hitee.order24customerapp.ParcelFragmentUtils;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.hitee.order24customerapp.Adapter.ParcelListAdapter;
import com.hitee.order24customerapp.Bottom.CustomerActivity;
import com.hitee.order24customerapp.DashboardUtils.BusinessActivity;
import com.hitee.order24customerapp.Models.ParcelListModel;
import com.hitee.order24customerapp.Models.UserModel;
import com.hitee.order24customerapp.ParcelUtils.ParcelInfoActivity;
import com.hitee.order24customerapp.ParcelUtils.ParcelListActivity;
import com.hitee.order24customerapp.R;
import com.hitee.order24customerapp.Utils.Constant;
import com.hitee.order24customerapp.Utils.PrefUtils;
import com.hitee.order24customerapp.Utils.RecyclerTouchListener;
import com.hitee.order24customerapp.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class ParcelAcceptedFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    // private static final String TAG = ParcelListActivity.class.getSimpleName();
    private RecyclerView recyclerView;
    private ArrayList<ParcelListModel> cartList;
    private ParcelListAdapter mAdapter;
    private ShimmerFrameLayout mShimmerViewContainer;
    String CategoryId;

    private SwipeRefreshLayout swipeRefreshLayout;
    private UserModel model;

    public ParcelAcceptedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_parcel_accepted, container, false);

        final ActionBar abar = ((ParcelInfoActivity) Objects.requireNonNull(getActivity())).getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.custom_actionbar, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        TextView textviewTitle = (TextView) viewActionBar.findViewById(R.id.txtTitle);
        textviewTitle.setText("PARCEL ACCEPTED RIDER");
        textviewTitle.setTextSize(14);
        if (abar != null) {
            abar.setCustomView(viewActionBar, params);

            abar.setDisplayShowCustomEnabled(true);
            abar.setDisplayShowTitleEnabled(false);
            abar.setHomeButtonEnabled(false);
            abar.setDisplayHomeAsUpEnabled(true);
        }
        model = PrefUtils.getCurrentUserModel(getActivity());

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout2);
        swipeRefreshLayout.setOnRefreshListener(this);

        swipeRefreshLayout.post(
                new Runnable() {
                    @Override
                    public void run() {
                        fetchParcel(Constant.Accepted_PARCEL + "/" + model.getId());
                    }
                }
        );

        recyclerView = view.findViewById(R.id.recycle_view);
        cartList = new ArrayList<>();
        mAdapter = new ParcelListAdapter(getActivity(), cartList);


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));

       // fetchParcel(Constant.Accepted_PARCEL + "/" + model.getId());
        mShimmerViewContainer = view.findViewById(R.id.shimmer_view_container);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                final ParcelListModel doc = cartList.get(position);
                // Intent i = new Intent(ParcelListActivity.this, ParcelCurrentStatus.class);
                //  i.putExtra("MenuId", doc.getId());
                // startActivity(i);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        return view;
    }

    private void fetchParcel(String URL) {
        cartList.clear();
        Log.d("MENU", URL);
        final ProgressDialog progress = new ProgressDialog(getActivity());
        progress.setMessage("Loading...");
        progress.show();

        JsonArrayRequest request = new JsonArrayRequest(URL,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        progress.dismiss();
                        Log.d("LL", response.toString());
                        try {
                            if (response.toString().equals("[]")) {
                                Toast.makeText(getActivity(), "There is no parcel found.", Toast.LENGTH_LONG).show();
                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);
                                return;
                            }

                            for (int i = 0; i < response.length(); i++) {

                                try {
                                    final JSONObject obj = response.getJSONObject(i);

                                    ParcelListModel mydic = new ParcelListModel();
                                    mydic.setId(obj.getString("Id"));
                                    mydic.setContactNo(obj.getString("ContactNo"));
                                    mydic.setCustomerId(obj.getString("customerId"));
                                    mydic.setDeliveryAddress(obj.getString("DeliveryAddress"));
                                    mydic.setDeliveryLat(obj.getString("DeliveryLat"));
                                    mydic.setDeliveryLng(obj.getString("DeliveryLng"));
                                    mydic.setImage(obj.getString("Image"));
                                    mydic.setItemDesc(obj.getString("ItemDesc"));
                                    mydic.setItemName(obj.getString("ItemName"));
                                    mydic.setPickLng(obj.getString("PickLng"));
                                    mydic.setPickup(obj.getString("Pickup"));
                                    mydic.setPickUpLat(obj.getString("PickUpLat"));
                                    mydic.setStatus(obj.getString("Status"));

                                    cartList.add(mydic);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            mAdapter.notifyDataSetChanged();
                            // stop animating Shimmer and hide the layout
                            mShimmerViewContainer.stopShimmerAnimation();
                            mShimmerViewContainer.setVisibility(View.GONE);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        mAdapter.notifyDataSetChanged();

                        // stop animating Shimmer and hide the layout
                        mShimmerViewContainer.stopShimmerAnimation();
                        mShimmerViewContainer.setVisibility(View.GONE);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error in getting json
                //   Log.e(TAG, "Error: " + error.getMessage());
                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
                progress.dismiss();
                Toast.makeText(getContext(), "Opps! Something went wrong, please try again later ", Toast.LENGTH_SHORT).show();
            }
        });

        AppController.getInstance().addToRequestQueue(request);


    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        super.onPause();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                Intent ii = new Intent(getActivity(), CustomerActivity.class);
                startActivity(ii);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {

        fetchParcel(Constant.Accepted_PARCEL + "/" + model.getId());
    }

}
