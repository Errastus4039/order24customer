package com.hitee.order24customerapp.OtherUtils;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.media.Image;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.hitee.order24customerapp.LoginUtils.LoginActivity;
import com.hitee.order24customerapp.Models.ReferralModel;
import com.hitee.order24customerapp.Models.UserModel;
import com.hitee.order24customerapp.PaymentUtils.PINVerificationActivity;
import com.hitee.order24customerapp.R;
import com.hitee.order24customerapp.Utils.Constant;
import com.hitee.order24customerapp.Utils.PrefUtils;
import com.hitee.order24customerapp.app.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class ReferralActivity extends AppCompatActivity {

    ReferralModel referralModel;
    UserModel userModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_referral);


        final ActionBar abar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.custom_actionbar, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        TextView txttitle = (TextView) viewActionBar.findViewById(R.id.txtTitle);
        txttitle.setText("R E F E R R A L");
        txttitle.setTextSize(14);
        if (abar != null) {
            abar.setCustomView(viewActionBar, params);

            abar.setDisplayShowCustomEnabled(true);
            abar.setDisplayShowTitleEnabled(false);
            abar.setHomeButtonEnabled(false);
        }

        TextView txtCode = (TextView) findViewById(R.id.coupon);
        referralModel = PrefUtils.getReferralCode(ReferralActivity.this);
        userModel = PrefUtils.getCurrentUserModel(ReferralActivity.this);
        if (userModel == null || userModel.equals("null")) {
            startActivity(new Intent(ReferralActivity.this, LoginActivity.class));
            finish();
        } else {


        }
        if (referralModel == null) {
            // Tag used to cancel the request
            String tag_json_obj = "json_obj_req";

            if (userModel == null || userModel.equals("null")) {
                startActivity(new Intent(ReferralActivity.this, LoginActivity.class));
                finish();
            } else {
                String url = Constant.GETREFERRAL + "/" + userModel.getId();
                ProgressDialog pDialog = new ProgressDialog(this);
                pDialog.setMessage("Loading...");
                pDialog.show();

                JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                        url, null,
                        new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {
                                Log.d("TAG", response.toString());
                                pDialog.hide();
                                try {
                                    txtCode.setText(response.getString("code"));
                                    ReferralModel model = new ReferralModel();
                                    model.setCode(response.getString("code"));
                                    PrefUtils.setReferralCodeModel(model, ReferralActivity.this);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        // hide the progress dialog
                        pDialog.hide();
                    }
                });

// Adding request to request queue
                AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);


            }
        } else {
            txtCode.setText(referralModel.getCode());
        }

        ImageView facebook = (ImageView) findViewById(R.id.facebook);

        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PackageManager pm = getPackageManager();
                try {

                    Intent waIntent = new Intent(Intent.ACTION_SEND);
                    waIntent.setType("text/plain");
                    String text = "Use my referral code " + txtCode.getText().toString() + " and get a free order up to ₦400 to shop from your favourite restaurant and store on our market place. Redeem it at https://order24.ng/Account/RegInvite/" + userModel.getId();

                    PackageInfo info = pm.getPackageInfo("com.facebook.katana",
                            PackageManager.GET_META_DATA);
                    //Check if package exists or not. If not then code
                    //in catch block will be called
                    waIntent.setPackage("com.facebook.katana");

                    waIntent.putExtra(Intent.EXTRA_TEXT, text);
                    startActivity(Intent.createChooser(waIntent, "Share with"));

                } catch (PackageManager.NameNotFoundException e) {
                    Toast.makeText(ReferralActivity.this, "Facebook is not Installed", Toast.LENGTH_SHORT)
                            .show();
                }
            }
        });
        ImageView twitter = (ImageView) findViewById(R.id.twitter);
        twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PackageManager pm = getPackageManager();
                try {

                    Intent waIntent = new Intent(Intent.ACTION_SEND);
                    waIntent.setType("text/plain");
                    String text = "Use my referral code " + txtCode.getText().toString() + " and get a free order up to ₦400 to shop from your favourite restaurant and store on our market place. Redeem it at https://order24.ng/Account/RegInvite/" + userModel.getId();

                    PackageInfo info = pm.getPackageInfo("com.twitter.android",
                            PackageManager.GET_META_DATA);
                    //Check if package exists or not. If not then code
                    //in catch block will be called
                    waIntent.setPackage("com.twitter.android");

                    waIntent.putExtra(Intent.EXTRA_TEXT, text);
                    startActivity(Intent.createChooser(waIntent, "Share with"));

                } catch (PackageManager.NameNotFoundException e) {
                    Toast.makeText(ReferralActivity.this, "Twitter is not Installed", Toast.LENGTH_SHORT)
                            .show();
                }
            }
        });
        ImageView instagram = (ImageView) findViewById(R.id.instagram);
        instagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PackageManager pm = getPackageManager();
                try {

                    Intent waIntent = new Intent(Intent.ACTION_SEND);
                    waIntent.setType("text/plain");
                    String text = "Use my referral code " + txtCode.getText().toString() + " and get a free order up to ₦400 to shop from your favourite restaurant and store on our market place. Redeem it at https://order24.ng/Account/RegInvite/" + userModel.getId();


                    PackageInfo info = pm.getPackageInfo("com.instagram.android",
                            PackageManager.GET_META_DATA);
                    //Check if package exists or not. If not then code
                    //in catch block will be called
                    waIntent.setPackage("com.instagram.android");

                    waIntent.putExtra(Intent.EXTRA_TEXT, text);
                    startActivity(Intent.createChooser(waIntent, "Share with"));

                } catch (PackageManager.NameNotFoundException e) {
                    Toast.makeText(ReferralActivity.this, "Instagram is not Installed", Toast.LENGTH_SHORT)
                            .show();
                }
            }
        });
        ImageView whatsapp = (ImageView) findViewById(R.id.whatapp);

        whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PackageManager pm = getPackageManager();
                try {

                    Intent waIntent = new Intent(Intent.ACTION_SEND);
                    waIntent.setType("text/plain");
                    String text = "Use my referral code " + txtCode.getText().toString() + " and get a free order up to ₦400 to shop from your favourite restaurant and store on our market place. Redeem it at https://order24.ng/Account/RegInvite/" + userModel.getId();

                    PackageInfo info = pm.getPackageInfo("com.whatsapp",
                            PackageManager.GET_META_DATA);
                    //Check if package exists or not. If not then code
                    //in catch block will be called
                    waIntent.setPackage("com.whatsapp");

                    waIntent.putExtra(Intent.EXTRA_TEXT, text);
                    startActivity(Intent.createChooser(waIntent, "Share with"));

                } catch (PackageManager.NameNotFoundException e) {
                    Toast.makeText(ReferralActivity.this, "WhatsApp not Installed", Toast.LENGTH_SHORT)
                            .show();
                }

            }
        });

    }

    public String GetREF() {
        SecureRandom secureRandom = new SecureRandom();
        String CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
        StringBuilder generatedString = new StringBuilder();
        for (int i = 0; i < 7; i++) {
            int randonSequence = secureRandom.nextInt(CHARACTERS.length());
            generatedString.append(CHARACTERS.charAt(randonSequence));
        }

        return generatedString.toString();
    }

}
