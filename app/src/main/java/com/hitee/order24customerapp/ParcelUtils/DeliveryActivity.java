package com.hitee.order24customerapp.ParcelUtils;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.hitee.order24customerapp.AddressUtils.AddNewAddressActivity;
import com.hitee.order24customerapp.AddressUtils.AddressActivity;
import com.hitee.order24customerapp.Bottom.CustomerActivity;
import com.hitee.order24customerapp.LoginUtils.LoginActivity;
import com.hitee.order24customerapp.Models.UserModel;
import com.hitee.order24customerapp.OrderUtils.OrderConfirmationActivity;
import com.hitee.order24customerapp.R;
import com.hitee.order24customerapp.Utils.AlertDialogManager;
import com.hitee.order24customerapp.Utils.Constant;
import com.hitee.order24customerapp.Utils.PrefUtils;
import com.hitee.order24customerapp.Utils.ViewDialog;
import com.hitee.order24customerapp.app.AppController;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class DeliveryActivity extends AppCompatActivity {

    String TAG = "placeautocomplete";
    TextView deliveryaddress, pickupaddress;
    String placename;

    LatLng DeliverylatLng, PickUpLatLng;

    AlertDialogManager alertDialogManager;

    ViewDialog viewDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery);

        final ActionBar abar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.custom_actionbar, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        TextView txttitle = (TextView) viewActionBar.findViewById(R.id.txtTitle);
        txttitle.setText("P I C K - U P - A D D R E S S");
        txttitle.setTextSize(14);
        if (abar != null) {
            abar.setCustomView(viewActionBar, params);

            abar.setDisplayShowCustomEnabled(true);
            abar.setDisplayShowTitleEnabled(false);
            abar.setHomeButtonEnabled(false);
            abar.setDisplayHomeAsUpEnabled(true);
        }
        viewDialog = new ViewDialog(this);
        alertDialogManager = new AlertDialogManager();
        deliveryaddress = (TextView) findViewById(R.id.txtView1);
        pickupaddress = (TextView) findViewById(R.id.txtView);

        Intent ii = getIntent();
        final String itemName = ii.getStringExtra("ItemName");
        final String txtDescription = ii.getStringExtra("txtDescription");
        final String txtContact = ii.getStringExtra("txtContact");
        final String picture = ii.getStringExtra("picture");


        // Initialize Places.
        Places.initialize(getApplicationContext(), "AIzaSyDxslmBCBD8ECpQKZ-AJIfxRFbUt7kmndA");
        // Create a new Places client instance.
        PlacesClient placesClient = Places.createClient(this);

        // Initialize the AutocompleteSupportFragment.
        AutocompleteSupportFragment autocompleteFragmentPickup = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);


        // Initialize the AutocompleteSupportFragment.
        AutocompleteSupportFragment autocompleteFragmentDelivery = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment_deveiery);


        // Specify the types of place data to return.
        autocompleteFragmentPickup.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG));

        autocompleteFragmentDelivery.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG));


        // Set up a PlaceSelectionListener to handle the response.
        autocompleteFragmentPickup.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                pickupaddress.setText(place.getName());
                //placename = place.getName();
                PickUpLatLng = place.getLatLng();


                if (PickUpLatLng != null) {
                    Log.i(TAG, "Place: " + PickUpLatLng.latitude);
                }
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i(TAG, "An error occurred: " + status);
            }
        });


        autocompleteFragmentDelivery.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                deliveryaddress.setText(place.getName());
                // placename = place.getName();
                DeliverylatLng = place.getLatLng();


                if (DeliverylatLng != null) {
                    Log.i(TAG, "Place: " + DeliverylatLng.latitude);
                }
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i(TAG, "An error occurred: " + status);
            }
        });


        Button btnSubmit = (Button) findViewById(R.id.btnsubmit);


        final UserModel userModel = PrefUtils.getCurrentUserModel(DeliveryActivity.this);
        if (userModel == null || userModel.equals("null")) {

            startActivity(new Intent(DeliveryActivity.this, LoginActivity.class));
        } else {

            btnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    viewDialog.showDialog();
                    StringRequest strReq = new StringRequest(Request.Method.POST, Constant.SEND_PARCEL,
                            new com.android.volley.Response.Listener<String>() {

                                @Override
                                public void onResponse(String response) {
                                    Log.d("TAGGG", String.valueOf(response));
                                    viewDialog.hideDialog();
                                    try {
                                        JSONObject jsonObject = null;
                                        try {
                                            jsonObject = new JSONObject(response);
                                            if (String.valueOf(jsonObject.getString("HttpStatusCode")).equals("200")) {
                                               // alertDialogManager.showAlertDialog(DeliveryActivity.this, "Success", "Your parcel has been initiated", true);
                                                deliveryaddress.setText("");
                                                pickupaddress.setText("");
                                                Intent ii = new Intent(DeliveryActivity.this, PaymentActivity.class);
                                                ii.putExtra("Id", jsonObject.getString("Id"));
                                                ii.putExtra("Amount", jsonObject.getString("Amount"));
                                                startActivity(ii);

                                            } else {
                                                alertDialogManager.showAlertDialog(DeliveryActivity.this, "Failed", "Opps!, Error occurred while saving address", false);
                                            }

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                }
                            }, new com.android.volley.Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            NetworkResponse response = error.networkResponse;
                            if (error instanceof ServerError && response != null) {

                            }
                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("DeliveryLat", String.valueOf(DeliverylatLng.latitude));
                            params.put("DeliveryAddress", String.valueOf(deliveryaddress.getText()));
                            params.put("DeliveryLng", String.valueOf(DeliverylatLng.longitude));
                            params.put("PickLng", String.valueOf(PickUpLatLng.longitude));
                            params.put("PickUpLat", String.valueOf(PickUpLatLng.latitude));
                            params.put("Pickup", pickupaddress.getText().toString());
                            params.put("Image", picture);
                            params.put("ContactNo", txtContact);
                            params.put("ItemDesc", txtDescription);
                            params.put("ItemName", itemName);
                            params.put("CustomerId", userModel.getId());

                          //  Log.d("POST_TAG", params.toString());
                            return params;
                        }
                    };
                    DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                    strReq.setRetryPolicy(retryPolicy);
                    AppController.getInstance().addToRequestQueue(strReq);
                }
            });

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                Intent ii = new Intent(DeliveryActivity.this, CustomerActivity.class);
                startActivity(ii);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
