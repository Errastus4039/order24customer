package com.hitee.order24customerapp.ParcelUtils;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.hitee.order24customerapp.Bottom.CustomerActivity;
import com.hitee.order24customerapp.ParcelFragmentUtils.ParcelAcceptedFragment;
import com.hitee.order24customerapp.ParcelFragmentUtils.ParcelCompletedFragment;
import com.hitee.order24customerapp.ParcelFragmentUtils.ParcelReassignFragment;
import com.hitee.order24customerapp.ParcelFragmentUtils.ParelWaitingForRiderFragment;
import com.hitee.order24customerapp.R;

public class ParcelInfoActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parcel_info);
        BottomNavigationView navigation = findViewById(R.id.nav_view);
        navigation.setOnNavigationItemSelectedListener(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        loadFragment(new ParelWaitingForRiderFragment());


    }

    private boolean loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;

        switch (item.getItemId()) {
            case R.id.parcel_waiting:
                fragment = new ParelWaitingForRiderFragment();
                break;

            case R.id.parcel_accepted:
                fragment = new ParcelAcceptedFragment();
                break;

            case R.id.parcel_completed:
                fragment = new ParcelCompletedFragment();
                break;

            case R.id.parcel_reassign:
                fragment = new ParcelReassignFragment();
                break;

        }
        loadFragment(fragment);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                Intent ii = new Intent(ParcelInfoActivity.this, CustomerActivity.class);
                startActivity(ii);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
