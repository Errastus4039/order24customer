package com.hitee.order24customerapp.ParcelUtils;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.hitee.order24customerapp.Adapter.MenuAdapter;
import com.hitee.order24customerapp.Adapter.ParcelListAdapter;
import com.hitee.order24customerapp.Bottom.CustomerActivity;
import com.hitee.order24customerapp.BusinessUtils.BusinessMenuActivity;
import com.hitee.order24customerapp.Models.MenuModel;
import com.hitee.order24customerapp.Models.ParcelListModel;
import com.hitee.order24customerapp.Models.UserModel;
import com.hitee.order24customerapp.OrderUtils.OrderConfirmationActivity;
import com.hitee.order24customerapp.ProductsUtils.ProductListActivity;
import com.hitee.order24customerapp.R;
import com.hitee.order24customerapp.Utils.Constant;
import com.hitee.order24customerapp.Utils.PrefUtils;
import com.hitee.order24customerapp.Utils.RecyclerTouchListener;
import com.hitee.order24customerapp.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ParcelListActivity extends AppCompatActivity {

    private static final String TAG = ParcelListActivity.class.getSimpleName();
    private RecyclerView recyclerView;
    private ArrayList<ParcelListModel> cartList;
    private ParcelListAdapter mAdapter;
    private ShimmerFrameLayout mShimmerViewContainer;
    String CategoryId;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parcel_list);


        final ActionBar abar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.custom_actionbar, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        TextView txttitle = (TextView) viewActionBar.findViewById(R.id.txtTitle);
        txttitle.setText("L I S T  O F P A R C E L");
        txttitle.setTextSize(14);
        if (abar != null) {
            abar.setCustomView(viewActionBar, params);

            abar.setDisplayShowCustomEnabled(true);
            abar.setDisplayShowTitleEnabled(false);
            abar.setHomeButtonEnabled(false);
            abar.setDisplayHomeAsUpEnabled(true);
        }

        UserModel model = PrefUtils.getCurrentUserModel(ParcelListActivity.this);

        recyclerView = findViewById(R.id.recycle_view);
        cartList = new ArrayList<>();
        mAdapter = new ParcelListAdapter(ParcelListActivity.this, cartList);


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        fetchParcel(Constant.GET_PARCEL + "/" + model.getId());
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                final ParcelListModel doc = cartList.get(position);
                // Intent i = new Intent(ParcelListActivity.this, ParcelCurrentStatus.class);
                //  i.putExtra("MenuId", doc.getId());
                // startActivity(i);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }


    private void fetchParcel(String URL) {

        Log.d("MENU", URL);
        final ProgressDialog progress = new ProgressDialog(ParcelListActivity.this);
        progress.setMessage("Loading...");
        progress.show();

        JsonArrayRequest request = new JsonArrayRequest(URL,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        progress.dismiss();
                        Log.d("LL", response.toString());
                        try {
                            if (response.toString().equals("[]")) {
                                Toast.makeText(ParcelListActivity.this, "There is no parcel found.", Toast.LENGTH_LONG).show();
                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);
                                return;
                            }

                            for (int i = 0; i < response.length(); i++) {

                                try {
                                    final JSONObject obj = response.getJSONObject(i);

                                    ParcelListModel mydic = new ParcelListModel();
                                    mydic.setId(obj.getString("Id"));
                                    mydic.setContactNo(obj.getString("ContactNo"));
                                    mydic.setCustomerId(obj.getString("customerId"));
                                    mydic.setDeliveryAddress(obj.getString("DeliveryAddress"));
                                    mydic.setDeliveryLat(obj.getString("DeliveryLat"));
                                    mydic.setDeliveryLng(obj.getString("DeliveryLng"));
                                    mydic.setImage(obj.getString("Image"));
                                    mydic.setItemDesc(obj.getString("ItemDesc"));
                                    mydic.setItemName(obj.getString("ItemName"));
                                    mydic.setPickLng(obj.getString("PickLng"));
                                    mydic.setPickup(obj.getString("Pickup"));
                                    mydic.setPickUpLat(obj.getString("PickUpLat"));
                                    mydic.setStatus(obj.getString("Status"));

                                    cartList.add(mydic);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            mAdapter.notifyDataSetChanged();
                            // stop animating Shimmer and hide the layout
                            mShimmerViewContainer.stopShimmerAnimation();
                            mShimmerViewContainer.setVisibility(View.GONE);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        mAdapter.notifyDataSetChanged();

                        // stop animating Shimmer and hide the layout
                        mShimmerViewContainer.stopShimmerAnimation();
                        mShimmerViewContainer.setVisibility(View.GONE);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error in getting json
                Log.e(TAG, "Error: " + error.getMessage());
                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
                progress.dismiss();
                Toast.makeText(getApplicationContext(), "Opps! Something went wrong, please try again later ", Toast.LENGTH_SHORT).show();
            }
        });

        AppController.getInstance().addToRequestQueue(request);


    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                Intent ii = new Intent(ParcelListActivity.this, CustomerActivity.class);
                startActivity(ii);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
