package com.hitee.order24customerapp.ParcelUtils;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.andrognito.pinlockview.IndicatorDots;
import com.andrognito.pinlockview.PinLockListener;
import com.andrognito.pinlockview.PinLockView;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.flutterwave.raveandroid.RaveConstants;
import com.flutterwave.raveandroid.RavePayActivity;
import com.flutterwave.raveandroid.RavePayManager;
import com.hitee.order24customerapp.Bottom.CustomerActivity;
import com.hitee.order24customerapp.LoginUtils.LoginActivity;
import com.hitee.order24customerapp.Models.UserModel;
import com.hitee.order24customerapp.OrderUtils.OrderConfirmationActivity;
import com.hitee.order24customerapp.OrderUtils.OrderSubmittedActivity;
import com.hitee.order24customerapp.PaymentUtils.RavePaymentActivity;
import com.hitee.order24customerapp.R;
import com.hitee.order24customerapp.Utils.AlertDialogManager;
import com.hitee.order24customerapp.Utils.Constant;
import com.hitee.order24customerapp.Utils.PrefUtils;
import com.hitee.order24customerapp.WalletUtils.LockWallet;
import com.hitee.order24customerapp.app.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class PaymentActivity extends AppCompatActivity {

    UserModel userModel;
    AlertDialogManager alertDialogManager;
    double amountDue;
    JSONObject jsonObject = null;
    String Id;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        final ActionBar abar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.custom_actionbar, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        TextView txttitle = (TextView) viewActionBar.findViewById(R.id.txtTitle);
        txttitle.setText("P R E V I E W  P A Y M E N T");
        txttitle.setTextSize(12);
        if (abar != null) {
            abar.setCustomView(viewActionBar, params);

            abar.setDisplayShowCustomEnabled(true);
            abar.setDisplayShowTitleEnabled(false);
            abar.setHomeButtonEnabled(false);
            abar.setDisplayHomeAsUpEnabled(true);
        }
        Intent retrieve = getIntent();
        TextView txtBalance = (TextView) findViewById(R.id.balance);
        Id = retrieve.getStringExtra("Id");
        alertDialogManager = new AlertDialogManager();
        String[] arrOfStr = retrieve.getStringExtra("Amount").split("-");

        String arr = "₦" + arrOfStr[0] + " - " + "₦" + arrOfStr[1];

        txtBalance.setText(arr);
        userModel = PrefUtils.getCurrentUserModel(PaymentActivity.this);
        if (userModel == null) {
            startActivity(new Intent(PaymentActivity.this, LoginActivity.class));
        } else {


            Button btnCard = (Button) findViewById(R.id.btnCard);
            btnCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String amt = arrOfStr[0];
                    Log.d("AMOUNT", amt);
                    String Fullname = userModel.getFirstName() + " " + userModel.getLastName();
                    Intent ii = new Intent(PaymentActivity.this, ParcelPaymentActivity.class);
                    ii.putExtra("amount", amt);
                    ii.putExtra("ParcelId", Id);
                    startActivity(ii);
                }
            });

            Button btnWallet = (Button) findViewById(R.id.btnWallet);
            btnWallet.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ViewGroup viewGroup = findViewById(android.R.id.content);

                    //then we will inflate the custom alert dialog xml that we created
                    View dialogView = LayoutInflater.from(v.getContext()).inflate(R.layout.lock_screen, viewGroup, false);
                    PinLockView mPinLockView = (PinLockView) dialogView.findViewById(R.id.pin_lock_view);
                    IndicatorDots mIndicatorDots = (IndicatorDots) dialogView.findViewById(R.id.indicator_dots);
                    AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());

                    //setting the view of the builder to our custom view that we already inflated
                    builder.setView(dialogView);

                    //finally creating the alert dialog and displaying it
                    AlertDialog alertDialog = builder.create();
                    mPinLockView.attachIndicatorDots(mIndicatorDots);
                    mPinLockView.setPinLockListener(new PinLockListener() {
                        @Override
                        public void onComplete(String pin) {
                            final ProgressDialog progress = new ProgressDialog(v.getContext());
                            progress.setTitle("Pin Confirmation");
                            progress.setMessage("Please wait till we verify the pin");
                            progress.show();


                            StringRequest strReq = new StringRequest(Request.Method.POST, Constant.PIN_WALLET,
                                    new com.android.volley.Response.Listener<String>() {

                                        @Override
                                        public void onResponse(String response) {
                                            progress.dismiss();
                                            try {

                                                try {
                                                    jsonObject = new JSONObject(response);

                                                    if (String.valueOf(jsonObject.getString("HttpStatusCode")).equals("200")) {
                                                        Log.d("BALANCE_AMOUNT", jsonObject.getString("Amount"));
                                                        double amount = Double.parseDouble(jsonObject.getString("Amount"));
                                                        double amountDue = Double.parseDouble(arrOfStr[0]);
                                                        if (amount < amountDue) {
                                                            alertDialogManager.showAlertDialog(v.getContext(), "WALLET", "The wallet amount is less than the order amount. Kindly fund your wallet", false);
                                                            alertDialog.dismiss();
                                                        } else {


                                                            StringRequest strReq = new StringRequest(Request.Method.POST, Constant.SEND_PAYMENT,
                                                                    new com.android.volley.Response.Listener<String>() {

                                                                        @Override
                                                                        public void onResponse(String response) {
                                                                            progress.dismiss();
                                                                            try {
                                                                                if (response.equals("200")) {
                                                                                    alertDialogManager.showAlertDialog(PaymentActivity.this, "PARCEL", "Your order has been sent out to driver", true);
                                                                                    finish();
                                                                                    startActivity(new Intent(PaymentActivity.this, ParcelListActivity.class));
                                                                                    alertDialog.dismiss();

                                                                                } else {
                                                                                    alertDialogManager.showAlertDialog(PaymentActivity.this, "Submission failed", "Your payment is not complete", false);
                                                                                }

                                                                            } catch (Exception e) {
                                                                                e.printStackTrace();
                                                                            }

                                                                        }
                                                                    }, new com.android.volley.Response.ErrorListener() {
                                                                @Override
                                                                public void onErrorResponse(VolleyError error) {

                                                                    NetworkResponse response = error.networkResponse;
                                                                    if (error instanceof ServerError && response != null) {

                                                                    }
                                                                }
                                                            }) {

                                                                @Override
                                                                protected Map<String, String> getParams() {

                                                                    Map<String, String> params = new HashMap<String, String>();
                                                                    params.put("userId", userModel.getId());
                                                                    params.put("amount", String.valueOf(amountDue));
                                                                    params.put("charged_amount", String.valueOf(amountDue));
                                                                    params.put("transaction_type", "DEBIT");
                                                                    params.put("firstname", userModel.getFirstName());
                                                                    params.put("lastname", userModel.getLastName());
                                                                    params.put("phonenumber", userModel.getPhoneNumber());
                                                                    params.put("email", userModel.getEmail());
                                                                    params.put("ParcelId", Id);
                                                                    Log.d("POST_TAGGG", params.toString());
                                                                    return params;
                                                                }
                                                            };
                                                            DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                                                            strReq.setRetryPolicy(retryPolicy);
                                                            AppController.getInstance().addToRequestQueue(strReq);

                                                        }

                                                    } else {

                                                        Toast.makeText(v.getContext(), "Wrong pin supplied", Toast.LENGTH_SHORT).show();
                                                        // alertDialogManager.showAlertDialog(LockWallet.this, "Failed", "Wrong Pin Supplied", false);
                                                    }

                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }

                                        }
                                    }, new com.android.volley.Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {

                                    NetworkResponse response = error.networkResponse;
                                    if (error instanceof ServerError && response != null) {

                                    }
                                }
                            }) {

                                @Override
                                protected Map<String, String> getParams() {

                                    Map<String, String> params = new HashMap<String, String>();
                                    params.put("code", pin);
                                    params.put("CustomerId", userModel.getId());
                                    return params;
                                }
                            };
                            DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                            strReq.setRetryPolicy(retryPolicy);
                            AppController.getInstance().addToRequestQueue(strReq);

                        }

                        @Override
                        public void onEmpty() {

                        }

                        @Override
                        public void onPinChange(int pinLength, String intermediatePin) {

                        }
                    });

                    mPinLockView.enableLayoutShuffling();

                    mPinLockView.setPinLength(4);
                    mPinLockView.setTextColor(ContextCompat.getColor(v.getContext(), R.color.white));

                    mIndicatorDots.setIndicatorType(IndicatorDots.IndicatorType.FILL_WITH_ANIMATION);

                    //Now we need an AlertDialog.Builder object

                    alertDialog.show();
                }
            });
        }
    }

    public String GetREF() {
        Long max = 0L;
        Long min = 100000000000L;
        //Use the date format that best suites you
        @SuppressLint("SimpleDateFormat") SimpleDateFormat spf = new SimpleDateFormat("dd/MM/yyyy");
        Long randomLong = 0L;
        for (int i = 0; i <= 10; i++) {
            Random r = new Random();
            randomLong = (r.nextLong() % (max - min)) + min;
            Date dt = new Date(randomLong);
        }
        return randomLong.toString();
    }


    private void showCustomDialog() {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(this).inflate(R.layout.info_window, viewGroup, false);


        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);

        //finally creating the alert dialog and displaying it
        AlertDialog alertDialog = builder.create();
        alertDialog.show();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RaveConstants.RAVE_REQUEST_CODE && data != null) {
            String message = data.getStringExtra("response");
            if (resultCode == RavePayActivity.RESULT_SUCCESS) {
                Log.d("FLUTTER", message);

                try {
                    JSONObject object = new JSONObject(message);
                    final JSONObject arr = object.getJSONObject("data");
                    Log.d("ARRAY", arr.toString());
                    // Toast.makeText(OrderConfirmationActivity.this, arr.getString("amount"), Toast.LENGTH_LONG).show();

                    final String amount = arr.getString("amount");
                    final String IP = arr.getString("IP");
                    final String appfee = arr.getString("appfee");
                    final String charged_amount = arr.getString("charged_amount");
                    final String currency = arr.getString("currency");
                    final String flwRef = arr.getString("flwRef");
                    final String narration = arr.getString("narration");
                    final String orderRef = arr.getString("orderRef");
                    final String raveRef = arr.getString("raveRef");
                    final String txRef = arr.getString("txRef");
                    final String vbRespcode = arr.getString("vbvrespcode");
                    final String vbrespmessage = arr.getString("vbvrespmessage");

                    // Toast.makeText(this, "SUCCESS " + message, Toast.LENGTH_SHORT).show();
                    final ProgressDialog progress = new ProgressDialog(PaymentActivity.this);
                    progress.setTitle("O R D E R  CONFIRMATION");
                    progress.setMessage("Submitting Order...");
                    progress.show();


                    StringRequest strReq = new StringRequest(Request.Method.POST, Constant.PAY_WITH_CARD,
                            new com.android.volley.Response.Listener<String>() {

                                @Override
                                public void onResponse(String response) {
                                    Log.d("TAGGGGGGGG", String.valueOf(response));
                                    progress.dismiss();
                                    try {
                                        if (response.equals("200")) {
                                            alertDialogManager.showAlertDialog(PaymentActivity.this, "PARCEL", "Your order has been sent out to driver", true);
                                            startActivity(new Intent(PaymentActivity.this, ParcelListActivity.class));
                                            finish();
                                        } else {
                                            alertDialogManager.showAlertDialog(PaymentActivity.this, "Submission failed", "Your order is not submitted", false);
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                }
                            }, new com.android.volley.Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            NetworkResponse response = error.networkResponse;
                            if (error instanceof ServerError && response != null) {

                            }
                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("CustomerId", userModel.getId());
                            params.put("Amount", amount);
                            params.put("IP", IP);
                            params.put("appfee", appfee);
                            params.put("charged_amount", charged_amount);
                            params.put("currency", currency);
                            params.put("flwRef", flwRef);
                            params.put("narration", narration);
                            params.put("orderRef", orderRef);
                            params.put("raveRef", raveRef);
                            params.put("txRef", txRef);
                            params.put("vbRespcode", vbRespcode);
                            params.put("vbrespmessage", vbrespmessage);
                            params.put("FirstName", userModel.getFirstName());
                            params.put("LastName", userModel.getLastName());
                            params.put("PhoneNumber", userModel.getPhoneNumber());
                            params.put("Email", userModel.getEmail());
                            params.put("ParcelId", Id);
                            Log.d("POST_TAGG", params.toString());
                            return params;
                        }
                    };
                    DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                    strReq.setRetryPolicy(retryPolicy);
                    AppController.getInstance().addToRequestQueue(strReq);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                Intent ii = new Intent(PaymentActivity.this, CustomerActivity.class);
                startActivity(ii);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
